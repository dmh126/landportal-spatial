"use strict";
var webpack = require('webpack');
var path = require('path');
var loaders = require('./webpack.loaders');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var WebpackLoggingPlugin = require('webpack-logging-plugin');
var DashboardPlugin = require('webpack-dashboard/plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var ExtendedDefinePlugin = require('extended-define-webpack-plugin');
var CopywebpackPlugin = require('copy-webpack-plugin');
var appConfig = require('./config.local');

const HOST = process.env.HOST || "127.0.0.1";
const PORT = process.env.PORT || "8000";

loaders.push({
  test: /\.scss$/,
  loaders: ['style-loader', 'css-loader?importLoaders=1', 'sass-loader'],
  exclude: ['node_modules']
});

module.exports = {
  entry: [
    './src/index.jsx', // your app's entry point
  ],
  devtool: process.env.WEBPACK_DEVTOOL || 'eval-source-map',
  output: {
    publicPath: '/',
    path: path.join(__dirname, '../public'),
    filename: 'bundle.js',
    sourcePrefix: ''
  },
  amd: {
        // Enable webpack-friendly use of require in Cesium
        toUrlUndefined: true
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders
  },
  node: {
    fs: 'empty'
  },
  devServer: {
    contentBase: "../public",
    // do not print bundle build stats
    noInfo: true,
    // enable HMR
    hot: true,
    // embed the webpack-dev-server runtime into the bundle
    inline: true,
    // serve index.html in place of 404 responses to allow HTML5 history
    historyApiFallback: true,
    port: PORT,
    host: HOST,
    stats: 'errors-only'
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new ExtractTextPlugin({
      filename: 'style.css',
      allChunks: true
    }),
    new ExtendedDefinePlugin({
	    __API_URL__ : appConfig.endpoint,
        __HOST_URL__: appConfig.host
    }),
    new DashboardPlugin(),
    new HtmlWebpackPlugin({

      cesium: appConfig.endpoint + '/static/Cesium/Cesium.js',
      template: './src/template.html',
      files: {
        css: ['style.css'],
        js: [ "bundle.js"],
      }
    }),
    new WebpackLoggingPlugin({
      formatError: (err) => err,
      successCallback: () => console.log(`
Powered by:

   ██████╗ ██████╗ ███╗   ██╗████████╗███████╗ ██████╗██╗  ██╗████████╗     ██████╗ ███╗   ███╗██████╗ ██╗  ██╗
  ██╔════╝██╔═══██╗████╗  ██║╚══██╔══╝██╔════╝██╔════╝██║  ██║╚══██╔══╝    ██╔════╝ ████╗ ████║██╔══██╗██║  ██║
  ██║     ██║   ██║██╔██╗ ██║   ██║   █████╗  ██║     ███████║   ██║       ██║  ███╗██╔████╔██║██████╔╝███████║
  ██║     ██║   ██║██║╚██╗██║   ██║   ██╔══╝  ██║     ██╔══██║   ██║       ██║   ██║██║╚██╔╝██║██╔══██╗██╔══██║
  ╚██████╗╚██████╔╝██║ ╚████║   ██║   ███████╗╚██████╗██║  ██║   ██║       ╚██████╔╝██║ ╚═╝ ██║██████╔╝██║  ██║
   ╚═════╝ ╚═════╝ ╚═╝  ╚═══╝   ╚═╝   ╚══════╝ ╚═════╝╚═╝  ╚═╝   ╚═╝        ╚═════╝ ╚═╝     ╚═╝╚═════╝ ╚═╝  ╚═╝

Author: Damian Harasymczuk
Contact: harasymczuk@contecht.eu

running on http://localhost:${PORT}
        `)
    }),
  ]
};
