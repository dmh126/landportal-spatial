import * as actionTypes from '../constants/actions';

export const browseByDatasets = (data) => {

    return {
        type: actionTypes.BROWSE_BY_DATASETS,
        payload: data,
    }

}

export const browseByThemes = (data) => {

    return {
        type: actionTypes.BROWSE_BY_THEMES,
        payload: data,
    }

}

export const browseByCountries = (data) => {

    return {
        type: actionTypes.BROWSE_BY_COUNTRIES,
        payload: data,
    }

}

export const browseByMain = (data) => {

    return {
        type: actionTypes.BROWSE_BY_MAIN,
        payload: data,
    }

}

export const browseAdvanced = (data) => {

    return {
        type: actionTypes.BROWSE_ADVANCED,
        payload: data,
    }
}

export const showIndicatorInfo = (data) => {

    return {
        type: actionTypes.SHOW_INDICATOR_INFO,
        payload: data,
    }
}
