import DatasetsService from '../services/datasetsService';
import {addDisplayedLayer} from './actions-datasets.js';
import {showModalCatalog} from './actions-viewer.js';
import * as actionTypes from '../constants/actions';

import {
  createBaseMap,
  createWmsLayer,
  createVectorLayer,
  createSparqlLayer,
  createHeatmapLayer,
  buildHeatmapFromVector,
  buildHeatmapFromSparql
} from '../components/viewer/helpers.js';

export const indicatorRequested = () => {

    return {
        type: actionTypes.REQUEST_INDICATOR,
    };

}

export const addLayer = (_map, type, id, zIndex) => {

    const map2d = _map.getOlMap();
    const datasetsService = new DatasetsService();

    if (!id) {
        const lm = [
            "5dc96d5d6aa7880018b2ace3",
            "5dca77736aa7880018b2ace4",
            "5dca7b1c6aa7880018b2ace5",
            "5dca7c8c6aa7880018b2ace6",
            "5dca7dc56aa7880018b2ace7",
            "5dca7e966aa7880018b2ace8",
            "5dca7f416aa7880018b2ace9",
            "5dca7fd56aa7880018b2acea",
            "5dca91306aa7880018b2acf2"
        ];

        id = lm[Math.floor(Math.random()*lm.length)];

    }

    return dispatch => {

        dispatch(indicatorRequested());

        datasetsService.getIndicatorById(id)
        .then( res => {

            let layer;

            if (res.ok) {
              type = res.indicator.type;
              const style = res.style;
              // GA
              if (window.ga && res.indicator) {
                  window.ga('send', 'event', 'layer', res.indicator.name);
              };
              
              switch (type) {

                case 'WMS':
                  layer = createWmsLayer(res.indicator, style);
                  break;

                case 'Vector':

                  if (style && style.type === 'heatmap') {
                      layer = buildHeatmapFromVector(res.indicator, style)
                  } else if (style && style.type === 'proportional') {
                      layer = createVectorLayer(res.indicator, style, true);
                  } else {
                      layer = createVectorLayer(res.indicator, style);
                  }

                  break;

                case 'SPARQL':
                    let ind = res.indicator.sparql.source_url;
                    datasetsService.getIndicatorBySparql(
                        ind
                    )
                    .then( s => {
                        const bindings = s.results.bindings;
                        if (style && style.type === 'heatmap') {
                            layer = buildHeatmapFromSparql(res.indicator, bindings, style);
                        } else if (style && style.type === 'proportional') {
                            layer = createSparqlLayer(res.indicator, bindings, style, true, zIndex);
                        } else {
                            layer = createSparqlLayer(res.indicator, bindings, style, false, zIndex);
                        }
                        if (layer) {
                          if (zIndex) {
                              layer.setZIndex(zIndex);
                          }
                          map2d.addLayer(layer)
                          dispatch(
                            addDisplayedLayer(res.indicator, style)
                          );
                        }
                    }).catch( e => {

                      dispatch(showModalCatalog(true));

                    })
                  break;

                default:
                  dispatch(showModalCatalog(true));
                  break;

              }

              if (layer) {
                if (zIndex) {
                    layer.setZIndex(zIndex);
                }
                map2d.addLayer(layer);
                dispatch(
                  addDisplayedLayer(res.indicator, style)
                );
              }

          } else {

              dispatch(showModalCatalog(true));

          }

        })
        .catch( e => {

            dispatch(showModalCatalog(true));

        })

    }


}
