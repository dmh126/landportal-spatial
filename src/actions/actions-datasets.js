import DatasetsService from '../services/datasetsService';
import * as actionTypes from '../constants/actions';

const datasetsApi = new DatasetsService();

export const datasetsRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUEST_DATASETS,
    };
}

export const datasetsReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_DATASETS,
        payload: data
    }

}

export const getDatasets = () => {

    return dispatch => {
        dispatch(datasetsRequested());

        datasetsApi.getDatasets()
        .then( datasets => {

          dispatch(
            datasetsReceived(datasets)
          );

        })
    }
}

export const getDatasetsByTheme = (params) => {

    return dispatch => {
        dispatch(datasetsRequested());

        datasetsApi.getDatasetsByTheme(params)
        .then( datasets => {

          dispatch(
            datasetsReceived(datasets)
          );

        })
    }
}

export const findDatasets = (params) => {

    return dispatch => {
        dispatch(datasetsRequested());
        datasetsApi.findDatasets(params)
        .then( datasets => {

          dispatch(
            datasetsReceived(datasets)
          );

        })
    }
}

export const datasetInfoRequested = () => {

    return {
        type: actionTypes.REQUEST_DATASET_INFO,
    };
}

export const datasetInfoReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_DATASET_INFO,
        payload: data
    }

}

export const getDatasetInfo = (dataset_id) => {

    return dispatch => {
        dispatch(datasetInfoRequested());
        datasetsApi.getDatasetById(dataset_id)
        .then( dataset => {

          dispatch(
            datasetInfoReceived(dataset)
          );

        })
    }
}




export const addDisplayedLayer = (data, style) => {

  return {
    type: actionTypes.ADDED_LAYER,
    payload: {data, style},
  }

}

export const removeDisplayedLayer = (data) => {

  return {
    type: actionTypes.REMOVED_LAYER,
    payload: data,
  }

}

export const updateDisplayedLayer = (data) => {

    return {
            type: actionTypes.UPDATED_LAYER,
            payload: data,
    }

}

export const moveDisplayedLayer = (data) => {

    return {
            type: actionTypes.MOVED_LAYER,
            payload: data,
    }

}

export const selectLayer = (data) => {

  return {
    type: actionTypes.SELECTED_LAYER,
    payload: data,
  }

}

/*

    INDICATORS

*/
export const indicatorsRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUEST_INDICATORS,
    };
}

export const indicatorsReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_INDICATORS,
        payload: data
    }

}

export const getIndicators = (dataset_id, reference, theme) => {

    return dispatch => {
        dispatch(indicatorsRequested());

        datasetsApi.getIndicators(dataset_id, reference, theme)
        .then( indicators => {

          dispatch(
            indicatorsReceived(indicators)
          );

        })
    }
}

export const getIndicatorsAdvanced = (query) => {

    return dispatch => {
        dispatch(indicatorsRequested());

        datasetsApi.getIndicatorsAdvanced(query)
        .then( indicators => {

          dispatch(
            indicatorsReceived(indicators)
          );

        })
    }

}

export const getIndicatorsByTheme = (query) => {

    return dispatch => {
        dispatch(adRequested());

        datasetsApi.getIndicatorsByTheme(query)
        .then( indicators => {

          dispatch(
            indicatorsReceived(indicators)
          );

        })
    }

}

export const getIndicatorsByCountry = (query) => {

    return dispatch => {
        dispatch(advancedRequested());

        datasetsApi.getIndicatorsByCountry(query)
        .then( data => {

          dispatch(
            advancedReceived(data)
          );

        })
    }

}

// example

export const getExampleDataset = () => {

    return dispatch => {
        dispatch(advancedRequested());

        datasetsApi.getExampleDataset()
        .then( datasets => {

          dispatch(
            advancedReceived(datasets)
          );

        })
    }
}


/* ADVANCED SEARCH */

export const advancedRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUEST_ADVANCED,
    };
}

export const advancedReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_ADVANCED,
        payload: data
    }

}

export const advancedSearch = (query) => {
    if (window.ga) {
        window.ga('send', 'event', 'search', query.query);
    };
    return dispatch => {
        dispatch(advancedRequested());

        datasetsApi.getAdvanced(query)
        .then( adv => {

          dispatch(
            advancedReceived(adv)
          );

        })
    }

}
