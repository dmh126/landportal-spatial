import * as actionTypes from '../constants/actions';

export const selectFeature = (data) => {

    return {
        type: actionTypes.FEATURE_SELECTED,
        payload: data
    }

}
