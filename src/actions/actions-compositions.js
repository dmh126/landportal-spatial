import CompositionsService from '../services/compositionsService';
import DatasetsService from '../services/datasetsService';
import * as actionTypes from '../constants/actions';
import {addDisplayedLayer} from './actions-datasets.js';
import async from 'async';
import _ from 'lodash';

import {
  createBaseMap,
  createWmsLayer,
  createVectorLayer,
  createSparqlLayer,
  createHeatmapLayer,
  buildHeatmapFromVector,
  buildHeatmapFromSparql
} from '../components/viewer/helpers.js';

const compositionsApi = new CompositionsService();
const datasetsService = new DatasetsService();

export const compositionCreated = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUESTED_CREATE_COMPOSITION,
    };
}

export const compositionIdReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_CREATE_COMPOSITION,
        payload: data
    }

}

export const createComposition = (params) => {

    return dispatch => {
        dispatch(compositionCreated());
        compositionsApi.createComposition(params)
        .then( composition => {

          dispatch(
            compositionIdReceived(composition)
          );

        })
    }
}

export const compositionRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUESTED_COMPOSITION,
    };
}

export const compositionLoaded = () => {

    return {
        type: actionTypes.LOADED_COMPOSITION,
    };
}

export const getComposition = (id, _map) => {

    const map2d = _map.getOlMap();

    return dispatch => {
        dispatch(compositionRequested());

        compositionsApi.getComposition(id)
        .then( composition => {

            const indicators = _.orderBy(composition.indicators, 'style.zIndex');

            const basemap = (composition.basemap) ? createBaseMap(composition.basemap) : null;
            const extent = composition.extent;
            if (basemap) {
                map2d.getLayers().setAt(0, basemap);
            }
            if (extent) {
                map2d.getView().fit(extent);
            }

            async.mapLimit(indicators, 1, (ind, callback) => {

                const {type, style, visible} = ind;


                datasetsService.getIndicatorById(ind.id)
                .then( res => {

                    let layer;

                    if (res.ok) {

                        const newStyle = {
                            properties: style,
                            type: style.visualization,
                            visible,
                        };
                        const zIndex = style.zIndex;

                        switch (type) {

                          case 'WMS':
                            layer = createWmsLayer(res.indicator);
                            break;

                          case 'Vector':

                            if (style && style.type === 'heatmap') {
                                layer = buildHeatmapFromVector(res.indicator, newStyle)
                            } else if (newStyle && newStyle.type === 'proportional') {
                                layer = createVectorLayer(res.indicator, newStyle, true);
                            } else {
                                layer = createVectorLayer(res.indicator, newStyle);
                            }

                            break;

                          case 'SPARQL':
                              let ind = res.indicator.sparql.source_url;
                              datasetsService.getIndicatorBySparql(
                                  ind
                              )
                              .then( s => {
                                  const bindings = s.results.bindings;
                                  if (newStyle && newStyle.type === 'heatmap') {
                                      layer = buildHeatmapFromSparql(res.indicator, bindings, newStyle);
                                  } else if (newStyle && newStyle.type === 'proportional') {
                                      layer = createSparqlLayer(res.indicator, bindings, newStyle, true);
                                  } else {
                                      layer = createSparqlLayer(res.indicator, bindings, newStyle);
                                  }
                                  if (layer) {
                                    if (zIndex) {
                                        layer.setZIndex(zIndex);
                                    }
                                    map2d.addLayer(layer);
                                    style.legend = layer.get('legend');
                                    //layer.setVisible(visible);
                                    layer.set('composition_visibility', visible);
                                    dispatch(
                                      addDisplayedLayer(res.indicator, newStyle)
                                    );
                                    callback(null, true)
                                  }
                              }).catch( e => {

                                callback(null, false);

                              })
                            break;

                          default:
                            callback(null, false);
                            break;

                        }

                        if (layer) {
                          if (zIndex) {
                              layer.setZIndex(zIndex);
                          }
                          map2d.addLayer(layer);
                          style.legend = layer.get('legend');
                          //layer.setVisible(visible);
                          layer.set('composition_visibility', visible);
                          dispatch(
                            addDisplayedLayer(res.indicator, newStyle)
                          );
                          callback(null, true)
                        }

                    }
                })

            }, (err, result) => {
                dispatch(compositionLoaded());
            })

        })
        .catch( e => {
            console.log(e)
        })

    }



}
