import * as actionTypes from '../constants/actions';
import DatasetsService from '../services/datasetsService';

export const viewerCreated = (data) => {
    // move credits to the right
    //data.getCesiumScene()._creditContainer.style.right = 0;

    return {
        type: actionTypes.VIEWER_CREATED,
        payload: data
    }

}

export const showLayerInfo = (data) => {

    return {
        type: actionTypes.LAYER_INFO_DISPLAY,
        payload: data,
    }

}

export const showLayerSettings = (data) => {

    return {
        type: actionTypes.LAYER_SETTINGS_DISPLAY,
        payload: data,
    }

}

export const showHelp = (data) => {

    return {
        type: actionTypes.HELP_DISPLAY,
        payload: data,
    }

}

export const showAttributesTable = (data) => {

    return {
        type: actionTypes.ATTRIBUTES_TABLE_DISPLAY,
        payload: data,
    }

}

export const showModalCatalog = (data) => {

    return {
        type: actionTypes.MODAL_CATALOG_DISPLAY,
        payload: data,
    }

}

export const showBaseMapPicker = (data) => {

    return {
        type: actionTypes.BASEMAP_PICKER_DISPLAY,
        payload: data,
    }

}

export const showMapLegend = (data) => {

    return {
        type: actionTypes.MAP_LEGEND_DISPLAY,
        payload: data,
    }

}

export const showPanel = (data) => {

    return {
        type: actionTypes.PANEL_DISPLAY,
        payload: data,
    }

}

export const setBasemap = (data) => {

    return {
        type: actionTypes.SET_BASEMAP,
        payload: data,
    }

}

export const showGlobe = (data) => {

    return {
        type: actionTypes.SHOW_GLOBE,
        payload: data,
    }

}

// THEMES AND CONCEPTS
export const themesRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUEST_THEMES,
    };
}

export const conceptsRequested = () => {
    /*
     * Simple async handler
     */
    return {
        type: actionTypes.REQUEST_CONCEPTS,
    };
}

export const themesReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_THEMES,
        payload: data.themes
    }

}

export const conceptsReceived = (data) => {

    return {
        type: actionTypes.RECEIVED_CONCEPTS,
        payload: data.concepts
    }

}

const datasetsApi = new DatasetsService();

export const getThemes = () => {

    return dispatch => {
        dispatch(themesRequested());

        datasetsApi.getThemes()
        .then( themes => {

          dispatch(
            themesReceived(themes)
          );

        })
    }
}

export const getConcepts = () => {

    return dispatch => {
        dispatch(conceptsRequested());

        datasetsApi.getConcepts()
        .then( concepts => {

          dispatch(
            conceptsReceived(concepts)
          );

        })
    }
}
