export const introSteps = [
    {
        element: '#layer-tree-container',
        intro: 'We have preselected one layer on the map, which you can see in this ‘Layers on map’ box. There is room for 3 layers in total.',
        position: 'right',
        highlightClass: 'highlighted-section'
    }, {
        element: '.layer-tree-node',
        intro: 'This box gives you more information about what you’re seeing on the map. You can see the legend, a short description and a link back to the Land Portal to read more about the data.',
        position: 'right',
        highlightClass: 'highlighted-section'
    }, {
        element: '.clear',
        intro: 'If you want to build your own map from scratch, you can ‘clear map’ using this image and start a new.',
        position: 'top',
        highlightClass: 'highlighted-section'
    }, {
        element: '.add-data-btn',
        intro: 'To explore the data we have available on the Land Portal and to add more layers to the map, select this menu item.',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    }, {
        element: '.explore-by',
        intro: 'You can explore the data on the Land Portal by Issue, Dataset or Region. Click on a specific Issue to expand the datasets related to that issue.',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    }, {
        element: '.explore-datasets',
        intro: 'Click on the title of a dataset to explore which layers of this dataset can be added to the map.',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    }, {
        element: '.explore-indicators',
        intro: 'Here you can see the layers within this dataset. You can read a bit more about the layer on the Land Portal, and add it to the map by selecting the Add to map link. You can add up to three layers to the map.',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    }, {
        element: '.settings',
        intro: 'To change the visualization of the layer on the map, select the ‘Change visualization’ icon.',
        position: 'right',
        highlightClass: 'highlighted-section-light'
    }, {
        element: '#layer-settings-container',
        intro: 'You can select different visualization types, different colors and change the opacity of the layer on the map. If applicable to the dataset, you can also select data from a different year.',
        position: 'right',
        highlightClass: 'highlighted-section'
    }, {
        element: '.toggle-panel',
        intro: 'If you want to look at the whole map, you can collapse the layer box to the left of your screen. Don’t worry, you can always pull back the “Layers on Map” legend by uncollapsing using the arrow on the left side of your map.',
        position: 'right',
        highlightClass: 'highlighted-section'
    }, {
        element: '.base-map-picker',
        intro: 'Try out a different map as the background of your data visualizations!',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    },
    {
        element: '.share',
        intro: 'Once you’ve finished the map to your liking, explore the different ways you can share your map with colleagues or friends.',
        position: 'bottom',
        highlightClass: 'highlighted-section'
    },
];

export const introOpts = {
    skipLabel: 'Exit',
    disableInteraction: true,
    exitOnOverlayClick: false,
    showStepNumbers: false,
    tooltipClass: 'tooltip-section',

}
