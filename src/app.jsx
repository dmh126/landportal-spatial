import React, {Component} from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';

import '../styles/index.scss';
import ViewerContainer from './components/viewer/ViewerContainer';


export default class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={ViewerContainer} />
        </div>
      </Router>
    )
  }
}
