import Services from './Services';

 export default class DatasetsService extends Services {

   getDatasets() {

     return this.http.get('/api/v1/datasets')
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getDatasetById(id) {

     return this.http.get(`/api/v1/dataset/${id}`)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getIndicatorsByTheme(params) {

     const opts = {
         headers: {
             "Content-Type": "application/json"
         }
     }

     return this.http.post(`/api/v1/indicators/theme`, params, opts)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getDatasetsByTheme(params) {

     const opts = {
         headers: {
             "Content-Type": "application/json"
         }
     }

     return this.http.post(`/api/v1/find/datasets/theme`, params, opts)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getIndicatorsByCountry(params) {

     const opts = {
         headers: {
             "Content-Type": "application/json"
         }
     }

     return this.http.post(`/api/v1/indicators/country`, params, opts)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   findDatasets(params) {

     const opts = {
         headers: {
             "Content-Type": "application/json"
         }
     }

     return this.http.post(`/api/v1/datasets`, params, opts)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getIndicators(dataset_id, reference, theme) {

       let url = `/api/v1/indicators/dataset/${dataset_id}`;

       if(reference) {
           url = url + `?reference=${reference}`;
       }
       if(theme) {
           url = url + `?search=${theme}`;
       }

       return this.http.get(url)
        .then( data => {

          return data.json()

        })
        .catch( error => {
          return {
            error: true,
            message: "Something is wrong.",
          }
        })

   }

   getIndicatorsAdvanced(query) {

       const opts = {
           headers: {
               "Content-Type": "application/json"
           }
       };

       return this.http.post(`/api/v1/indicators/advanced`, query, opts)
        .then( data => {

          return data.json()

        })
        .catch( error => {
          return {
            error: true,
            message: "Something is wrong.",
          }
        })

   }

   getAdvanced(query) {

       const opts = {
           headers: {
               "Content-Type": "application/json"
           }
       };

       return this.http.post(`/api/v1/indicators/advanced`, query, opts)
        .then( data => {

          return data.json()

        })
        .catch( error => {
          return {
            error: true,
            message: "Something is wrong.",
          }
        })

   }

   getIndicatorById(id) {

     return this.http.get(`/api/v1/indicator/${id}`)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getIndicatorBySparql(id) {
       return this.http.get(`/api/v1/sparql/${id}`)
        .then( data => {

          return data.json()

        })
        .catch( error => {
          return {
            error: true,
            message: "Something is wrong.",
          }
        })
   }

   getExampleDataset() {

       return this.http.get(`/api/v1/example/dataset`)
        .then( data => {

          return data.json()

        })
        .catch( error => {
          return {
            error: true,
            message: "Something is wrong.",
          }
        })

   }


   getThemes() {

     return this.http.get(`/api/v1/themes`)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   getConcepts() {

     return this.http.get(`/api/v1/concepts`)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }


}
