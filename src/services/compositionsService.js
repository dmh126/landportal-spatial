import Services from './Services';

 export default class CompositionsService extends Services {

   getComposition(id) {

     return this.http.get(`/api/v1/compositions/${id}`)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

   createComposition(params) {

     const opts = {
         headers: {
             "Content-Type": "application/json"
         }
     }

     return this.http.post(`/api/v1/composition/new`, params, opts)
      .then( data => {

        return data.json()

      })
      .catch( error => {
        return {
          error: true,
          message: "Something is wrong.",
        }
      })

   }

}
