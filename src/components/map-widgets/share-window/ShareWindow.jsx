import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';
import {FaCamera, FaLink, FaCode} from 'react-icons/fa';
import {compositionIdReceived} from '../../../actions/index';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {saveAs} from 'file-saver';

import './style.scss';

class ShareWindow extends Component {

    constructor() {
        super();

        this.state = {
            copied: false,
        }
    }

    copyToClipboard = (n) => {
        let link = `${__HOST_URL__}/?map=${this.props.show}`
        let iframe = `<iframe src="${link}"></iframe>`

        if (n === 1) {
            return link;
        } else {
            return iframe;
        }

    }

    captureImage = () => {

        const _map = this.props.viewer.getOlMap();

        if (_map) {

            _map.once('rendercomplete', e => {
                let canvas = e.context.canvas;

                if (navigator.msSaveBlob) {

                    navigator.msSaveBlob(canvas.msToBlob(), 'geoportal_landportal_export.png');

                } else {
                    canvas.toBlob( blob => {
                        saveAs(blob, 'geoportal_landportal_export.png');
                    });
                }

            });

            _map.renderSync();

        }

    }

    handleCopy = () => {
        this.setState({copied: true});
        setTimeout(() => this.setState({copied: false}), 1500 );
    }


    render() {

        if (this.props.show) {

            let link = `${__HOST_URL__}/?map=${this.props.show}`
            let iframe = `<iframe src="${link}" height="680"  width="1000"></iframe>`

            return (
                <div className="share-window">
                    <div className="share-window-entry">
                        <div className="share-window-entry-text">
                            <label><FaCamera /> <span className="share-window-entry-download" onClick={() => this.captureImage()}>Download picture</span></label>
                        </div>
                    </div>
                    <div className="share-window-entry">
                        <div className="share-window-entry-text">
                            <CopyToClipboard  text={this.copyToClipboard(1)} onCopy={this.handleCopy}>
                                <label><FaLink /> <span className="share-window-entry-link">Link to page</span></label>
                            </CopyToClipboard>
                            <div>
                                <small>{this.state.copied ? 'Copied to clipboard!' : ''}</small>
                            </div>
                        </div>
                    </div>
                    <div className="share-window-entry">
                        <div className="share-window-entry-text">
                            <label><FaCode /> Embed on website</label>
                            <input type="text" id="share-iframe" defaultValue={iframe} />
                            <CopyToClipboard  text={this.copyToClipboard(2)}>
                                <span className="share-window-entry-copy">
                                    copy
                                </span>
                            </CopyToClipboard>
                        </div>
                    </div>
                </div>
            )

        } else {
            return null;
        }

    }

}

function mapStateToProps(state) {

    return {
        show: state.compositions.composition,
        viewer: state.viewer.viewer
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        compositionIdReceived: compositionIdReceived,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ShareWindow);
