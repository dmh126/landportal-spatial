import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';

import {selectFeature} from '../../../actions/index';

import './style.scss';

const properties_map = {
    'name': 'country',
    'name_2': 'geography',
    'name_1': 'geography',
    'name_0': 'country',
    //'iso_a3': 'code',
    //'region_un': 'continent',
    //'subregion': 'subregion',
    'indicator_': 'indicator ',
    'indicator': 'indicator'
}

class FeatureInfo extends Component {

    constructor() {

        super();

        this.state = {
            selected_feature: null
        }

    }

    renderTable(feature) {

        let values = feature.getProperties();

        let entries = Object.entries(values);

        return (
            <div className="selected-feature-body">
                {
                    entries.map(([key, value], index) => {
                        if (typeof value !== 'object') {
                            //console.log(properties_map[key])
                            if (properties_map[key] || key.includes('indicator_')) {
                                return (
                                    <div className="selected-feature-entry" key={index}>
                                        <div className="selected-feature-field">{properties_map[key] || key.replace('_', ' ')}</div>
                                        <div className="selected-feature-value">{value.toLocaleString() || <i>null</i>}</div>
                                    </div>
                                )
                            } else {
                                return null;
                            }
                        } else {
                            return null;
                        }
                    })
                }
            </div>
        )
    }

    render() {
        const feature = this.props.selected_feature
        let position = feature ? {top: feature.position.top, left: feature.position.left} : {} ;

        return (feature) ?
            (<Draggable cancel=".selected-feature-body">
                <div className="selected-feature-info" style={position}>
                    <div className="selected-feature-head">
                        Feature Info
                        <span
                            onClick={() => this.props.selectFeature(null)}
                            >X</span>
                    </div>
                    {this.renderTable(feature)}
                </div>
            </Draggable>) : null
    }
}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        selected_feature: state.features.selected_feature,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectFeature: selectFeature
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(FeatureInfo);
