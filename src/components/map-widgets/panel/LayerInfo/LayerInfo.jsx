import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';
import {FaExternalLinkAlt} from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';

import GeoJSON from 'ol/format/GeoJSON.js';

import {selectLayer, showLayerInfo, showLayerSettings} from '../../../../actions/index';
import {sortStringsFirst} from '../../../../helpers/index';

import LayerEntry from './LayerEntry';
import Resizer from '../../../resizer/Resizer';

import './style.scss';

class LayerInfo extends Component {

    componentWillUnmount() {
        this.props.selectLayer(null);
    }

    componentDidUpdate() {
        ReactTooltip.hide();
    }

    downloadlayer = () => {

        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        const format = new GeoJSON({featureProjection: 'EPSG:3857'});
        const features = layer.getSource().getFeatures();
        const json = format.writeFeatures(features);

        return 'data:text/json;charset=utf-8,' + json;


    }

    renderField = (field, value, index, nested, toSkip) => {

        if (typeof value === 'string') {

            return (<LayerEntry field={field} value={value} key={index} nested={nested}/>)

        } else if (typeof value === 'object' && value !== null) {

            let entries = Object.entries(value);
            // structure: indicator (obj) -> meta (obj) -> indicator (obj) -> metadata k:v
            // should be simplified by skipping this 'indicator' entry
            // remove empty containers too
            let toSkip_next = false;
            if (entries.length === 1) {
                if (typeof entries[0][1] === 'object') {
                    toSkip_next = true;
                }
            }
            if (entries.length === 0) {
                toSkip = true;
            }

            return (
                <div className="layer-info-group" key={index}>
                    { !toSkip && <div className="layer-info-group-name">{field}</div>}
                    <div className="layer-info-group-content">
                        {
                            entries.map(([
                                field, value
                            ], index) => {
                                return this.renderField(field, value, index, true, toSkip_next);
                            })
                        }
                    </div>
                </div>
            )

        } else {

            return null;

        }

    }

    renderTable(data) {

        if (data) {
          data.meta.type = data.type;
            return (
                    <div id="layer-info-container">
                        <div className="layer-info-header">
                            Layer Info
                            <span onClick={this.closeTable}>X</span>
                        </div>
                        <div className="layer-info-body">
                            {
                                /*Object.entries(data).sort(sortStringsFirst).map(([
                                    field, value
                                ], index) => {
                                    return this.renderField(field, value, index, false, false);
                                })*/
                                this.renderInfoCard(data.meta, data.dataset)
                            }
                        </div>
                        {/*<Resizer container={"layer-info-container"} />*/}
                    </div>
            )

        } else {
            return null
        }

    }

    renderInfoCard(meta, dataset) {

        if (meta) {

            let toDisplay;
            let picture = meta.indicator.picture;
            if (picture && (typeof meta.indicator.picture === 'string')){
                toDisplay = picture;
            } else if (picture && picture.data) {
                toDisplay = `data:${picture.contentType};base64, ${picture.data.toString('base64')}`
            }

            return (
                <div className="layer-info-card">
                    <div className="layer-info-card-top">
                        {/*<div className="layer-info-card-image">
                            <img src={toDisplay} />
                        </div>*/}
                        <div className="layer-info-card-meta">
                            {meta.indicator.label && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">Label</div>
                                <div className="layer-info-card-entry-value">{meta.indicator.label}
                                {
                                    (meta.type === 'SPARQL' && meta.indicator.id) &&
                                    <a href={`https://landportal.org/book/indicator/${meta.indicator.id.split('.').join("")}`} target="_blank">
                                        <FaExternalLinkAlt />
                                    </a>
                                }
                                </div>
                            </div>}
                            {meta.indicator.description && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">Description</div>
                                <div className="layer-info-card-entry-value">{meta.indicator.description}</div>
                            </div>}
                            {dataset.name && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">Dataset</div>
                                <div className="layer-info-card-entry-value">{dataset.name}
                                {
                                    (meta.type === 'SPARQL' && meta.indicator.dataset) &&
                                    <a href={`http://landportal.org/book/datasets/${meta.indicator.dataset.split('.').join("")}`} target="_blank">
                                        <FaExternalLinkAlt />
                                    </a>
                                }
                                </div>
                            </div>}
                            {dataset.meta.dataset.organization && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">Organization</div>
                                <div className="layer-info-card-entry-value">{dataset.meta.dataset.organization}
                                {
                                    (meta.type === 'SPARQL' && dataset.meta.dataset.organizationUri) &&
                                    <a href={dataset.meta.dataset.organizationUri} target="_blank">
                                        <FaExternalLinkAlt />
                                    </a>
                                }
                                </div>
                            </div>}
                            {meta.indicator.measurement_unit && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">Measurement unit</div>
                                <div className="layer-info-card-entry-value">{meta.indicator.measurement_unit}</div>
                            </div>}

                            {dataset.meta.dataset.license && <div className="layer-info-card-entry">
                                <div className="layer-info-card-entry-key">License info</div>
                                <div className="layer-info-card-entry-value">
                                    {
                                        (meta.type === 'SPARQL' && dataset.meta.dataset.license) &&
                                        <a href={dataset.meta.dataset.license} target="_blank">
                                            <FaExternalLinkAlt />
                                        </a>
                                    }
                                </div>
                            </div>}

                            {this.renderTags(meta.indicator)}

                            {
                                (["Vector", "SPARQL"].includes(this.props.selected_layer.type)) && this.renderDownload()
                            }
                        </div>
                    </div>
                </div>
            )
        }
    }

    renderDownload = () => {

        const layer = this.props.selected_layer;
        const type = layer.type;
        let source_url;
        if (type === 'Vector') {
            source_url = layer.vector.source_url;

            const formats_vector = {
                "Shapefile": "SHAPE-ZIP",
                "JSON": "application/json",
                "GML": "gml3",
                "CSV": "csv"
            };

            return (
                <div className="layer-info-card-entry">
                    <div className="layer-info-card-entry-key">Download</div>
                    {
                        Object.entries(formats_vector).map( ([k,v], index) => {
                            const url = new URL(source_url);
                            url.searchParams.set("outputFormat", v);
                            return (<a key={index} className="layer-info-card-download" href={url.toString()} target="_blank">{k}</a>)
                        })
                    }
                </div>
            )

        } else if (type === 'SPARQL') {
            source_url = layer.sparql.source_url;

            const formats_sparql = {
                "JSON": "json",
                "CSV": "csv",
                "XML": "xml"
            };

            return (
                <div className="layer-info-card-entry">
                    <div className="layer-info-card-entry-key">Download</div>
                    {
                        Object.entries(formats_sparql).map( ([k,v], index) => {
                            const url = new URL(`${__API_URL__}/api/v1/sparql/${source_url}?format=${v}&mode=download`);
                            return (<a key={index} className="layer-info-card-download" href={url.toString()} target="_blank">{k}</a>)
                        })
                    }
                </div>
            )
        } else {
            return null;
        }



    }

    renderTags(indicator) {
        const themes = indicator.related_themes.split(';').filter(i => i);
        const concepts = indicator.related_landvoc_concepts.split(';').filter(i => i);
        if (themes.length || concepts.length) {
            return (
                <div className="layer-info-card-entry">
                    <div className="layer-info-card-entry-key">Landvoc</div>
                    <div className="layer-info-card-landvoc">
                        <div>
                        {
                            themes.map((item, index) => {
                                return <div className="tag-theme" key={index}>{item}</div>
                            })
                        }
                        </div>
                        <div>
                        {
                            concepts.map((item, index) => {
                                return <div className="tag-concept" key={index}>{item}</div>
                            })
                        }
                        </div>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }


    closeTable = () => {
        if (!this.props.layer_settings && !this.props.attributes_table) {
            this.props.selectLayer(null);
        }
        this.props.showLayerInfo(false);
        //this.props.showLayerSettings(false);

    }

    render() {

        if (this.props.selected_layer && this.props.show) {
            return this.renderTable(this.props.selected_layer);
        } else {
            return null;
        }

    }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        selected_layer: state.datasets.selected_layer,
        show: state.viewer.layer_info,
        layer_settings: state.viewer.layer_settings,
        attributes_table: state.viewer.attributes_table,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectLayer: selectLayer,
        showLayerInfo: showLayerInfo,
        showLayerSettings: showLayerSettings,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LayerInfo);
