import React, {Component} from 'react';


class LayerEntry extends Component {


  render() {

    const {index, field, value, nested} = this.props;

    return (
      <div className={nested ? "layer-info-entry nested" : "layer-info-entry"}>
        <div className="layer-info-field">{field}</div>
        <div className="layer-info-value">{value}</div>
      </div>
    )
  }

}

export default LayerEntry;
