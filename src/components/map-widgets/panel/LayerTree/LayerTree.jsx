import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {showModalCatalog, showPanel, removeDisplayedLayer} from '../../../../actions/index.js';

import {FaDatabase, FaAngleUp, FaAngleDown, FaLayerGroup} from 'react-icons/fa';

import LayerNode from './LayerNode';
import Bar from '../../controls/Bar';

import './style.scss';

class LayerTree extends Component {

  constructor(){

    super();

    this.state = {
      layers: [],
    }

  }

  toggleLayers = () => {

      this.props.showPanel(!this.props.show);
  }

  clearMap = () => {

      const {displayed_layers, viewer} = this.props;
      const _map = viewer.getOlMap();
      const layers = _map.getLayers();
      for (let i=displayed_layers.length; i > 0; i--) {
          const item = displayed_layers[i-1];
          try {
              layers.removeAt(i);
              this.props.removeDisplayedLayer(item._id);
          } catch (err) {
              _map.render();
          };
      }
      /*
      displayed_layers.forEach((item, index) => {
          try {
              console.log(item, index)
              //console.log(layers)
              layers.removeAt(index + 1);
          } catch (err) {
              console.log(err)
              _map.render();
          }
          //this.props.removeDisplayedLayer(item._id);
      });*/

  }

  render() {

    const len = this.props.displayed_layers.length;

    return (
      <div id='layer-tree-container'>
        <div className="layer-tree-header">
            Layers on map
            {(len != 0) && <span className="clear" onClick={this.clearMap}>clear</span>}
            <span className="icon"><FaLayerGroup /></span>
        </div>
        {
          this.props.show &&
          this.props.displayed_layers.map((item, index) => {
            const newItem = {...item, ...{index: (len - index)}}

            return (
                <LayerNode
                    item={newItem}
                    key={item._id}
                    />
            )

          })
        }
        {
            Array(3-len).fill(0).map( (item, index) => {
                return (
                    <div
                        key={index}
                        className="layer-tree-empty">
                        {len + 1 + index}. Empty slot. Select a layer and add it to the map.
                    </div>
                )
            })
        }

        {/*
            (this.props.show && (len === 0)) && <div className="layer-tree-empty" onClick={() => this.props.showModalCatalog(true)}><FaDatabase /> Add layers...</div>
        */}

    </div>
    )
  }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        show: state.viewer.panel,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showModalCatalog,
        showPanel,
        removeDisplayedLayer
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LayerTree);
