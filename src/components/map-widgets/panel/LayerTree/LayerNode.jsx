import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import _ from 'lodash';
import {FaCaretUp, FaCaretDown} from 'react-icons/fa';
import {MdSettings, MdInfoOutline, MdDelete, MdViewList, MdVisibility, MdVisibilityOff} from 'react-icons/md';

import {
    selectLayer,
    showLayerInfo,
    showLayerSettings,
    removeDisplayedLayer,
    moveDisplayedLayer,
    showAttributesTable,
    updateDisplayedLayer
} from '../../../../actions/index';

import {RAMPS} from '../../../../constants/ramps';
import LayerInfo from '../LayerInfo/LayerInfo';
//import LayerSettings from '../LayerSettings/LayerSettings';

import './style.scss';

class LayerNode extends Component {

    constructor(props) {

        super();

        this.state = {
            visible: true,
            legend: null,
            index: props.item.index,
        }

    }

    componentDidMount() {
        this.intervalId = setInterval(() => {
            this.getLegend()
        },1000)
    }

    componentDidUpdate() {

    }

    componentWillReceiveProps(nextProps) {
        this.getLegend(nextProps.item.index);
    }

    changeVisibility = (e) => {

        let visible = e.target.checked;
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.item.index];
        layer.setVisible(visible);
        this.setState({visible: visible});
        _map.render();
    }

    layerInfo = () => {

        const already_selected = this.props.selected_layer;

        const selected = this.props.displayed_layers.find(d => d._id === this.props.item._id);

        if (selected) {
            if (already_selected) {
                if (already_selected._id === selected._id) {
                        if (this.props.layer_info) {
                            this.props.showLayerInfo(false);
                        } else {
                            this.props.showLayerInfo(this.props.item.index);
                        }
                        if (!this.props.layer_settings && !this.props.attributes_table) {
                            this.props.selectLayer(null);
                        }
                }
            } else {
                this.props.selectLayer(selected);
                this.props.showLayerInfo(this.props.item.index);
            }
        };


    }

    layerSettings = () => {

        const already_selected = this.props.selected_layer;

        const selected = this.props.displayed_layers.find(d => d._id === this.props.item._id);

        if (selected) {
            if (already_selected) {
                if (this.props.layer_settings) {
                    this.props.showLayerSettings(false);
                } else {
                    this.props.showLayerSettings(this.props.item.index);
                }
                if (!this.props.layer_info && !this.props.attributes_table) {
                    this.props.selectLayer(null);
                }
            } else {
                this.props.selectLayer(selected);
                this.props.showLayerSettings(this.props.item.index);
            }
        };


    }

    renderDownload = () => {

        const layer = this.props.item;
        const type = layer.type;
        let source_url;
        if (type === 'Vector') {
            source_url = layer.vector.source_url;

            const formats_vector = {
                "Shapefile": "SHAPE-ZIP",
                "JSON": "application/json",
                "GML": "gml3",
                "CSV": "csv"
            };

            return (
                <div className="layer-tree-node-download">
                    Download
                    <span>
                        {
                            Object.entries(formats_vector).map( ([k,v], index) => {
                                const url = new URL(source_url);
                                url.searchParams.set("outputFormat", v);
                                return (<a key={index} className="layer-info-card-download" href={url.toString()} target="_blank">{k}</a>)
                            })
                        }
                    </span>
                </div>
            )

        } else if (type === 'SPARQL') {
            source_url = layer.sparql.source_url;

            const formats_sparql = {
                "JSON": "json",
                "CSV": "csv",
                "XML": "xml"
            };

            return (
                <div className="layer-tree-node-download">
                    Download
                    <span>
                        {
                            Object.entries(formats_sparql).map( ([k,v], index) => {
                                const url = new URL(`${__API_URL__}/api/v1/sparql/${source_url}?format=${v}&mode=download`);
                                return (<a key={index} className="layer-info-card-download" href={url.toString()} target="_blank">{k}</a>)
                            })
                        }
                    </span>
                </div>
            )
        } else {
            return (<div className="layer-tree-node-download"></div>);
        }
    }

    removeLayer = (item) => {
        const r = confirm('Are you sure you want to remove this layer?');
        if (!r) {
            return 0
        }
        this.props.showLayerSettings(false);
        this.props.selectLayer(null);
        const _map = this.props.viewer.getOlMap();
        const layers = _map.getLayers();
        // for the heatmap - there is an error because the source has been changed - to investigate
        try {
            layers.removeAt(item.index);
        } catch (err) {
            //console.log(err);
            _map.render();
        }
        //layers.removeAt(item.index);
        this.props.removeDisplayedLayer(item._id);

    }

    moveLayer = (dir, index) => {

        if (dir === 1 && index === this.props.displayed_layers.length ) {
            return null;
        }

        if (dir === -1 && index === 1 ) {
            return null;
        }

        const newIndex = index + dir;
        const _map = this.props.viewer.getOlMap();
        const layers = _map.getLayers().array_;
        [layers[index], layers[newIndex]] = [layers[newIndex], layers[index]];
        const moved = layers[newIndex];
        const neighbour = layers[index];
        moved.setZIndex(newIndex);
        neighbour.setZIndex(index);
        this.props.moveDisplayedLayer({dir, index});

    }

    getLegend = (ind) => {
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[ind || this.props.item.index];
        const legend = layer.get('legend');

        if (legend) {
            let v = layer.get('composition_visibility');
            let visible;
            if (typeof v === 'boolean') {
                visible = v;
                layer.setVisible(visible);
                layer.set('composition_visibility', undefined);
            } else {
                visible = layer.getVisible();
            }
            this.setState({legend, visible});
            clearInterval(this.intervalId);
        }
    }

    renderGraduatedLegend = (legend, background, unit) => {
        let isText;
        let lt5;
        if (legend) {
            isText = (typeof legend.jenks[0] === 'string');
            lt5 = (legend.jenks.length < 3);

        }

        return (
            <div className="layer-tree-node-legend">
                <div className="layer-tree-node-header">Legend</div>
                <div className="layer-tree-node-ramp">
                    {
                        legend && legend.ramp.map((item, index) => {
                            let value;
                            if (isText) {
                                value = this.nFormatter(legend.jenks[index]);
                            } else {
                                value = this.nFormatter( lt5 ? legend.jenks[index] : legend.jenks[index + 1])
                            }
                            return (
                                <div className="legend-node" key={index}>
                                    <div className="legend-node-color" style={{background: item}}></div>
                                    <div className="legend-node-value">{value} {<span>{unit}</span>}</div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }

    renderHeatmapLegend = (legend, background, unit) => {

        return (
            <div className="layer-tree-node-legend">
                <div className="layer-tree-node-header">Legend</div>
                <div className="layer-tree-node-symbol" >
                    <div className="heatmap"></div>
                </div>
                <div className="legend-node-value">
                    <div className="layer-tree-node-min">Min: {this.nFormatter(legend.min)} {unit}</div>
                    <div className="layer-tree-node-min">Max: {this.nFormatter(legend.max)} {unit}</div>
                </div>
            </div>
        )
    }

    renderProportionalLegend = (legend, background, unit) => {


        const shapeClass = legend ? legend.shape : 'circle';
        const minValue = legend ? legend.jenks[0] : 0;
        const maxValue = legend ? legend.jenks[legend.jenks.length - 1] : 'Nan';

        const style = {};

        if (shapeClass === 'triangle') {
            style.borderBottom = `25px solid ${legend.color}`;
            style.background = 'transparent';
        } else {
            style.background = legend ? legend.color : '#00ff00';
        }

        return (
            <div className="layer-tree-node-legend">
                <div className="layer-tree-node-header">Legend</div>
                <div className="layer-tree-node-symbol" >
                    <div className={shapeClass} style={style}></div>
                </div>
                <div className="layer-tree-node-min">Min: {this.nFormatter(minValue)} {unit}</div>
                <div className="layer-tree-node-max">Max: {this.nFormatter(maxValue)} {unit}</div>
            </div>
        )
    }

    renderLegendEntry = (visualization, legend, background, unit) => {

        if (visualization === 'graduated') {
            return legend ? this.renderGraduatedLegend(legend, background, unit) : null;
        } else if (visualization === 'proportional') {
            return legend ? this.renderProportionalLegend(legend, background, unit) : null;
        } else if (visualization === 'heatmap') {
            return legend ? this.renderHeatmapLegend(legend, background, unit) : null;
        }

    }

    renderMore = (item) => {
        const id = item.meta.indicator.id;
        let url;
        if (id) {
            url = `http://landportal.org/book/indicator/${id.split('.').join("")}`;
        };
        if (id) {
            return (
                <div
                    className="layer-tree-node-more"
                    onClick={() => window.open(url)}
                    >Read more on Land Portal</div>
            )
        } else {
            return null;
        }

    }

    renderTime = () => {
        const {item} = this.props;
        const _map = this.props.viewer.getOlMap();

        const layer = _map.getLayers().array_[item.index];
        const time = layer.get('time');
        const style = item.style;
        const t1 = style.time || time || false;

        if (!['SPARQL', 'Vector'].includes(item.type)) return "";
        if (t1) return `Year ${t1}`;
        return "";

        /*
        const features = layer.getSource().getFeatures();
        let values = Array.from(new Set(_.flatten(features.map( feature => {
            let t = feature.get('times');
            if (t) {
                return t.split(';')
            } else {
                let times = feature.getKeys()
                .filter(k => k.includes('indicator'))
                .map(k => k.split('_')[1]).filter(k => k).sort();
                return times;
            }
        })).filter( el => el))).sort().reverse();

        if (values.length) {
            return (
                <select className="time-switch" onChange={this.changeTime}>
                    {
                        values.map( (item, index) => {
                            return (
                                <option key={index} value={item}>{item}</option>
                            );
                        })
                    }
                </select>
            )
        } else {
            if (t1) return `Year: ${values}`;
            return "";
        }*/

    }

    changeTime = (e) => {

        const {value} = e.target;

        const style = this.props.item.style;

        if (style.visualization === 'heatmap') {
            /*const _map = this.props.viewer.getOlMap();
            const layer = _map.getLayers().array_[this.props.show];
            const newLayer = createHeatmapLayer(layer, "indicator_" + value);
            _map.getLayers().array_[this.props.show] = newLayer;
            newLayer.on("change", (e) => {
                _map.render();
            });
            this.setState({time: value});
            this.updateStyle({time: value});*/
        } else {

            //this.setState({time: value});
            this.updateStyle({time: value});
            this.setColors(style, value, style.label);

        }
    }

    updateStyle = (style) => {
        const {item, viewer} = this.props;
        const _map = viewer.getOlMap();
        const layer = _map.getLayers().array_[item.index];
        const id = layer.get("layer_id");
        const _obj = {
            _id: id,
            style: style
        };
        this.props.updateDisplayedLayer(_obj);

    }

    nFormatter(num) {

         const isNum = (typeof num === 'number');

         if (!isNum) {
             return num;
         }

         if (num >= 1000000000) {
            return '< ' + (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
         }
         if (num >= 1000000) {
            return '< ' + (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
         }
         if (num >= 1000) {
            return '< ' + (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
         }
         return '< ' + num.toFixed(1);
    }

    render() {
        let expand;
        const {item, selected_layer, layer_settings} = this.props;
        let unit = item.meta.indicator.measurement_unit;
        if (item && selected_layer) {
            expand = (item._id === selected_layer._id)
        }

        const ramp = RAMPS.find( r => r.value === this.props.item.style.palette);
        let background;
        if (ramp) {
            let palette = ramp.palette.join(',');
            background = `linear-gradient( 90deg, ${palette})`;
        } else {
            background = '#a9a9a9';
        }

        const visualization = this.props.item.style.visualization;
        if (visualization === "heatmap") {
            background = 'linear-gradient( 90deg, #0f0, #ff0, #f00)'
        }
        if (visualization === "proportional") {
            background = this.props.item.style.color;
        }

        const table = ["Vector", "SPARQL"].includes(this.props.item.type);

        const legend = this.state.legend;

        const settings_displayed = selected_layer ? (selected_layer._id === item._id) && layer_settings : false;

        return (
            <div>
            <div className="layer-tree-node">
                    {this.renderLegendEntry(visualization, legend, background, unit)}
                    <div className="layer-tree-node-name" title={this.props.item.name}>
                        {this.props.item.name}
                    </div>
                    <div className="layer-tree-node-buttons">
                        <div
                            className="layer-tree-node-icon settings"
                            style={{color: settings_displayed ? "#f69a2d" : "#2d2d2d"}}
                            onClick={this.layerSettings}
                            title="Change data visualization">
                            <MdSettings />
                        </div>
                        <div className="layer-tree-node-icon" onClick={() => this.removeLayer(this.props.item)} title="Remove">
                            <MdDelete />
                        </div>
                        <div className="layer-tree-node-visibility" title="Visibility: On-Off">
                            <input className="visibility-checkbox" id={"visibility-checkbox-" + this.props.item.index} type="checkbox" onChange={(e) => this.changeVisibility(e)} defaultChecked={this.state.visible} />
                            <label htmlFor={"visibility-checkbox-" + this.props.item.index}>
                                {(this.state.visible) ? <MdVisibility /> : <MdVisibilityOff />}
                            </label>
                        </div>
                    </div>
                    <div className="layer-tree-node-info">
                        <div className="layer-tree-node-time">{this.renderTime()}</div>
                        <div className="layer-tree-node-about">{this.props.item.about}</div>
                        {this.renderMore(this.props.item)}
                        {this.renderDownload()}
                    </div>
                    <div className="layer-tree-node-move">
                        <div
                            style={{color: (this.props.item.index != this.props.displayed_layers.length) ? '#000' : '#d3d3d3'}}
                            title={(this.props.item.index != this.props.displayed_layers.length) ? "Move up" : null}
                            >
                            <FaCaretUp onClick={()=> this.moveLayer(1, this.props.item.index)} />
                        </div>
                        <div
                            style={{color: (this.props.item.index != 1) ? '#000' : '#d3d3d3'}}
                            title={(this.props.item.index != 1) ? "Move down" : null}
                            >
                            <FaCaretDown onClick={()=> this.moveLayer(-1, this.props.item.index)} />
                        </div>
                    </div>
            </div>
            </div>
        );

    }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        selected_layer: state.datasets.selected_layer,
        layer_settings: state.viewer.layer_settings,
        layer_info: state.viewer.layer_info,
        attributes_table: state.viewer.attributes_table,

        show: state.viewer.panel,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectLayer,
        showLayerInfo,
        showLayerSettings,
        removeDisplayedLayer,
        moveDisplayedLayer,
        showAttributesTable,
        updateDisplayedLayer
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LayerNode);
