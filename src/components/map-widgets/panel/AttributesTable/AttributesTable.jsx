import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';

import {selectLayer, showLayerInfo, showLayerSettings, showAttributesTable} from '../../../../actions/index';
import {sortStringsFirst} from '../../../../helpers/index';

import './style.scss';

class AttributesTable extends Component {

    constructor(props) {
        super();

        this.state = {
            limit: 15,
            showHidden: true,
        }

    }

    componentWillUnmount() {
        this.props.selectLayer(null);
    }

    componentWillReceiveProps() {

        if (!(this.props.selected_layer && this.props.show)) {
            this.setState({
                limit: 15,
            })
        }
    }

    loadMore = (e) => {
        const elem = e.target;
        const end = (elem.scrollTop + elem.offsetHeight) > (elem.scrollHeight - 5)
        if (end) {
            let c = this.state.limit;
            this.setState({
                limit: c + 15,
            })
        }
    }

    displayHiddenObjects = () => {

        let value = !this.state.showHidden;
        this.setState({
            showHidden: value,
        });

    }

    renderTableHead(headers) {

        return (
            <thead>
                <tr>
                    {
                        headers.map( (item, index) => {
                            let name;
                            let s = item.split('_');
                            if (s[0] === 'indicator'){
                                name = s[1] ? `Value in ${s[1]}` : 'Value';
                            } else if (s[0].toLowerCase() === 'name') {
                                if (!s[1] || s[1] === '0') {
                                    name = 'Country';
                                } else {
                                    name = 'Geography';
                                }
                            }
                            return (<th key={index}>{name}</th>)

                        })
                    }
                </tr>
            </thead>
        )

    }

    renderTableBody(features, headers) {

        return (
            <tbody>
                {
                    features
                    .filter( i => i.get("indicator"))
                    .sort((a,b) => (parseFloat(a.get('id')) - parseFloat(b.get('id'))))
                    .map( (feature, index) => {

                            return (
                                <tr key={index}>
                                    {
                                        headers.map( (field, index) => {

                                            const value = feature.get(field);

                                            return <td key={index}>{value || "-"}</td>
                                        })
                                    }
                                </tr>
                            );

                    })
                }
            </tbody>
        )

    }

    renderFeaturesTable(layer) {

        let headers = [];

        const features = layer.getSource().getFeatures();

        features.map( feature => {

            //let keys = feature.getKeys().filter( k => !["bbox", "geometry", "time", "times", "indicators"].includes(k));
            const attrs = ["name", "indicator"];
            let keys = feature.getKeys().filter( k => {
                return attrs.some(i => k.includes(i));
            });

            headers = Array.from(new Set([...headers, ...keys]));

            // remove redundant
            let has_time = headers.some(i => i.includes('indicator_'));
            if (has_time) {
                headers = headers.filter( i => i !== 'indicator')
            }


        });

        return (
            <table>
                {this.renderTableHead(headers)}
                {this.renderTableBody(features, headers)}
            </table>
        )

    }


    renderTable(data) {

        if (data) {
            const _map = this.props.viewer.getOlMap();
            const layer = _map.getLayers().array_[this.props.show];
            return (
                <Draggable cancel=".attributes-table-body">
                    <div id="attributes-table">
                        <div className="attributes-table-header">
                            Attributes table
                            <span className="close" onClick={this.closeTable}>X</span>
                        </div>
                        <div className="attributes-table-body" onScroll={(e) => this.loadMore(e)}>
                            {
                                this.renderFeaturesTable(layer)
                            }
                        </div>
                    </div>
                </Draggable>
            )

        } else {
            return null
        }

    }



    closeTable = () => {
        if (!this.props.layer_settings && !this.props.layer_info) {
            this.props.selectLayer(null);
        }
        this.props.showAttributesTable(false);
        //this.props.showLayerSettings(false);

    }

    render() {

        if (this.props.selected_layer && this.props.show) {
            return this.renderTable(this.props.selected_layer);
        } else {
            return null;
        }

    }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        selected_layer: state.datasets.selected_layer,
        show: state.viewer.attributes_table,
        layer_settings: state.viewer.layer_settings,
        layer_info: state.viewer.layer_info,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectLayer: selectLayer,
        showLayerInfo: showLayerInfo,
        showLayerSettings: showLayerSettings,
        showAttributesTable: showAttributesTable,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(AttributesTable);
