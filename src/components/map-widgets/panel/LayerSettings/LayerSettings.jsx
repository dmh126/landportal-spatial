import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';

import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Text from 'ol/style/Text';

import Geostats from 'geostats';
import Chroma from 'chroma-js';
import * as _ from 'lodash';

import {selectLayer, showLayerSettings, showLayerInfo, updateDisplayedLayer} from '../../../../actions/index';
import {sortStringsFirst} from '../../../../helpers/index';
import {styleGraduatedColors, areBigger, createHeatmapLayer, createVectorLayer, rebuildSparqlLayer} from '../../../viewer/helpers';

import {RAMPS} from '../../../../constants/ramps';
import {COLORS} from '../../../../constants/colors';

import './style.scss';

class LayerSettings extends Component {

    constructor(props) {

        super();

        this.state = {
            index: props.layerIndex,
            opacity: 100,
            color: 'OrRD',
            time: null,
            visualization: 'graduated',
            radius: 8,
            blur: 15,
        };

    }

    componentWillUnmount() {
        this.props.selectLayer(null);
    }

    setOpacity = (e) => {
        const opacity = parseInt(e.target.value);
        this.setState({opacity});
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        layer.setOpacity(this.state.opacity/100);
        this.updateStyle({opacity});
        _map.render();
    }

    setRadius = (e) => {
        const radius = parseInt(e.target.value);
        this.setState({radius});
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        this.updateStyle({radius})
        layer.setRadius(this.state.radius);

    }

    setBlur = (e) => {
        const blur = parseInt(e.target.value);
        this.setState({blur});
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        this.updateStyle({blur})
        layer.setBlur(this.state.blur);
    }


    setColors = (payload, time, label) => {

        let palette = payload.palette || 'OrRD';
        let color = payload.color || '#00FF00';
        let shape = payload.shape || 'circle';

        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        const features = layer.getSource().getFeatures();

        this.setState({palette, color});

        let t = this.props.selected_layer.style.time;
        //const selected_time = (time === 'any') ? null : time || ((t === 'any') ? null : t);
        let selected_time;
        if (time === 'any') {
            selected_time = null;
        } else {
            if (time) {
                selected_time = time;
            } else {
                if (t === 'any') {
                    selected_time = null;
                } else if (t) {
                    selected_time = t;
                } else {
                    selected_time = layer.get('time')
                    this.setState({time: selected_time})
                }
            }
        }

        let new_label;
        if (label) {
            new_label = (selected_time) ? `indicator_${selected_time}` : 'indicator';
        } else {
            new_label = null;
        }

        let values = _.flatten(features.map( feature => {
            let v = [];
            if (selected_time) {
                v.push(feature.get('indicator_' + selected_time));
            } else {
                v.push(feature.get('indicator'));
            }

            return v.filter( el => el)
        }));



        let isString;
        if (typeof values[0] === 'string' ) {
            isString = true;
        } else {
            isString = false;
        };

        let jenks;
        let ramp;
        if (isString) {
            jenks = Array.from(new Set(values)).sort();
            ramp = Chroma.scale(palette).colors(jenks.length);

        } else {
            let n_breaks = (values.length < 6) ? values.length -1  : 5;
            const geostats = new Geostats(values);
            jenks = geostats.getClassJenks(n_breaks || 1)
            ramp = Chroma.scale(palette).colors(n_breaks || 1);

        };

        features.forEach( feature => {
            feature.setStyle(styleGraduatedColors(feature, jenks, {ramp, color, shape}, selected_time, new_label))
        });

        layer.set("legend", {jenks, ramp, color, shape}, true)

        this.updateStyle({palette, color});

    }

    setShape = (e, style) => {
        const shape = e.target.value;
        this.updateStyle({shape})
        let payload = {
            color: style.color,
            palette: style.palette,
            shape
        }
        this.setColors(payload, null, style.label)
    }

    updateStyle = (style) => {

        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];
        const id = layer.get("layer_id");
        const _obj = {
            _id: id,
            style: style
        };
        this.props.updateDisplayedLayer(_obj);

    }

    renderTimeSwitch(type) {

            if (["Vector", "SPARQL"].includes(type)) {

                const _map = this.props.viewer.getOlMap();
                const layer = _map.getLayers().array_[this.props.show];
                const features = layer.getSource().getFeatures();

                //let values = Array.from(new Set(features.map( feature => feature.get('time')).filter( el => el))).sort();

                let values = Array.from(new Set(_.flatten(features.map( feature => {
                    let t = feature.get('times');
                    if (t) {
                        return t.split(';')
                    } else {
                        let times = feature.getKeys()
                        .filter(k => k.includes('indicator'))
                        .map(k => k.split('_')[1]).filter(k => k).sort();
                        return times;
                    }
                })).filter( el => el))).sort().reverse();

                //console.log(values)
                //let values = layer.get('time') || [];

                return (
                    <div className="layer-settings-group">
                        <div className="layer-settings-name">Time</div>
                        <div className="layer-settings-time">
                            <select onChange={(e) => this.changeTime(e)} value={this.state.time || values[0]}>
                                {
                                    values.map( (item, index) => {
                                        return <option key={index} value={item}>{item}</option>
                                    })
                                }
                            </select>
                        </div>
                    </div>
                );

            } else {

                return null;

            }



    }

    changeVisualizationType = (e) => {

        const visualization = e.target.value;
        const selected = this.props.selected_layer;
        const _map = this.props.viewer.getOlMap();
        const layer = _map.getLayers().array_[this.props.show];

        if (layer.get('indicator_type') === 'string') { // TODO disabling switch completely
            return
        };

        //this.setState({time: null});
        //this.updateStyle({time: 'any'});
        let time = layer.get('time');
        let zIndex = layer.getZIndex();

        const style = {
            properties: selected.style,
            visualization: selected.style.visualization,
            time
        }

        let newLayer;

        switch (visualization) {

            case "heatmap":
                let color = RAMPS.find( c => c.value === this.state.color || 'OrRd');
                newLayer = createHeatmapLayer(layer, time, color.palette);
                break;

            case "graduated":
                if (selected.type === 'Vector') {
                    newLayer = createVectorLayer(selected, style);
                } else if (selected.type === 'SPARQL') {
                    newLayer = rebuildSparqlLayer(layer, style, false, zIndex);
                }
                break;

            case "proportional":
                if (selected.type === 'Vector') {
                    newLayer = createVectorLayer(selected, style, true, zIndex);
                } else if (selected.type === 'SPARQL') {
                    newLayer = rebuildSparqlLayer(layer, style, true, zIndex);
                }
                break;

            default:
                break;

        }

        this.setState({visualization});

        if (newLayer) {
            _map.getLayers().array_[this.props.show] = newLayer;
            newLayer.on("change", (e) => {
                _map.render();
            });
        }

        this.updateStyle({visualization})

        _map.render();

    }

    changeTime = (e) => {

        let value = e.target.value;
        const style = this.props.selected_layer.style

        if (style.visualization === 'heatmap') {
            const _map = this.props.viewer.getOlMap();
            const layer = _map.getLayers().array_[this.props.show];
            const newLayer = createHeatmapLayer(layer, "indicator_" + value);
            _map.getLayers().array_[this.props.show] = newLayer;
            newLayer.on("change", (e) => {
                _map.render();
            });
            this.setState({time: value});
            this.updateStyle({time: value});
        } else {

            this.setState({time: value});
            this.updateStyle({time: value});
            this.setColors(style, value, style.label);

        }

    }

    setLabels = (e, style) => {
        const value = e.target.checked;

        if (value) {
            this.updateStyle({label: "indicator"})
            this.setColors(style, style.time, "indicator")
        } else {
            this.updateStyle({label: null})
            this.setColors(style, style.time)
        };


    }

    renderLabelSwitch = (style, type) => {

        if ((["graduated", "proportional"].includes(style.visualization)) && (["Vector", "SPARQL"].includes(type))) {

            return (
                <div className="layer-settings-group">
                    <div className="layer-settings-name">Labels</div>
                    <div>
                        <input type="checkbox"  onChange={(e) => this.setLabels(e, style)} defaultChecked={(style.label) ? true : false} />
                    </div>
                </div>
            );

        } else {

            return null;

        }

    }

    renderVisualizationTypeSwitch = (style, type) => {

        let allowed = style.allowed || ['Graduated Colors', 'Heatmap', 'Proportional'];

        if ( ["Vector", "SPARQL"].includes(type) ) {

            return (
                <div className="layer-settings-group">
                    <div className="layer-settings-name">Visualization type</div>
                    <div className="layer-settings-type">
                        <select onChange={(e) => this.changeVisualizationType(e)} value={style.visualization}>
                            {
                                (allowed.includes('Graduated Colors')) && <option value={'graduated'}>Graduated colors</option>
                            }
                            {
                                (allowed.includes('Heatmap')) && <option value={'heatmap'}>Heatmap</option>
                            }
                            {
                                (allowed.includes('Proportional')) && <option value={'proportional'}>Proportional symbols</option>
                            }
                        </select>
                    </div>
                </div>
            )
        } else {

            return null;

        }

    }

    renderHeatmapSettings = (style, type) => {

        if ((style.visualization === "heatmap") && (["Vector", "SPARQL"].includes(type)) ) {

            return (
                <div className="layer-settings-group">
                    <div className="layer-settings-name">Heatmap</div>
                        <div className="layer-settings-slider">
                            <label><span className="slider-name">Radius</span><span className="slider-value">{style.radius}</span>
                                <input type="range" min="0" max="100" defaultValue={style.radius} className="slider" id="heatmap-radius" onChange={(e) => this.setRadius(e)} />
                            </label>
                        </div>
                        <div className="layer-settings-slider">
                            <label><span className="slider-name">Blur</span><span className="slider-value">{style.blur}</span>
                                <input type="range" min="0" max="100" defaultValue={style.blur} className="slider" id="heatmap-blur" onChange={(e) => this.setBlur(e)} />
                            </label>
                        </div>
                </div>
            )

        } else {

            return null;

        }

    }

    renderGraduatedSettings = (style, type) => {

        if ((style.visualization === "graduated") && (["Vector", "SPARQL"].includes(type)) ) {

            return (
                <div className="layer-settings-group">
                    <div className="layer-settings-name">Graduated colors</div>
                    <div className="layer-settings-colors">
                    {
                        RAMPS.map( (item, index) => {
                            let ramp = item.palette.join(',')
                            let btn = {background: `linear-gradient(0.25turn, ${ramp})`};
                            btn.border = (style.color === item.value) ? "2px solid #0066CC" : 'none';
                            btn.boxShadow = (style.color === item.value) ? "0 0 10px #0066CC" : 'none';
                            let payload = {
                                palette: item.value,
                                color: style.color,
                                shape: style.shape,
                            }
                            return (
                                <button
                                    className="layer-settings-palette"
                                    key={index}
                                    style={btn}
                                    onClick={(e) => this.setColors(payload, null, style.label)}
                                    title={item.value}
                                    ></button>
                            )
                        })
                    }
                    </div>
                </div>
            )

        } else {

            return null;

        }

    }

    renderProportionalSettings = (style, type) => {

        if ((style.visualization === "proportional") && (["Vector", "SPARQL"].includes(type)) ) {

            return (
                <div className="layer-settings-group">

                    <div className="layer-settings-name">Proportional symbols</div>

                    <div className="layer-settings-type">
                        <select onChange={(e) => this.setShape(e, style)} value={style.shape}>
                            <option value={'circle'}>Circle</option>
                            <option value={'triangle'}>Triangle</option>
                            <option value={'square'}>Square</option>
                        </select>
                    </div>

                    <div className="layer-settings-colors">
                    {
                        COLORS.map( (item, index) => {
                            let btn = {background: item.value};
                            btn.border = (style.color === item.value) ? "2px solid #0066CC" : 'none';
                            btn.boxShadow = (style.color === item.value) ? "0 0 10px #0066CC" : 'none';
                            let payload = {
                                color: item.value,
                                palette: style.palette,
                                shape: style.shape,
                            }
                            return (
                                <button
                                    className="layer-settings-palette"
                                    key={index}
                                    style={btn}
                                    onClick={(e) => this.setColors(payload, null, style.label)}
                                    title={item.value}
                                    ></button>
                            )
                        })
                    }
                    </div>

                </div>
            )

        } else {

            return null;

        }

    }


    renderTable(data) {

        if (data) {

            const layer = this.props.displayed_layers.find( l => l._id === data._id);
            const style = layer.style;
            const type = layer.type;

            return (
                    <div id="layer-settings-container">
                        <div className="layer-settings-header">
                            Edit data visualization
                            <span onClick={this.closeTable}>X</span>
                        </div>
                        <div className="layer-settings-body">
                            {/*<div className="layer-settings-label">{layer.name}</div>*/}
                            {this.renderVisualizationTypeSwitch(style, type)}

                            {this.renderHeatmapSettings(style, type)}

                            {this.renderGraduatedSettings(style, type)}

                            {this.renderProportionalSettings(style, type)}

                            <div className="layer-settings-group">
                                <div className="layer-settings-name">Display</div>
                                <div className="layer-settings-slider">
                                    <label><span className="slider-name">Opacity</span><span className="slider-value">{style.opacity}</span>
                                        <input type="range" min="0" max="100" defaultValue={style.opacity} className="slider" id="myRange" onChange={(e) => this.setOpacity(e)} />
                                    </label>
                                </div>
                            </div>

                            {
                                this.renderTimeSwitch(type)
                            }
                            {
                                //this.renderLabelSwitch(style, type)
                            }
                        </div>
                    </div>
            )

        }

    }

    closeTable = () => {
        if (!this.props.layer_info && !this.props.attributes_table ) {
            this.props.selectLayer(null);
        }
        this.props.showLayerSettings(false);
        //this.props.showLayerInfo(false);

    }

    render() {

        if (this.props.selected_layer && this.props.show) {
            return this.renderTable(this.props.selected_layer);
        } else {
            return null;
        }

    }

}

function mapStateToProps(state) {

    return {
        selected_layer: state.datasets.selected_layer,
        show: state.viewer.layer_settings,
        viewer: state.viewer.viewer,
        layer_info: state.viewer.layer_info,
        attributes_table: state.viewer.attributes_table,
        displayed_layers: state.datasets.displayed_layers,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        selectLayer: selectLayer,
        showLayerSettings: showLayerSettings,
        showLayerInfo: showLayerInfo,
        updateDisplayedLayer: updateDisplayedLayer,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(LayerSettings);
