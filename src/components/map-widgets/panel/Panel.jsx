import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import LayerTree from './LayerTree/LayerTree';
//import LayerInfo from './LayerInfo/LayerInfo';
import LayerSettings from './LayerSettings/LayerSettings';
import Bar from '../controls/Bar';
import {showPanel} from '../../../actions/index';
import {FaAngleLeft, FaAngleRight} from 'react-icons/fa';

import './style.scss';

class Panel extends Component {

    constructor(){
        super();

        this.state = {
            collapsed: false,
        };
    }

    togglePanel = () => {
        const {collapsed} = this.state;
        this.setState( {collapsed: !collapsed} );
    }

    render() {
        const {collapsed} = this.state;
        const translate = collapsed ? 'translate(-100%)' : 'translate(0)';

        return (
            <div id='map-widgets-panel' style={{transform: translate}}>
                <LayerTree/>
                {this.props.show && <LayerSettings/>}
                <div
                    className="toggle-panel"
                    onClick={this.togglePanel}
                    >
                    {collapsed ? <FaAngleRight /> : <FaAngleLeft />}
                </div>
                {collapsed && <div className="toggle-label">Expand legend</div>}
            </div>
        )
    }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        layer_info: state.viewer.layer_info,
        layer_settings: state.viewer.layer_settings,
        show: state.viewer.panel,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showPanel: showPanel
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Panel);
