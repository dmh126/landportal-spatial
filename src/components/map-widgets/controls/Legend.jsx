import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdPalette} from 'react-icons/md';
import ReactTooltip from 'react-tooltip'

import {showMapLegend} from '../../../actions/index';

import './style.scss';

class Legend extends Component {

    openLegend = () => {

        this.props.showMapLegend(!this.props.map_legend);

    }

    render() {
        return (
            <div
                className='bar-icon icon-big'
                onClick={() => this.openLegend()}
                data-tip="Map legend"
                >
                <MdPalette />
                
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        map_legend: state.viewer.map_display,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showMapLegend: showMapLegend,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Legend);
