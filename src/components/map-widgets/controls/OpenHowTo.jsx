import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaQuestionCircle} from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';

import {
    showHelp,
    showModalCatalog,
    compositionIdReceived,
} from '../../../actions/index.js';

import './style.scss';

class OpenHowTo extends Component {

    openHowTo = () => {

        this.props.showModalCatalog(false),
        this.props.compositionIdReceived(null),
        this.props.showHelp(!this.props.help);

    }

    render() {

        const cn = 'bar-icon icon-big ' + ((this.props.help) ? 'selected' : '');
        return (
            <div
                className={cn}
                onClick={() => this.openHowTo()}
                >
                Help
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {help: state.viewer.help}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showHelp,
        showModalCatalog,
        compositionIdReceived,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(OpenHowTo);
