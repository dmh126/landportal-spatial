import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaMap} from 'react-icons/fa';

import {showBaseMapPicker} from '../../../actions/index.js';

import './style.scss';

import ReactTooltip from 'react-tooltip'

class BaseMapPicker extends Component {

    openBasemaps = () => {

        this.props.showBaseMapPicker(!this.props.show);

    }

    render() {
        return (
            <div
                className='bar-icon icon-big'
                onClick={() => this.openBasemaps()}
                data-tip="Select Basemap"
                >
                <FaMap />

            </div>
        )
    }
}

function mapStateToProps(state) {

    return {viewer: state.viewer.viewer, show: state.viewer.basemap_picker}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showBaseMapPicker: showBaseMapPicker,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(BaseMapPicker);
