import React, {Component} from 'react';
import './style.scss';

class Contact extends Component {

    goHome() {
        window.location.assign('mailto:geoportal@landportal.info');
    }

    render() {
        return (
            <div
                className='bar-icon icon-big'
                onClick={this.goHome}
                >
                Contact us
            </div>
        )
    }
}

export default Contact;
