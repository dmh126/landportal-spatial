import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaChevronDown, FaChevronUp} from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';
import {
    showModalCatalog,
    compositionIdReceived,
    showHelp
} from '../../../actions/index.js';

import './style.scss';

class OpenCatalog extends Component {

    openCatalog = () => {

        this.props.showModalCatalog(!this.props.show);
        this.props.compositionIdReceived(null);
        this.props.showHelp(false);

    }

    render() {
        let cn = 'bar-icon icon-big add-data-btn'
        if (this.props.show) cn += ' selected';
        return (
            <div
                className={cn}
                onClick={() => this.openCatalog()}
                >
                Explore & Add data {this.props.show ? <FaChevronUp /> : <FaChevronDown />}

            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        show: state.viewer.modal_catalog,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showModalCatalog,
        compositionIdReceived,
        showHelp
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(OpenCatalog);
