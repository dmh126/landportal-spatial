import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTooltip from 'react-tooltip';
import {bindActionCreators} from 'redux';

import {showGlobe} from '../../../actions/index.js';

import './style.scss';

class SwitchKD extends Component {

  constructor() {

    super();

    this.state = {
        label: '3D'
    }

  }

  switchKD() {

    const viewer = this.props.viewer;

    if ( viewer ) {

      const is3D = viewer.getEnabled();

      viewer.setEnabled( !is3D );

      this.setState({
        label: (!is3D) ? '2D' : '3D'
      })

      this.props.showGlobe(!is3D)

    }

  }

  render() {
    return (
      <div
        className='bar-icon'
        onClick={ () => this.switchKD() }
        data-tip="Switch: 2D-3D"
        >
        { this.state.label }
      </div>
    )
  }
}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showGlobe
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(SwitchKD);
