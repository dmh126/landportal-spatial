import React, {Component} from 'react';
import './style.scss';

class Homepage extends Component {

    goHome() {
        window.open('http://landportal.org');
    }

    render() {
        return (
            <div
                className='bar-icon icon-big homepage'
                onClick={this.goHome}
                >
                Back to the Land Portal

            </div>
        )
    }
}

export default Homepage;
