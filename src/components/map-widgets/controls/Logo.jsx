import React, {Component} from 'react';
import Picture from '../../../assets/geoportal-logo_white_dark-backgrounds.png';
import './style.scss';

class Logo extends Component {


    render() {
        return (
            <div
                className='bar-icon icon-big'
                >
                <img src={Picture} />

            </div>
        )
    }
}

export default Logo;
