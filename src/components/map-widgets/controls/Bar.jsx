import React, {Component} from 'react';

import OpenCatalog from './OpenCatalog';
import Logo from './Logo';
import BaseMapPicker from '../base-map-picker/BaseMapPicker';
import OpenHowTo from './OpenHowTo';
import Share from './Share';
import Homepage from './Homepage';
import Search from './Search';
import Contact from './Contact';

import './style.scss';

export default class Bar extends Component {

  render() {
    return (
      <div className="bar">
          <Logo />
          <OpenCatalog />
          <Share />
          <Contact />
          <OpenHowTo />
          <BaseMapPicker />
          <Search />
          <Homepage />
      </div>
    )
  }
}
