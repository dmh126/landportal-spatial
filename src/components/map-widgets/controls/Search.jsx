import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaSistrix} from 'react-icons/fa';
import ReactTooltip from 'react-tooltip';
import {showModalCatalog, advancedSearch} from '../../../actions/index.js';

import './style.scss';

class Search extends Component {

    constructor(){
        super();
        this.state = {
            text: "",
        }
    }

    searchText = () => {
        const query = this.state.text;
        this.props.advancedSearch({query});
        this.props.showModalCatalog(true);
        //console.log(this.state.text)
    }

    enterPressed = (e) => {
        if (e.key === "Enter") {
            this.searchText();
        };
    }

    clearSearch = () => {
        this.setState({text: ''})
    }

    render() {
        return (
            <div
                className='advanced-search'
                >
                <input type="text"
                    value={this.state.text}
                    onKeyPress={this.enterPressed}
                    onChange={(e)=>this.setState({text: e.target.value})} />
                <button onClick={this.searchText}>🔍</button>
                {this.state.text && <span onClick={this.clearSearch}>x</span>}
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showModalCatalog,
        advancedSearch
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Search);
