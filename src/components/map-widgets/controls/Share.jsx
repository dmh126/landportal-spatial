import React, {Component} from 'react';
import {connect} from 'react-redux';
import ReactTooltip from 'react-tooltip';
import {bindActionCreators} from 'redux';
import {FaChevronUp, FaChevronDown} from 'react-icons/fa';
import {
    createComposition,
    compositionIdReceived,
    showModalCatalog,
    showHelp
} from '../../../actions/index';

import './style.scss';

class Share extends Component {

    share() {

        if (this.props.show) {
            this.props.compositionIdReceived(null);
            return;
        }

        this.props.showModalCatalog(false);
        this.props.showHelp(false);

        const layers = this.props.displayed_layers;
        const visibility = this.getVisibility();

        if (layers) {

            const indicators = layers.map((item, index) => {
                return {id: item._id, type: item.type, style: item.style, visible: visibility[index]};
            });

            const basemap = this.props.basemap;
            const extent = this.getMapExtent();
            this.props.createComposition({indicators, basemap, extent});
        }

    }

    getMapExtent = () => {

        const _map = this.props.viewer.getOlMap();
        const ext = _map.getView().calculateExtent(_map.getSize());

        return ext;
    }

    getVisibility = () => {
        const _map = this.props.viewer.getOlMap();
        const layers = _map.getLayers().array_;
        let visibility = [];
        for (let i = 1; i < layers.length; i++) {
            let lyr = layers[i];
            visibility.push(lyr.getVisible())
        }
        return visibility.reverse();
    }

    render() {
        let cn = 'bar-icon icon-big share '
        if (this.props.show) cn += 'selected';
        return (
            <div className={cn} onClick={() => this.share()}>
                Share your map
                {this.props.show ? <FaChevronUp /> : <FaChevronDown />}
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {viewer: state.viewer.viewer, displayed_layers: state.datasets.displayed_layers, basemap: state.viewer.basemap, show: state.compositions.composition}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        createComposition,
        compositionIdReceived,
        showModalCatalog,
        showHelp
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Share);
