import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {saveAs} from 'file-saver';
import {FaCamera} from 'react-icons/fa';
import ReactTooltip from 'react-tooltip'

import './style.scss';

class CaptureImage extends Component {

    constructor() {

        super();

    }

    captureImage = () => {

        const _map = this.props.viewer.getOlMap();

        if (_map) {

            _map.once('rendercomplete', e => {
                let canvas = e.context.canvas;

                if (navigator.msSaveBlob) {

                    navigator.msSaveBlob(canvas.msToBlob(), 'spatial_land_portal_export.png');

                } else {
                    canvas.toBlob( blob => {
                        saveAs(blob, 'spatial_land_portal_export.png');
                    });
                }

            });

            _map.renderSync();

        }

    }

    render() {
        return (
            <div
                className='bar-icon icon-big'
                onClick={() => this.captureImage()}
                data-tip="Export Map to PNG"
                >
                <FaCamera />

            </div>
        )
    }
}

function mapStateToProps(state) {

    return {viewer: state.viewer.viewer}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        testAction: null
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(CaptureImage);
