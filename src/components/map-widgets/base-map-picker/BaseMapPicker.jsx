import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';

import {BASEMAPS} from '../../../constants/spatial.js';
import {createBaseMap} from '../../viewer/helpers.js';

import {showBaseMapPicker, setBasemap} from '../../../actions/index';

import './style.scss';

class BaseMapPicker extends Component {

    changeBasemap = (name) => {

        const baseMap = createBaseMap(name);

        const map2d = this.props.viewer.getOlMap();

        map2d.getLayers().setAt(0, baseMap);

        this.props.setBasemap(name);
        this.props.showBaseMapPicker(false);

    }

    render() {



            return (
                <div className="base-map-picker">
                    {
                        Object.keys(BASEMAPS).map( (item, index) => {
                            const cn = (this.props.selected === item)? "base-map-entry selected" : "base-map-entry";
                            return (
                                <div
                                    key={index}
                                    className={cn}
                                    onClick={()=>this.changeBasemap(item)}
                                    style={{
                                        background: BASEMAPS[item].thumbnail,
                                        backgroundPosition: 'center',
                                    }}
                                    ></div>
                            )
                        })

                    }
                </div>
            )


    }

}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        show: state.viewer.basemap_picker,
        selected: state.viewer.basemap,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showBaseMapPicker,
        setBasemap
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(BaseMapPicker);
