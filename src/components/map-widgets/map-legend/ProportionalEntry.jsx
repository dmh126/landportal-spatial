import React, {Component} from 'react';

export default class ProportionalEntry extends Component {

    render(){
        const layer = this.props.layer;
        const legend = this.props.legend;
        const jenks = legend.jenks;
        const type = typeof jenks[0];
        const ramp = legend.ramp;
        const className = `${layer.style.shape}`
        let style;
        if (layer.style.shape === 'triangle') {
            style = {borderBottom: `25px solid ${layer.style.color}`};
        } else {
            style = {background: layer.style.color};
        }

        return(
         <div className="legend-panel-mapLegend">
             <div className="legend-panel-layer-name">{layer.name}</div>
            {
                /*ramp.map((item, index) => {
                    const style = {
                        //backgroundColor: layer.style.color,
                        width: `${5 + 5 * index}px`,
                        height: `${5 + 5 * index}px`
                    }
                    const className = `legend-panel-symbol ${layer.style.shape}`
                    return (
                    <div key={index}>
                        <div className={className} style={style}></div>
                        <div className="legend-panel-jenks-proportional">{(type==="string")? jenks[index]: `${jenks[index]} - ${jenks[index+1]}`}</div>
                    </div>
                    )
                })*/

                <div className="legend-panel-list">
                    <div className="legend-panel-symbol">
                        <div className={className} style={style}></div>
                    </div>
                    <div className="legend-panel-jenks-proportional">min: {jenks[0].toFixed(2)}, max: {jenks[jenks.length -1].toFixed(2)}</div>
                </div>
            }
        </div>
        )
    }
}
