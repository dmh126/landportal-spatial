import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';

import {showMapLegend} from '../../../actions/index';

import Resizer from '../../resizer/Resizer';

import GraduatedEntry from './GraduatedEntry';
import ProportionalEntry from './ProportionalEntry';

import './style.scss';

class MapLegend extends Component {

  constructor(){
    super();

    }


    closeWindow = () => {

        this.props.showMapLegend(false);

    }

    render() {

        if (this.props.show) {

            const layers = this.props.viewer.getOlMap().getLayers().array_;

            return (
                <Draggable cancel=".legend-panel-body, .resizer">
                    <div id="legend-panel-container">
                        <div className="legend-panel-header">Map Legend
                            <span onClick={this.closeWindow}>X</span>
                        </div>
                        <div className="legend-panel-body">
                        {
                           this.props.displayed_layers.map((item, index) => {

                               const legend = layers[item.style.zIndex].get('legend');


                               const _type = item.style.visualization;

                               if(_type === 'graduated'){
                                return(
                                  <GraduatedEntry
                                      layer={item}
                                      legend={legend}
                                      key={index}
                                  />
                                )
                                } else if(_type === 'proportional'){
                                  return(
                                    <ProportionalEntry
                                        layer={item}
                                        legend={legend}
                                        key={index}
                                    />
                                  )
                                }
                              })
                        }
                        </div>
                      <Resizer container={"legend-panel-container"} />
                    </div>
                </Draggable>
            )

        } else {

            return null;

        }

    }


}

function mapStateToProps(state) {

    return {
        viewer: state.viewer.viewer,
        show: state.viewer.map_legend,
        displayed_layers: state.datasets.displayed_layers,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showMapLegend: showMapLegend,
    }, dispatch)
}


export default connect(mapStateToProps, matchDispatchToProps)(MapLegend);
