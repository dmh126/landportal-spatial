import React, {Component} from 'react';
import {RAMPS} from '../../../constants/ramps';

export default class GraduatedEntry extends Component {

    render(){

        const layer = this.props.layer;
        const legend = this.props.legend;
        const jenks = legend.jenks;
        const type = typeof jenks[0];
        const ramp = legend.ramp;

        return(
         <div className="legend-panel-mapLegend">
            <div className="legend-panel-layer-name">{layer.name}</div>
            {
                ramp.map((item, index) => {

                    return (
                    <div key={index} className="legend-panel-entry">
                        <div className="legend-panel-ramp" style={{backgroundColor : item}}></div>
                        <div className="legend-panel-jenks">{(type==="string")? jenks[index]: `${jenks[index]} - ${jenks[index+1]}`}</div>
                    </div>
                    )
                })
            }

        </div>
        )
    }
}
