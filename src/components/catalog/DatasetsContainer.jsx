import React, {Component} from 'react';

import Datasets from './Datasets';
import Search from './Search';
import NavBar from '../navbar/NavBar';

import './style.scss';

export default class DatasetsContainer extends Component {

  componentDidMount(){

  }


  render() {
    return (
      <div>
        <NavBar />
        <Search />
        <Datasets />
      </div>
    )
  }
}
