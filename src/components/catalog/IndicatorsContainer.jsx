import React, {Component} from 'react';

import Indicators from './Indicators';
import DatasetInfo from './DatasetInfo';
import NavBar from '../navbar/NavBar';

import './style.scss';

export default class IndicatorsContainer extends Component {

  constructor(props){

      super();

      this.dataset_id = props.match.params.dataset_id;
  }


  render() {
    return (
      <div>
        <NavBar />
        <DatasetInfo dataset_id={this.dataset_id} />
        <Indicators dataset_id={this.dataset_id} />
      </div>
    )
  }
}
