import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getDatasetInfo} from '../../actions/index';
import SearchForm from '../forms/SearchForm';

class DatasetInfo extends Component {

    componentDidMount() {

        this.props.getDatasetInfo(this.props.dataset_id)

    }

    render() {

        const info = this.props.dataset_info;

        if (!info) {
            return null;
        }

        let content;
        let meta;

        let n_indicators = (this.props.indicators) ? this.props.indicators.length : 0;

        if (info.ok) {

            const metadata = info.dataset.meta.dataset;

            content = (
                <div className="catalog-dataset-info-block">
                    <h3>Dataset Info</h3>
                    {<div className="catalog-dataset-info-field">
                        <img src={metadata.logo} />
                    </div>}
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Name</div>
                        <div className="info-field-value">{info.dataset.name}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">About</div>
                        <div className="info-field-value">{info.dataset.about}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Number of indicators</div>
                        <div className="info-field-value">{n_indicators}</div>
                    </div>

                </div>
            )

            meta = (
                <div className="catalog-dataset-info-block">
                    <h3>Metadata</h3>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Label</div>
                        <div className="info-field-value">{metadata.label}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Description</div>
                        <div className="info-field-value">{metadata.description}</div>
                    </div>
                    {/*<div className="catalog-dataset-info-field">
                        <div className="info-field-label">Logo</div>
                        <div className="info-field-value">{metadata.logo}</div>
                    </div>*/}
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">License</div>
                        <div className="info-field-value">{metadata.license}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Copyright details</div>
                        <div className="info-field-value">{metadata.copyright_details}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Organization</div>
                        <div className="info-field-value">{metadata.organization}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Related overarching categories</div>
                        <div className="info-field-value">{metadata.related_overarching_categories}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Related themes</div>
                        <div className="info-field-value">{metadata.related_themes}</div>
                    </div>
                    <div className="catalog-dataset-info-field">
                        <div className="info-field-label">Related LandVoc concepts</div>
                        <div className="info-field-value">{metadata.related_landvoc_concepts}</div>
                    </div>
                </div>
            )

        }

        return (
            <div id="catalog-dataset-info">
                {content}
                {meta}
            </div>
        )
    }
}

function mapStateToProps(state) {

    return {
        dataset_info: state.datasets.dataset_info,
        indicators: state.datasets.indicators,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasetInfo: getDatasetInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(DatasetInfo);
