import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createLink} from '../../helpers/index';

import {getIndicators} from '../../actions/index';
import {formatDate} from '../../helpers/index';

class Indicators extends Component {

    componentDidMount() {

        this.props.getIndicators(this.props.dataset_id);

    }

    renderTableHeaders() {

        return (
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>About</th>
                    <th>Type</th>
                    <th>Last update</th>
                    <th>Url</th>
                </tr>
            </thead>
        )

    }

    renderTableRows(data) {

        return (
            <tbody>
                {
                    data.map((item, index) => {
                        let src;
                        if (item.type === 'SPARQL') {
                            src = item.sparql.source_url;
                        }
                        return (

                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.about}</td>
                                <td>{item.type}</td>
                                <td>{formatDate(item.updated_at)}</td>
                                <td>
                                    <Link to={createLink(item.type, item._id, src)} target="_blank">View</Link>
                                </td>
                            </tr>

                        )

                    })
                }
            </tbody>
        )

    }

    render() {

        return (
            <div id="catalog-table">
                <table>
                    <caption><h3>Indicators</h3></caption>
                    {/* Render table headers */
                        this.renderTableHeaders()
                    }
                    {/* Render table rows */
                        this.renderTableRows(this.props.indicators)
                    }

                </table>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {indicators: state.datasets.indicators}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getIndicators: getIndicators
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Indicators);
