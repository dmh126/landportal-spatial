import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createLink, formatDate} from '../../helpers/index';

import {getDatasets} from '../../actions/index';

class Datasets extends Component {

    componentDidMount() {

        this.props.getDatasets();

    }

    renderTableHeaders() {

        return (
            <thead>
                <tr>
                    <th>#</th>
                    <th style={{minWidth:'250px'}}>Name</th>
                    <th>About</th>
                    <th style={{minWidth:'150px'}}>Last update</th>
                </tr>
            </thead>
        )

    }

    renderTableRows(data) {

        return (
            <tbody>
                {
                    data.map((item, index) => {

                        return (

                            <tr key={index}>
                                <td>{index + 1}</td>
                                <td>
                                    <Link to={`/catalog/${item._id}/indicators`}>{item.name}</Link>
                                </td>
                                <td>{item.about}</td>
                                <td>{formatDate(item.updated_at)}</td>
                            </tr>

                        )

                    })
                }
            </tbody>
        )

    }

    render() {

        return (
            <div id="catalog-table">
                <table>
                    <caption><h3>Datasets</h3></caption>
                    {/* Render table headers */
                        this.renderTableHeaders()
                    }
                    {/* Render table rows */
                        this.renderTableRows(this.props.datasets)
                    }

                </table>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {datasets: state.datasets.datasets}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasets: getDatasets
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Datasets);
