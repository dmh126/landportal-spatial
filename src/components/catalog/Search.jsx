import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {findDatasets} from '../../actions/index';
import SearchForm from '../forms/SearchForm';

class Search extends Component {

  constructor() {
      super();

      this.state = {
          advanced: false,
      }
  }

  toggleAdvanced = () => {
      this.setState({
          advanced: !this.state.advanced,
      })
  }

  handleSubmit(values) {
      /*
      let type = [];
      if (values['type-Vector']) type.push('Vector');
      if (values['type-Raster']) type.push('Raster');
      if (values['type-WMS']) type.push('WMS');
      values.type = type;*/

      this.props.findDatasets(values);

  }



  render() {

    return (
      <div id="catalog-search">
        <SearchForm onSubmit={values => this.handleSubmit(values)} advanced={this.state.advanced} onExpand={this.toggleAdvanced} />
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        findDatasets: findDatasets,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Search);
