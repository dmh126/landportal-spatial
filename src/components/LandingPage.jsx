import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import NavBar from './navbar/NavBar';
import LoginForm from './forms/LoginForm'
import ExampleIndicators from './home/ExampleIndicators';

import Logo from '../assets/lp-logo.png';

class LandingPage extends Component {



  render() {
    return (
      <div>
        <NavBar />
          <div className="landing-page">
              <div className="landing-block">
                  <img src={Logo} />
                  <div className="landing-about">
                      Landportal Spatial Component is an application to explore and visualize data...
                  </div>
                  <div className="landing-buttons">
                      <Link to="/catalog">Browse Catalog</Link>
                      <a href="https://landportal.org">Landportal Homepage</a>
                  </div>
              </div>
              <div className="landing-block">
                  <h2>Example Indicators</h2>
                  <ExampleIndicators />
              </div>
          </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        testing: state.testing,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        testAction: null,
    }, dispatch)
}

export default LandingPage;
