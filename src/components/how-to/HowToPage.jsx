import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';

import {
    showHelp
} from '../../actions/index';

import Img_1 from '../../assets/howto/1.png';
import Img_2 from '../../assets/howto/2.png';
import Img_3 from '../../assets/howto/3.png';
import Img_4 from '../../assets/howto/4.png';
import Img_5 from '../../assets/howto/5.png';
import Img_6 from '../../assets/howto/6.png';
import Img_7 from '../../assets/howto/7.png';
import Img_8 from '../../assets/howto/8.png';

import './style.scss';

class HowToPage extends Component {

  renderContainer() {
      return (
          <div className="howto-container">
              <h2>Geoportal Help</h2>
              <p>Welcome to the beta version of the Land Portal's Geoportal!</p>
              <p>
                  If this is your first time to the Land Portal's Geoportal,
                  or you are looking for more guidance on how to use it, you've came to the right place.
                  If you have any further questions or feedback,
                  please do not hesitate to get in touch with us through geoportal@landportal.info!
              </p>
              <h5>WHAT IS THE GEOPORTAL?</h5>
              <p>
                  The Geoportal is a component of the Land Portal website,
                  and aims to bring together and visualize statistical and geospatial data related to land issues.
                  The portal is tailored to <u>non-GIS experts</u> to allow them to visualize data and understand land issues better.
              </p>
              <p>
                  <i><b>The Geoportal is a component of the Land Portal website,
                      and aims to bring together and visualize statistical and geospatial data related to land issues.
                      The portal is tailored to non-GIS experts to allow them to visualize data and understand land issues better. </b></i>
              </p>
              <h5>HOW CAN I ADD DATA TO THE MAP?</h5>
              <p>
                  To add data to the map, you can use the navigation bar on the top of your screen and select 'Explore & Add Data'.
                  The menu allows you to browse the available data by Issue, Region or Datasets.
                  You can also use the search bar to type in the keyword that you're looking for.
              </p>
              <img src={Img_1} />
              <p>
                  You can select the dataset of your interest,
                  and explore the various layers this dataset contains.
              </p>
              <p>
                  Select 'Add to Map' to visualize it on the map.
              </p>
              <p>
                  Please note, you can only add up to three layers on the map.
              </p>
              <h5>HOW CAN I CHANGE DATA VISUALIZATIONS?</h5>
              <p>
                  Once you have added data to the map, you can change the visualization in the 'Layers on Map' box.
              </p>
              <img src={Img_2} />
              <p>
                  Depending on the type of data you have displayed on the map, you can change the way it's visualized.
              </p>
              <img src={Img_3} />
              <p>
                  You can change the type of visualization, you can change the colors and you can change the transparency of the layer as well. Please feel free to play around to a setting that you like best.
              </p>
              <h5>HOW CAN I CHANGE THE LAYER ORDER?</h5>
              <p>
                  If you add a new layer, it will automatically become the top layer.
              </p>
              <img src={Img_4} />
              <p>
                  You can shift the layer order by using the arrows on the left side of the layer panel.
              </p>
              <h5>HOW CAN I VIEW THE WHOLE MAP?</h5>
              <p>
                  If the Layer panel is blocking your full view of the map,
                  you can collapse the box to the left side by using the arrow as indicated in the image.
                  You can always bring it back by uncollapsing the arrow.
              </p>
              <img src={Img_5} /> <img src={Img_6} />
              <h5>HOW CAN I CHANGE THE WORLD MAP BACKGROUND?</h5>
              <p>
                  If you want to use a different background for your world map,
                   such as satellite images, you can select a different basemap in the top navigation.
              </p>
              <img src={Img_7} />
              <h5>
                  WHAT CAN I DO WITH MY MAP ONCE I'VE CREATED IT?
              </h5>
              <p>
                  Once you have designed your map the way you like it,
                  you can share it with the various options provided in the 'Share your map' menu item in the top navigation.
              </p>
              <img src={Img_8} />
              <p>
                  You can download a picture from your map to save on your computer to use in a presentation, for example.
              </p>
              <p>
                  You can also copy the link to share with a friend or colleague,
                  who can use the link to access the Geoportal with your selected layers and visualizations.
              </p>
              <p>
                  To include the map in your website, you can copy the embed code.
              </p>
              <h5>WHY CAN'T I USE THE GEOPORTAL ON MY MOBILE PHONE?</h5>
              <p>
                  We are committed to giving our users the best experience of the Geoportal possible.
                  Currently, we can only guarantee this on a desktop screen.
                  We are exploring whether we can optimize the use for mobile phones in the future as well!
              </p>
          </div>
      )
  }

  closeHowto = (e) => {
      if (e.target.className !== 'howto-backdrop') {
          e.stopPropagation();
          return 0;
      }
      this.props.showHelp(false);
  }

  render() {

    if(this.props.show) {
        return (
          <div className="howto-backdrop" onClick={this.closeHowto}>
              {this.renderContainer()}
          </div>
        )
    } else {
        return null;
    }

  }
}

function mapStateToProps(state) {

    return {
        show: state.viewer.help,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        showHelp,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(HowToPage);
