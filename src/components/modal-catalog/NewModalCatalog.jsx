import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getDatasets,
    getIndicators,
    showModalCatalog,
    addLayer,
    getIndicatorsAdvanced,
    advancedSearch,
    browseAdvanced,
    showIndicatorInfo
} from '../../actions/index';

import MCBrowse from './MCBrowse';
import MCDatasets from './MCDatasets';
import MCThemes from './MCThemes';
import MCCountries from './MCCountries';
import MCAdvanced from './MCAdvanced';
import Spinner from '../loaders/Spinner';

import './newstyle.scss';

class ModalCatalog extends Component {

    constructor(props) {

        super();

        this.state = {
            browse: true,
            query: '',
        };
    }

    handleQueryInput = (e) => {
        this.setState({query: e.target.value});
    }

    search = () => {
        const q = {
            query: this.state.query
        };
        this.props.advancedSearch(q);
        this.props.browseAdvanced();
    }

    onEnter = (e) => {
        if (e.key === 'Enter') {
            this.search();
        }
    }

    closeWindow = () => {
        this.props.showModalCatalog(false);
    }

    renderBrowse = () => {

        const tab = this.props.active_tab;

        switch(tab) {

            case ('datasets'):
                return (<MCDatasets />);
                break;

            case ('themes'):
                return (<MCThemes />);
                break;

            case ('countries'):
                return (<MCCountries />);
                break;

            case ('advanced'):
                return (<MCAdvanced />);
                break;

            default:
                return (<MCBrowse />);
        }

    }

    renderLoading = () => {

        if (this.props.loading) {

            return (
                <div className="mc-loading-backdrop">
                    <Spinner />
                </div>
            )

        } else {

            return null;

        }

    }

    renderIndicatorInfo = () => {

        const indicator = this.props.indicator_info
        if (indicator) {
            const meta = indicator.meta.indicator || indicator.meta.dataset;
            const dataset = indicator.dataset;
            return (
                <div
                    className="mc-indicator-info"
                    onClick={() => this.closeInfo()}>
                    <div className="mc-indicator-info-takeover">

                        {
                            /*Object.entries(meta).map( ([key, value], index) => {
                                if (value) {

                                    return (
                                        <div key={index} className="mc-indicator-info-node">
                                            <div className="mc-indicator-info-key">
                                                {key}
                                            </div>
                                            <div className="mc-indicator-info-value">
                                                {value}
                                            </div>
                                        </div>
                                    )

                                } else {

                                    return null;

                                }
                            })*/
                        }
                        {
                            meta.label && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Label
                                </div>
                                <div className="mc-indicator-info-value">
                                    {meta.label}
                                </div>
                            </div>
                        }
                        {
                            meta.description && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Description
                                </div>
                                <div className="mc-indicator-info-value">
                                    {meta.description}
                                </div>
                            </div>
                        }
                        {
                            dataset && dataset.name && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Dataset
                                </div>
                                <div className="mc-indicator-info-value">
                                    {dataset.name}
                                </div>
                            </div>
                        }
                        {
                            dataset && dataset.meta && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Organization
                                </div>
                                <div className="mc-indicator-info-value">
                                    {dataset.meta.dataset.organization}
                                </div>
                            </div>
                        }
                        {
                            meta.organization && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Organization
                                </div>
                                <div className="mc-indicator-info-value">
                                    {meta.organization}
                                </div>
                            </div>
                        }
                        {
                            meta.measurement_unit && <div className="mc-indicator-info-node">
                                <div className="mc-indicator-info-key">
                                    Measurement Unit
                                </div>
                                <div className="mc-indicator-info-value">
                                    {meta.measurement_unit}
                                </div>
                            </div>
                        }
                        {this.renderTags(meta)}

                    </div>
                </div>
            )
        } else {
            return null;
        }

    }


    renderTags(indicator) {
        const themes = indicator.related_themes.split(';').filter(i => i);
        const concepts = indicator.related_landvoc_concepts.split(';').filter(i => i);
        if (themes.length || concepts.length) {
            return (
                <div className="mc-indicator-info-node">
                    <div className="mc-indicator-info-key">
                        Landvoc
                    </div>
                    <div className="mc-indicator-info-value">
                        <div className="layer-info-card-landvoc">
                            <div>
                            {
                                themes.map((item, index) => {
                                    return <div className="tag-theme" key={index}>{item}</div>
                                })
                            }
                            </div>
                            <div>
                            {
                                concepts.map((item, index) => {
                                    return <div className="tag-concept" key={index}>{item}</div>
                                })
                            }
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }


    closeInfo = () => {
        this.props.showIndicatorInfo(null);
    }

    render() {

        if (this.props.show) {
            return (
                <div id="modal-catalog">
                    {this.renderLoading()}
                    {/*<div className="mc-search">
                        <div className="mc-search-input">
                            <input type="text" id="search" name="search"
                                onChange={(e) => this.handleQueryInput(e)}
                                onKeyPress={(e) => this.onEnter(e)} />
                        </div>
                        <div className="mc-search-button">
                            <button onClick={this.search}>Search</button>
                        </div>
                    </div>*/}
                    <div className="mc-browse">
                        {this.renderIndicatorInfo()}
                        {this.renderBrowse()}
                    </div>
                </div>
            )
        } else {
            return null;
        }
    }

};

function mapStateToProps(state) {

    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        show: state.viewer.modal_catalog,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        loading: state.datasets.loading,

        active_tab: state.catalog.active_tab,
        advanced: state.datasets.advanced,
        indicator_info: state.catalog.indicator_info,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasets: getDatasets,
        getIndicators: getIndicators,
        showModalCatalog: showModalCatalog,
        addLayer: addLayer,
        getIndicatorsAdvanced: getIndicatorsAdvanced,
        advancedSearch: advancedSearch,
        browseAdvanced: browseAdvanced,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ModalCatalog);
