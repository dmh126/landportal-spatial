import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdInfoOutline} from 'react-icons/md';

import {browseByMain, getDatasets, getIndicators, addLayer, browseAdvanced, showIndicatorInfo} from '../../actions/index';

class MCDatasets extends Component {

    constructor(props) {

        super();

        this.state = {
            selected_dataset: null,
        };
    }

    componentDidMount() {
        this.props.getDatasets();
    }


    selectDataset = (item) => {

        if (item) {
            const {_id, reference} = item;
            this.props.getIndicators(_id, reference);
        } else {
            if (this.props.advanced) {
                this.props.browseAdvanced()
            }
        }
        this.setState({
            selected_dataset: item,
        })
    }

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    indicatorInfo = (indicator) => {
        this.props.showIndicatorInfo(indicator);
    }

    renderIndicators = () => {
        const ds = this.state.selected_dataset || this.props.selected_item;
        return (
            <div className="mc-browse-datasets">
                <div className="mc-browse-datasets-info">
                    <div className="mc-browse-dataset-name">{ds.name}
                        <span onClick={()=>this.selectDataset(null)}>back</span>
                    </div>
                    <div className="mc-browse-dataset-description">{ds.about}</div>
                </div>
                <div className="mc-browse-indicators-list">
                    {
                        this.props.indicators.map( (item, index) => {
                            const existing = this.props.displayed_layers.find(l => l._id === item._id) ? 'existing' : 'add';
                            return (
                                <div
                                    className="mc-browse-indicator-entry"
                                    key={index}
                                    >{item.name}
                                    <div>
                                        <span className={existing} onClick={() => this.addToMap(item.type, item._id)}>+ Add to map</span>
                                        <span className="icon"
                                            onClick={() => this.indicatorInfo(item)}
                                            ><MdInfoOutline /></span>
                                    </div>
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )

    }

    renderDatasets = () => {

        return (
            <div className="mc-browse-datasets">
                <div className="mc-browse-datasets-header">Explore datasets
                    <span onClick={() => this.props.browseByMain()}>back</span>
                </div>
                <div className="mc-browse-datasets-list">
                    {
                        this.props.datasets.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-datasets-entry"
                                    key={index}
                                    >
                                    <div className="ds-label" onClick={()=>this.selectDataset(item)}>{item.name}</div>
                                    <span className="icon"
                                        onClick={() => this.indicatorInfo(item)}
                                        ><MdInfoOutline /></span>
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )

    }



    render() {

        const ds = this.state.selected_dataset || this.props.selected_item;

        return (ds) ? this.renderIndicators() : this.renderDatasets();

    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,

        selected_item: state.catalog.selected_item,
        advanced: state.datasets.advanced,

    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByMain,
        getDatasets,
        getIndicators,
        addLayer,
        browseAdvanced,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MCDatasets);
