import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdInfoOutline} from 'react-icons/md';

import {browseByMain, browseAdvanced, getDatasets, getIndicators, addLayer, getIndicatorsByTheme, showIndicatorInfo} from '../../actions/index';

import {THEMES} from '../../constants/LandVoc'

class MCThemes extends Component {

    constructor(props) {

        super();

        this.state = {
            selected_theme: null,
        };
    }

    componentDidMount() {
    }

    selectTheme = (item) => {

        if (item) {
            this.props.getIndicatorsByTheme({theme: item});
        } else {
            if (this.props.advanced) {
                this.props.browseAdvanced()
            }
        }

        this.setState({
            selected_theme: item,
        })

    }

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    renderThemes = () => {

        return (
            <div className="mc-browse-themes">
                <div className="mc-browse-themes-header">Explore themes
                    <span onClick={() => this.props.browseByMain()}>back</span>
                </div>
                <div className="mc-browse-themes-list">
                    {
                        this.props.themes.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-themes-entry"
                                    key={index}
                                    onClick={()=>this.selectTheme(item)}
                                    >{item}</div>
                            )

                        })
                    }
                </div>
            </div>
        )
    }

    indicatorInfo = (indicator) => {
        this.props.showIndicatorInfo(indicator);
    }

    renderIndicators = () => {
        const theme = this.state.selected_theme || this.props.selected_item;
        return (
            <div className="mc-browse-datasets">
                <div className="mc-browse-datasets-info">
                    <div className="mc-browse-dataset-name">{theme}
                        <span onClick={()=>this.selectTheme(null)}>back</span>
                    </div>
                </div>
                <div className="mc-browse-indicators-list">
                    {
                        this.props.indicators.map( (item, index) => {
                            const existing = this.props.displayed_layers.find(l => l._id === item._id) ? 'existing' : 'add';
                            return (
                                <div
                                    className="mc-browse-indicator-entry"
                                    key={index}
                                    >{item.name}
                                    <div>
                                        <span className={existing} onClick={() => this.addToMap(item.type, item._id)}>+ Add to map</span>
                                        <span className="icon"
                                            onClick={() => this.indicatorInfo(item)}
                                            ><MdInfoOutline /></span>
                                    </div>
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )

    }



    render() {
        const theme = this.state.selected_theme || this.props.selected_item;

        return theme ? this.renderIndicators() : this.renderThemes()

    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        themes: state.viewer.themes,
        selected_item: state.catalog.selected_item,
        advanced: state.datasets.advanced,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByMain,
        browseAdvanced,
        getDatasets,
        getIndicators,
        getIndicatorsByTheme,
        addLayer,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MCThemes);
