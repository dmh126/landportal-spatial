import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdInfoOutline} from 'react-icons/md';

import Region from './Region';

import {browseByMain, browseAdvanced, getDatasets, getIndicators, addLayer, getIndicatorsByCountry, showIndicatorInfo} from '../../actions/index';

import {REGIONS} from '../../constants/spatial'

class MCCountries extends Component {

    constructor(props) {

        super();

        this.state = {
            selected_country: null,
        };
    }

    componentDidMount() {
    }

    selectCountry = (item) => {

        if (item) {
            this.props.getIndicatorsByCountry({country: item.code});
        } else {
            if (this.props.advanced) {
                this.props.browseAdvanced()
            }
        }

        this.setState({
            selected_country: item,
        })

    }

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    renderCountries = () => {

        return (
            <div className="mc-browse-countries">
                <div className="mc-browse-countries-header">Explore countries
                    <span onClick={() => this.props.browseByMain()}>back</span>
                </div>
                <div className="mc-browse-countries-list">
                    {
                        REGIONS.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-countries-entry"
                                    key={index}
                                    >
                                    <Region region={item} selectCountry={this.selectCountry} />
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )
    }

    indicatorInfo = (indicator) => {
        this.props.showIndicatorInfo(indicator);
    }

    renderIndicators = () => {
        const country = this.state.selected_country || this.props.selected_item;
        return (
            <div className="mc-browse-datasets">
                <div className="mc-browse-datasets-info">
                    <div className="mc-browse-dataset-name">{country.country}
                        <span onClick={()=>this.selectCountry(null)}>back</span>
                    </div>
                </div>
                <div className="mc-browse-indicators-list">
                    {
                        this.props.indicators.map( (item, index) => {
                            const existing = this.props.displayed_layers.find(l => l._id === item._id) ? 'existing' : 'add';
                            return (
                                <div
                                    className="mc-browse-indicator-entry"
                                    key={index}
                                    >{item.name}
                                    <div>
                                        <span className={existing} onClick={() => this.addToMap(item.type, item._id)}>+ Add to map</span>
                                        <span className="icon"
                                            onClick={() => this.indicatorInfo(item)}
                                            ><MdInfoOutline /></span>
                                    </div>
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )

    }



    render() {
        const country = this.state.selected_country || this.props.selected_item;

        return country ? this.renderIndicators() : this.renderCountries()

    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,

        selected_item: state.catalog.selected_item,
        advanced: state.datasets.advanced,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByMain,
        browseAdvanced,
        getDatasets,
        getIndicators,
        getIndicatorsByCountry,
        addLayer,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MCCountries);
