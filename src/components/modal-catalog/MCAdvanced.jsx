import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaAngleUp, FaAngleDown} from 'react-icons/fa';
import {MdInfoOutline} from 'react-icons/md';

import {
    browseByMain,
    browseByDatasets,
    browseByThemes,
    browseByCountries,
    getDatasets,
    getIndicators,
    getIndicatorsByTheme,
    getIndicatorsByCountry,
    showIndicatorInfo,
    addLayer
} from '../../actions/index';

class MCAdvanced extends Component {

    constructor(props) {

        super();

        this.state = {
            show_indicators: false,
            show_datasets: true,
            show_themes: false,
            show_countries: false,
        }

    }

    componentDidMount() {

    }

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    selectDataset = (item) => {
        if (item) {
            const {_id, reference} = item;
            this.props.getIndicators(_id, reference);
            this.props.browseByDatasets(item);
        }
    }

    selectTheme = (item) => {
        if (item) {
            this.props.getIndicatorsByTheme({theme: item});
            this.props.browseByThemes(item);
        }
    }

    selectCountry = (item) => {
        if (item) {
            this.props.getIndicatorsByCountry({country: item.code});
            this.props.browseByCountries(item);
        }
    }

    toggleSection = (name) => {

        if (name === 'indicators') {
            this.setState({show_indicators: !this.state.show_indicators})
        } else if (name === 'datasets') {
            this.setState({show_datasets: !this.state.show_datasets})
        } else if (name === 'themes') {
            this.setState({show_themes: !this.state.show_themes})
        } else if (name === 'countries') {
            this.setState({show_countries: !this.state.show_countries})
        }
    }

    renderToggle = (condition, name) => {

        return (
            <span onClick={() => this.toggleSection(name)}>
                {condition ? <FaAngleUp /> : <FaAngleDown />}
            </span>
        )

    }

    indicatorInfo = (indicator) => {
        this.props.showIndicatorInfo(indicator);
    }

    renderIndicators(indicators) {
        const n_i = indicators.length;
        const show = this.state.show_indicators;
        return (
            <div className="mc-browse-indicators-node">
                <div className="mc-browse-indicators-control">Indicators ({n_i}) {this.renderToggle(show, 'indicators')}</div>
                {show && <div className="mc-browse-indicators-list">
                    {
                        indicators.map( (item, index) => {
                            const existing = this.props.displayed_layers.find(l => l._id === item._id) ? 'existing' : 'add';
                            return (
                                <div
                                    className="mc-browse-indicator-entry"
                                    key={index}
                                    >{item.name}
                                    <div>
                                        <span className={existing} onClick={() => this.addToMap(item.type, item._id)}>+ Add to map</span>
                                        <span className="icon"
                                            onClick={() => this.indicatorInfo(item)}
                                            ><MdInfoOutline /></span>
                                    </div>
                                </div>
                            )

                        })
                    }
                </div>}
            </div>
        )
    }

    renderDatasets = (datasets) => {
        const n_d = datasets.length;
        const show = this.state.show_datasets;
        return (
            <div className="mc-browse-datasets-node">
                <div className="mc-browse-datasets-control">Datasets ({n_d}) {this.renderToggle(show, 'datasets')}</div>
                {show && <div className="mc-browse-datasets-list">
                    {
                        datasets.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-datasets-entry"
                                    key={index}
                                    >
                                    <div className="ds-label">
                                        {item.name}
                                        <span onClick={()=>this.selectDataset(item)}>> Show layers</span>
                                    </div>
                                    <span className="icon"
                                        onClick={() => this.indicatorInfo(item)}
                                        ><MdInfoOutline /></span>
                                </div>
                            )

                        })
                    }
                </div>}
            </div>
        )

    }

    renderThemes = (themes) => {
        const n_t = themes.length;
        const show = this.state.show_themes;
        return (
            <div className="mc-browse-themes-node">
                <div className="mc-browse-themes-control">Themes ({n_t}) {this.renderToggle(show, 'themes')}</div>
                {show && <div className="mc-browse-themes-list">
                    {
                        themes.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-themes-entry"
                                    key={index}
                                    >{item}
                                        <span onClick={()=>this.selectTheme(item)}>> Show layers</span>
                                </div>
                            )

                        })
                    }
                </div>}
            </div>
        )
    }

    renderCountries = (countries) => {
        const n_c = countries.length;
        const show = this.state.show_countries;
        return (
            <div className="mc-browse-countries-node">
                <div className="mc-browse-countries-control">Countries & Regions ({n_c}) {this.renderToggle(show, 'countries')}</div>
                {show && <div className="mc-browse-countries-list">
                    {
                        countries.map( (item, index) => {

                            return (
                                <div
                                    className="mc-browse-countries-entry"
                                    key={index}
                                    >{item.country || item.region}
                                        <span onClick={()=>this.selectCountry(item)}>> Show layers</span>
                                </div>
                            )

                        })
                    }
                </div>}
            </div>
        )
    }

    render() {

        const advanced = this.props.advanced;

        return (
            <div className="mc-browse-advanced">
                <div className="mc-browse-advanced-header">Search results
                    <span onClick={() => this.props.browseByMain()}>back</span>
                </div>
                <div className="mc-browse-advanced-results">
                    {
                        advanced ? this.renderDatasets(advanced.datasets) : null
                    }
                    {
                        advanced ? this.renderThemes(advanced.themes) : null
                    }
                    {
                        advanced ? this.renderCountries(advanced.countries) : null
                    }
                    {
                        advanced ? this.renderIndicators(advanced.indicators) : null
                    }
                </div>
            </div>
        )

    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,

        advanced: state.datasets.advanced,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByMain,
        browseByDatasets,
        browseByThemes,
        browseByCountries,
        getDatasets,
        getIndicators,
        getIndicatorsByTheme,
        getIndicatorsByCountry,
        addLayer,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MCAdvanced);
