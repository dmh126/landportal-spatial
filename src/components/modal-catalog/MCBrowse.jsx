import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {browseByDatasets, browseByThemes, browseByCountries} from '../../actions/index';

class MCBrowse extends Component {

    constructor(props) {

        super();

        this.state = {};
    }


    render() {

        return (
            <div className="mc-browse">
                <div className="mc-browse-header">Browse by</div>

                <div
                    className="mc-browse-entry"
                    onClick={() => this.props.browseByDatasets()}
                    >
                    Datasets</div>
                <div
                    className="mc-browse-entry"
                    onClick={() => this.props.browseByThemes()}
                    >Themes</div>
                <div
                    className="mc-browse-entry"
                    onClick={() => this.props.browseByCountries()}
                    >Countries</div>
            </div>
        )
    }

};

function mapStateToProps(state) {

    return {

    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByThemes: browseByThemes,
        browseByDatasets: browseByDatasets,
        browseByCountries: browseByCountries,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(MCBrowse);
