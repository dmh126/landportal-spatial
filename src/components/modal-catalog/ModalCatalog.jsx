import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Draggable from 'react-draggable';
import {FaAngleDoubleUp} from 'react-icons/fa';
import {toLonLat} from 'ol/proj';

import {getDatasets, getIndicators, showModalCatalog, addLayer, getIndicatorsAdvanced} from '../../actions/index';

import AdvancedSearchForm from '../forms/AdvancedSearchForm';
import Spinner from '../loaders/Spinner';
import Resizer from '../resizer/Resizer';

import {CONCEPTS, THEMES} from '../../constants/LandVoc.js';

import RasterImg from '../../assets/raster.png';
import VectorImg from '../../assets/vector.png';

import './style.scss';

class ModalCatalog extends Component {

    constructor(props) {

        super();

        this.state = {
            search: '',
            search_theme: '',
            search_concept: '',
            form: false,
            selected_dataset: null,
            offsetLeft: null,
            offsetTop: null,
            datasets_advanced: false,

            browse: true,
            by_dataset: false,
            by_theme: false,
            by_country: false,
            results: true,

        };
    }

    componentDidMount() {

        this.props.getDatasets();

    }

    searchBasic = (e) => {

        let phrase = e.target.value;

        this.setState({search: phrase});

    }

    searchTheme = (e) => {

        let theme = e.target.value;

        this.setState({search_theme: theme});

    }

    searchConcept = (e) => {

        let concept = e.target.value;

        this.setState({search_concept: concept});

    }

    renderHeader = (dataset) => {

        if (dataset) {

            return (
                <span className="ds-selected">{dataset.name}</span>
            )

        } else {

            return (
                <span className="-dsnot-selected">Select layers</span>
            )

        }

    }

    getIndicatorsByDatasetId = (item) => {
        const {_id, name, reference} = item;
        this.props.getIndicators(_id, reference);
        this.setState({
            selected_dataset: {
                id: _id,
                name
            }
        });
    }

    searchWords = (s, target) => {

        let arr = s.toLowerCase().split(' ');
        let words = arr.map( item => {

            if (target.toLowerCase().includes(item)) {
                return true;
            } else {
                return false;
            }

        });

        return !words.includes(false);
    }

    renderDatasetsList = (datasets) => {
        datasets = datasets.filter(d => {
            const n = this.searchWords(this.state.search, d.name); //d.name.toLowerCase().includes(this.state.search.toLowerCase());
            const t = d.meta.dataset.related_themes.toLowerCase().includes(this.state.search_theme.toLowerCase());
            const c = d.meta.dataset.related_landvoc_concepts.toLowerCase().includes(this.state.search_concept.toLowerCase());

            if (n && t && c) {
                return true;
            } else {
                return false;
            };

        });

        return (
            <div className="mcatalog-list-datasets">
                {datasets.map((item, index) => {

                    let cname = "mcatalog-list-entry";
                    if (this.state.selected_dataset) {
                        if (this.state.selected_dataset.id === item._id) {
                            cname = cname + "-selected";
                        }
                    }

                    return (
                        <div key={index} className={cname} onClick={() => this.getIndicatorsByDatasetId(item)}>
                            {item.name}
                        </div>
                    )
                })
}
            </div>
        )
    }

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    renderIndicatorsList = (indicators) => {

        return (
            <div className="mcatalog-tiles-indicators">
                {indicators.map((item, index) => {

                    const existing = this.props.displayed_layers.find(l => l._id === item._id);

                    return (
                        <div key={index} className="mcatalog-tiles-entry">
                            <div>
                                <div className="mcatalog-tiles-top">
                                    <div className="mcatalog-tiles-top-name">{item.name}</div>
                                    {
                                        (item.meta || item.dataset) && <div className="mcatalog-tiles-top-dataset">
                                            {
                                                (item.type === 'SPARQL') ? item.meta.indicator.dataset : item.dataset.name
                                            }
                                        </div>
                                    }
                                </div>
                                <div className="mcatalog-tiles-bottom">
                                    <div className="mcatalog-tiles-type">
                                        <img src={ (['WMS', 'Raster'].includes(item.type)) ? RasterImg : VectorImg} />
                                    </div>
                                    <div className="mcatalog-tiles-add">
                                        <div className={existing
                                            ? "already-existing"
                                            : ""} onClick={() => this.addToMap(item.type, item._id)}>+</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
}
            </div>
        )

    }

    showForm = () => {

        this.setState({
            form: !this.state.form
        })
        this.getMapExtent()

    }

    renderForm = () => {

        this.form = <AdvancedSearchForm onSubmit={this.handleSubmit} onClose={this.showForm} getExtent={this.getMapExtent} />

        return this.form;
    }

    handleSubmit = (values) => {
        this.props.getIndicatorsAdvanced(values);
        this.showForm();
        this.setState({selected_dataset: {
            name: "Search results"
        }})

    }

    closeWindow = () => {

        this.props.showModalCatalog(false);

    }

    showDatasetsAdvanced = (b) => {

         this.setState({
             datasets_advanced: b,
         });

    }

    getMapExtent = () => {

        const _map = this.props.viewer.getOlMap();
        const ext = _map.getView().calculateExtent(_map.getSize());

        const min = toLonLat([ext[0], ext[1]]);
        const max = toLonLat([ext[2], ext[3]]);
        return {min, max};
    }

    renderDatasetsAdvanced = () => {

        return (
            <div className="mcatalog-search-select">

                <select onChange={(e) => this.searchTheme(e)} value={this.state.search_theme}>
                    <option value={''}>Related themes...</option>
                    {
                        THEMES.map( (item, index) => {
                            return (
                                <option key={index} value={item}>{item}</option>
                            )

                        })
                    }
                </select>

                <select onChange={(e) => this.searchConcept(e)} value={this.state.search_concept}>
                    <option value={''}>Related concepts...</option>
                    {
                        CONCEPTS.map( (item, index) => {
                            return (
                                <option key={index} value={item}>{item}</option>
                            )

                        })
                    }
                </select>

            </div>
        )

    }

    // new stuff
    chooseTab = (tab) => {

        switch(tab) {

            case "by_dataset":
                this.setState({
                    by_dataset: true, by_theme: false, by_country: false, browse: false,
                });
                break;
            case "by_theme":
                this.setState({
                    by_dataset: false, by_theme: true, by_country: false, browse: false,
                });
                break;
            case "by_country":
                this.setState({
                    by_dataset: false, by_theme: false, by_country: true, browse: false,
                });
                break;
            default:
                this.setState({
                    by_dataset: false, by_theme: false, by_country: false, browse: true,
                });

        }

    }

    renderBrowse() {

        return (
            <div className="mcatalog-body-browse-browse">
                <div className="mcatalog-browse-category">Browse:</div>
                <div className="mcatalog-browse-list">
                    <button onClick={()=>this.chooseTab('by_dataset')}>By Dataset</button>

                    <button onClick={()=>this.chooseTab('by_theme')}>By Theme</button>

                    <button onClick={()=>this.chooseTab('by_country')}>By Country</button>
                </div>
            </div>
        )
    }

    renderByDataset() {

        const datasets = this.props.datasets;

        return (
            <div className="mcatalog-body-browse-dataset">
                <div className="mcatalog-browse-category">Browse by dataset</div>
                <div className="mcatalog-browse-back">
                    <span onClick={()=>this.chooseTab()}>Back</span>
                </div>
                <div className="mcatalog-browse-list">
                    <div className="mcatalog-datasets-list">
                        {
                            datasets.map( (item, index) => {
                                return <div
                                    className="mcatalog-list-entry"
                                    key={index}
                                    onClick={() => this.getIndicatorsByDatasetId(item)}
                                    >{item.name}</div>
                            })
                        }
                    </div>
                </div>
            </div>
        )

    }


    render() {

        if (this.props.show) {
            let nIndicators = this.props.indicators.length;
            return (
                <Draggable
                    cancel=".mcatalog-list, .mcatalog-tiles, .mcatalog-form, .mcatalog-loader, .resizer"
                    onStop={this.handleStop}>
                    <div id='modal-catalog'>
                        <div className="mcatalog-head">
                            {this.renderHeader(this.state.selected_dataset)}
                            <span className="mcatalog-close" onClick={this.closeWindow}>X</span>
                        </div>
                        {/*<div className="mcatalog-list">

                            <div className="mcatalog-list-name">
                                Datasets
                            </div>

                            {this.renderDatasetsList(this.props.datasets)}

                            <div className="mcatalog-search-basic" onMouseLeave={(e) => this.showDatasetsAdvanced(false)}>
                                {
                                    this.state.datasets_advanced && this.renderDatasetsAdvanced()
                                }
                                <div className="mcatalog-search-input">
                                    <input type="text" placeholder="Search..." onChange={(e) => this.searchBasic(e)}/>
                                </div>
                                <div className="mcatalog-search-expand" onMouseEnter={(e) => this.showDatasetsAdvanced(true)}><FaAngleDoubleUp /></div>

                            </div>

                        </div>*/}
                        {/*
                            !this.state.form && <div className="mcatalog-tiles">

                                <div className="mcatalog-number">
                                    {
                                        nIndicators ? 'Number of indicators: ' + nIndicators : 'Select a dataset from the list to display available indicators or use'
                                    } <span className="mcatalog-search-advanced" onClick={this.showForm}>advanced search</span>
                                </div>

                                {this.renderIndicatorsList(this.props.indicators)}
                            </div>
                        }
                        {
                            this.state.form && <div className="mcatalog-form">
                                {this.renderForm()}
                            </div>
                        }
                        {
                            this.props.loading && <div className="mcatalog-loader">
                                <Spinner/>
                                <div className="spinner-label">Loading...</div>
                            </div>
                        */}
                        <div className="mcatalog-body">
                            <div className="mcatalog-body-search">

                                    <div className="mcatalog-body-input">
                                        <input type="text" id="search" name="search"/>
                                    </div>
                                    <div className="mcatalog-body-button">
                                        <button>Search</button>
                                    </div>

                            </div>
                            <div className="mcatalog-body-browse">
                            {
                                this.state.browse && this.renderBrowse()
                            }
                            {
                                this.state.by_dataset && this.renderByDataset()
                            }

                            </div>
                        </div>
                        {/*<Resizer container={"modal-catalog"}/>*/}
                    </div>
                </Draggable>
            )
        } else {
            return null;
        }
    }

}

function mapStateToProps(state) {

    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        show: state.viewer.modal_catalog,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        loading: state.datasets.loading
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasets: getDatasets,
        getIndicators: getIndicators,
        showModalCatalog: showModalCatalog,
        addLayer: addLayer,
        getIndicatorsAdvanced: getIndicatorsAdvanced
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ModalCatalog);
