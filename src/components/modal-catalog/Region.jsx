import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdInfoOutline} from 'react-icons/md';
import {FaAngleUp, FaAngleDown} from 'react-icons/fa';

import {browseByMain, browseAdvanced, getDatasets, getIndicators, addLayer, getIndicatorsByCountry, showIndicatorInfo} from '../../actions/index';

class Region extends Component {

    constructor(props) {

        super();

        this.state = {
            selected_country: null,
            show: false,
        };
    }

    componentDidMount() {
    }

    /*selectCountry = (item) => {

        if (item) {
            this.props.getIndicatorsByCountry({country: item.code});
        } else {
            if (this.props.advanced) {
                this.props.browseAdvanced()
            }
        }

        this.setState({
            selected_country: item,
        })

    }*/

    addToMap = (type, id) => {
        const existing = this.props.displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = this.props.displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    toggleRegion = () => {

        let s = this.state.show;

        this.setState({show: !s});
    }


    render() {

        const item = this.props.region;

        return(
            <div>
                <div>
                    <span
                        className="mc-region"
                        onClick={()=>this.props.selectCountry(item)}
                        style={{paddingLeft: (item.countries) ? '5px' : 0}}
                        >
                        {item.region}
                    </span>
                    {item.countries && <span className="icon" onClick={this.toggleRegion}>
                        {this.state.show ? <FaAngleUp /> : <FaAngleDown />}
                    </span>}
                </div>
                {
                    (item.countries && this.state.show) && item.countries.map( (country, ind) => {
                        return <div key={ind} className="mc-country" onClick={()=>this.props.selectCountry(country)}>{country.country}</div>
                    })
                }
            </div>
        )

    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,

        selected_item: state.catalog.selected_item,
        advanced: state.datasets.advanced,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        browseByMain,
        browseAdvanced,
        getDatasets,
        getIndicators,
        getIndicatorsByCountry,
        addLayer,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Region);
