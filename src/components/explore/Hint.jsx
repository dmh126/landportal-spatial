import React, {Component} from 'react';
import {FaHandPointRight} from 'react-icons/fa'

const Hint = (props) => {

    return (
        <div className="explore-by-datasets">
            <FaHandPointRight />
        </div>
    )
}

export default Hint;
