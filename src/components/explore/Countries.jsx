import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {MdInfoOutline} from 'react-icons/md';

import Region from './Region';

import {
    getIndicatorsByCountry
} from '../../actions/index';

import {REGIONS} from '../../constants/spatial'

class Countries extends Component {

    constructor(props) {

        super();

        this.state = {
            selected_country: null,
        };
    }


    selectCountry = (item) => {

        if (item) {
            this.props.getIndicatorsByCountry({country: item.code});
        }

        this.setState({
            selected_country: item,
        })

    }


    renderCountries = () => {

        return (
            <div className="explore-countries">
                <div className="explore-countries-list">
                    {
                        REGIONS.map( (item, index) => {

                            return (
                                <div
                                    className="explore-countries-entry"
                                    key={index}
                                    >
                                    <Region
                                        region={item}
                                        selectCountry={this.selectCountry}
                                        selected= {this.state.selected_country}
                                        />
                                </div>
                            )

                        })
                    }
                </div>
            </div>
        )
    }

    render() {
        return this.renderCountries()
    }

};

function mapStateToProps(state) {
    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,

        selected_item: state.catalog.selected_item,
        advanced: state.datasets.advanced,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({

        getIndicatorsByCountry,
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Countries);
