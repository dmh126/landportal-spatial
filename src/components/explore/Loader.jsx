import React, {Component} from 'react';

import Spinner from '../loaders/Spinner';

const Loader = () => {
    return (
        <div className="explore-loader">
            <Spinner />
        </div>
    );
};

export default Loader;
