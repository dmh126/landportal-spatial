import React, {Component} from 'react';

import Conflicts from '../../assets/themes/conflicts.png';
import Corruption from '../../assets/themes/corruption.png';
import Environment from '../../assets/themes/environment.png';
import FLR from '../../assets/themes/FLR.png';
import FoodSecurity from '../../assets/themes/food_security.png';
import ForestTenure from '../../assets/themes/forest_tenure.png';
import Gender from '../../assets/themes/gender.png';
import Indigenous from '../../assets/themes/indigneous.png';
import Investments from '../../assets/themes/investments.png';
import PostConflict from '../../assets/themes/post_conflict.png';
import Rangelands from '../../assets/themes/rangelands.png';
import Urban from '../../assets/themes/urban.png';
import Stakeholders from '../../assets/themes/stakeholders.png';
import Socio from '../../assets/themes/socio.png';

const mapImgToName = {
    "Forest Landscape Restoration" : FLR,
    "Forest Tenure": ForestTenure,
    "Indigenous & Community Land Rights": Indigenous,
    "Land & Corruption": Corruption,
    "Land & Food Security": FoodSecurity,
    "Land & Gender": Gender,
    "Land & Investments": Investments,
    "Land Conflicts": Conflicts,
    "Land Stakeholders & Institutions": Stakeholders,
    "Land in post-conflict settings": PostConflict,
    "Land, Climate Change & Environment": Environment,
    "Rangelands, Drylands & Pastoralism": Rangelands,
    "Socio-Economic & Institutional Context": Socio,
    "Urban Tenure": Urban,
}


const Issues = (props) => {

    const {themes, selectTheme, selected} = props;

    return (
        <div className="explore-by-issues">
            {
                themes.map( (item, index) => {
                    let cn = "explore-by-issues-entry";
                    if (selected === item) {
                        cn += " selected";
                    }
                    return (
                        <div
                            className={cn}
                            key={index}
                            onClick={()=>selectTheme(item)}
                            ><img src={mapImgToName[item]}/><span>{item}</span></div>
                    );
                })
            }
        </div>
    )
}

export default Issues;
