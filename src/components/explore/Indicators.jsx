import React, {Component} from 'react';

import Loader from './Loader';

const Indicators = (props) => {

    const {indicators, addToMap, displayed, loading} = props;

    const displayedIds = displayed.map(i => i._id);

    const goTo = (meta, resource) => {
        let url;
        switch (resource) {
            case 'dataset':
                url = `https://landportal.org/book/datasets/${meta.reference}`;
                break;
            case 'indicator':
                url = `http://landportal.org/book/indicator/${meta.id.split('.').join("")}`;
                break;
            case 'provider':
                url = meta.organizationUri;
                break;
        }
        if (url) window.open(url);
    }

    return (
        <div className="explore-indicators">
            {loading && <Loader />}
            <div className="explore-indicators-header">Layers</div>
            <div className="explore-indicators-list">
                {
                    indicators.map( (item, index) => {

                        const meta = item.meta.indicator;
                        const cn = (displayedIds.includes(item._id) || (displayedIds.length > 2)) ? "indicators-entry-add selected-layer" : "indicators-entry-add";
                        return (
                            <div
                                key={index}
                                className="explore-indicators-list-entry"
                                >
                                <div
                                    className={cn}
                                    onClick={()=>addToMap(item.type, item._id)}
                                    >
                                    + Add to map
                                </div>
                                <div className="indicators-entry-info">
                                    <div className="indicator-name" >{item.name}</div>
                                    <div className="indicator-unit">Measurement unit: <span>{meta.measurement_unit}</span></div>
                                    <div className="indicator-description">{item.about}</div>
                                    <div className="indicator-more" onClick={()=>goTo(meta, 'indicator')}>Read more on the Land Portal</div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default Indicators;
