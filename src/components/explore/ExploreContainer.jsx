import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getDatasets,
    getIndicators,
    showModalCatalog,
    addLayer,
    getIndicatorsAdvanced,
    advancedSearch,
    browseAdvanced,
    showIndicatorInfo
} from '../../actions/index';

import ExploreBy from './ExploreBy';
import Datasets from './Datasets';
import Indicators from './Indicators';
import Spinner from '../loaders/Spinner';

import './style.scss';

class ModalCatalog extends Component {

    constructor(props) {

        super();

        this.state = {
            browse: true,
            query: '',
            selected_dataset: null,
            selected_theme: null
        };
    }

    handleQueryInput = (e) => {
        this.setState({query: e.target.value});
    }

    search = () => {
        const q = {
            query: this.state.query
        };
        this.props.advancedSearch(q);
        this.props.browseAdvanced();
    }

    onEnter = (e) => {
        if (e.key === 'Enter') {
            this.search();
        }
    }

    selectTheme = (t) => {
        this.setState({selected_theme: t});
    };


    renderLoading = () => {

        if (this.props.loading) {

            return (
                <div className="mc-loading-backdrop">
                    <Spinner />
                </div>
            )

        } else {

            return null;

        }

    }


    closeInfo = () => {
        this.props.showIndicatorInfo(null);
    }

    selectIndicators = (id) => {

        this.setState({selected_dataset: id});
        this.props.getIndicators(id, null, this.state.selected_theme);
    }

    addToMap = (type, id) => {

        const {displayed_layers} = this.props;
        if (displayed_layers.length > 2) {
            return 0;
        }
        const existing = displayed_layers.find(l => l._id === id);
        if (existing) {
            return 0;
        }
        const _map = this.props.viewer;
        const zIndex = displayed_layers.length + 1;
        this.props.addLayer(_map, type, id, zIndex);

    }

    render() {

        const {show, datasets, indicators, displayed_layers, loading, advanced} = this.props;
        const {selected_dataset} = this.state;

        if (show) {
            return (
                <div id="explore-container">
                    <ExploreBy select={this.selectTheme} />
                    <Datasets
                        datasets={datasets}
                        selectIndicators={this.selectIndicators}
                        loading={loading || advanced}
                        selected={selected_dataset}
                        />
                    <Indicators
                        indicators={indicators}
                        addToMap={this.addToMap}
                        displayed={displayed_layers}
                        loading={loading || advanced}
                        />
                </div>
            )
        } else {
            return null;
        }
    }

};

function mapStateToProps(state) {

    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        show: state.viewer.modal_catalog,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        loading: state.datasets.loading,

        active_tab: state.catalog.active_tab,
        advanced: state.datasets.advanced,
        indicator_info: state.catalog.indicator_info,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasets,
        getIndicators,
        showModalCatalog,
        addLayer,
        getIndicatorsAdvanced,
        advancedSearch,
        browseAdvanced,
        showIndicatorInfo
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ModalCatalog);
