import React, {Component} from 'react';

import Loader from './Loader';
import Picture from '../../assets/default.jpg';

const Datasets = (props) => {

    const {datasets, selectIndicators, selected, loading} = props;

    const arrayBufferToBase64 = (buffer) => {
        let binary = '';
        let bytes = [].slice.call(new Uint8Array(buffer));
        bytes.forEach((b) => binary += String.fromCharCode(b));
        return window.btoa(binary);
    };

    const goTo = (meta, resource) => {
        let url;
        switch (resource) {
            case 'dataset':
                url = `https://landportal.org/book/dataset/${meta.reference || meta.meta.dataset.id}`;
                break;
            case 'indicator':
                url = `http://landportal.org/book/indicator/`;
                break;
            case 'provider':
                url = meta.organizationUri;
                break;
        }
        if (url) window.open(url);
    }

    return (
        <div className="explore-datasets">
            {loading && <Loader />}
            <div className="explore-datasets-header">Datasets</div>
            <div className="explore-datasets-list">
                {
                    datasets.map( (item, index) => {
                        const meta = item.meta.dataset;
                        let cn = "explore-datasets-list-entry";
                        if (selected === item._id) cn += " selected";
                        let image = Picture;
                        if (meta.logo) {

                            if (typeof meta.logo === 'string') {
                                image = meta.logo;
                            } else {
                                image = (meta.logo.url) ?
                                    meta.logo.url :
                                    `data:${meta.logo.contentType};base64, ${arrayBufferToBase64(meta.logo.data.data)}`;
                            }

                        }
                        return (
                            <div
                                key={index}
                                className={cn}
                                >
                                <div className="datasets-entry-image">
                                    <img src={image} />
                                </div>
                                <div className="datasets-entry-info">
                                    <div className="dataset-name" onClick={() => selectIndicators(item._id)}>{item.name}</div>
                                    <div className="dataset-provider" onClick={() => goTo(meta, 'provider')}>Provider: <span>{meta.organization}</span></div>
                                    <div className="dataset-description">{item.about}</div>
                                    <div className="dataset-more" onClick={() => goTo(item, 'dataset')}>Read more on the Land Portal</div>
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        </div>
    )
}

export default Datasets;
