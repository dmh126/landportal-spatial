import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {FaDatabase, FaMapMarkerAlt, FaLayerGroup} from 'react-icons/fa';

import {
    getDatasets,
    getIndicators,
    showModalCatalog,
    addLayer,
    getIndicatorsAdvanced,
    advancedSearch,
    browseAdvanced,
    showIndicatorInfo,
    getDatasetsByTheme
} from '../../actions/index';

import Issues from './Issues';
import Countries from './Countries';
import Hint from './Hint';

import './style.scss';

class ExploreBy extends Component {

    constructor() {

        super();

        this.state = {
            tab: 1,
            selected_theme: null,
        };

    }

    componentDidMount(){
        if (this.state.tab === 0) {
            if (!this.props.advanced) {
                this.props.getDatasets();
            }
            //this.props.getDatasets();
        }
    }

    changeTab = (tab) => {

        if (tab === 0) {
            this.props.getDatasets();
        };
        this.setState({tab, selected_theme: null});
        this.props.select(null);
    }

    renderHead = (tab) => {

        const icons = {
            "Datasets": <FaDatabase />,
            "Issues": <FaLayerGroup />,
            "Regions": <FaMapMarkerAlt />
        };

        const tabs = ["Datasets","Issues", "Regions"]; // add "Regions"

        return (
            <div className="explore-by-head">
                <span className="first">Explore by {tabs[tab]} </span>
                <span className="second">Or explore by
                    {
                        tabs.map( (item, index) => {
                            if (index !== tab) {
                                return (
                                    <span
                                        className="clickable"
                                        onClick={() => this.changeTab(index)}
                                        key={index}
                                        >{icons[item]}{item}</span>
                                );
                            }
                        })
                    }
                </span>
            </div>
        )
    };

    selectDatasetsByTheme = (theme) => {
        this.props.getDatasetsByTheme({theme});
        this.setState({selected_theme: theme})
        this.props.select(theme);
    }

    renderBody = (tab) => {

        switch(tab) {
            case 0:
                return <Hint />;
            case 1:
                return (
                    <Issues
                        themes={this.props.themes}
                        selectTheme={this.selectDatasetsByTheme}
                        selected={this.state.selected_theme}
                        />
                );
            case 2:
                return (<Countries />);
            default:
                return null;
        }

    }

    render() {

        const {tab} = this.state;

        return (
            <div className="explore-by">
                {this.renderHead(tab)}
                {this.renderBody(tab)}
            </div>
        )

    }

};


function mapStateToProps(state) {

    return {
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
        show: state.viewer.modal_catalog,
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        loading: state.datasets.loading,
        themes: state.viewer.themes,
        active_tab: state.catalog.active_tab,
        advanced: state.datasets.advanced,
        indicator_info: state.catalog.indicator_info
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDatasets,
        getIndicators,
        showModalCatalog,
        addLayer,
        getIndicatorsAdvanced,
        advancedSearch,
        browseAdvanced,
        showIndicatorInfo,
        getDatasetsByTheme
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ExploreBy);
