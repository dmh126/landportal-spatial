import React from 'react';
import { Field, reduxForm } from 'redux-form';

import {TYPES, INITIAL_VALUES as initialValues} from '../../constants/catalog.js';

const SearchForm = props => {

  const { handleSubmit, pristine, reset, submitting, onExpand } = props;

  return (

    <form onSubmit={handleSubmit}>

      <div className="search-field">
        <label className="search-label">Name</label>
        <div>
          <Field
            name="name"
            component="input"
            type="text"
            placeholder="Name"
          />
        </div>
      </div>
      {/*
      <div className="search-field">
        <label className="search-label">Types</label>
        <div>
          {
            TYPES.map( (item, index) => {
              return (
                <label key={index}><Field
                  name={`type-${item}`}
                  id={`type-${item}`}
                  component="input"
                  type="checkbox"
                />{item}</label>
              )
            })
          }
        </div>
      </div>
      */}

      <div className="search-field">
        <label className="search-label">Created at</label>
        <div>
        <label>From </label>
          <Field
            name="created_at_from"
            component="input"
            type="date"
          />
      <label> To </label>
          <Field
            name="created_at_to"
            component="input"
            type="date"
          />
        </div>
      </div>

      {
          props.advanced && <div className="search-advanced">Advanced search is curently not available.</div>
      }


      <div className="search-buttons">
        <div className="search-buttons-form">
            <button id="submit" type="submit">
              Search
            </button>
            <span id="reset" disabled={pristine || submitting} onClick={reset}>
              Reset
            </span>
        </div>
        <div className="search-buttons-advanced">
            <span
                onClick={onExpand}>
                {(props.advanced) ? "Hide" : "Advanced"}
                <i className={(props.advanced) ? "fas fa-angle-up" : "fas fa-angle-down"}></i>
            </span>
        </div>
      </div>

    </form>
  )
}

export default reduxForm({
  form: 'search-form', // a unique identifier for this form
  initialValues
})(SearchForm)
