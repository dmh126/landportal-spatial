import React from 'react';
import {Field, reduxForm} from 'redux-form';

import {TYPES, INITIAL_VALUES as initialValues} from '../../constants/catalog.js';
import {CATEGORIES, THEMES, CONCEPTS} from '../../constants/LandVoc.js';
import {COUNTRIES, REGIONS} from '../../constants/spatial.js';

const setBoundingBox = (getExtent, change) => {

    const ext = getExtent();

    change('minlon', ext.min[0]);
    change('minlat', ext.min[1]);
    change('maxlon', ext.max[0]);
    change('maxlat', ext.max[1]);
}

const SearchForm = props => {

    const {handleSubmit, pristine, reset, submitting, onExpand, onClose, getExtent, change} = props;

    return (

        <form onSubmit={handleSubmit}>

            <div className="separator">Basic</div>

            <div className="search-row">

                <div className="search-field">
                  <label className="search-label">Types</label>
                  <div>
                    {
                      TYPES.map( (item, index) => {
                        return (
                          <label key={index} className="search-label"><Field
                            name={`type-${item}`}
                            id={`type-${item}`}
                            component="input"
                            type="checkbox"
                          />{item}</label>
                        )
                      })
                    }
                  </div>
                </div>

            </div>

            <div className="separator">Metadata</div>

            <div className="search-row">

                <div className="search-field">
                    <label className="search-label">Label</label>
                    <div>
                        <Field name="label" component="input" type="text" placeholder="Label"/>
                    </div>
                </div>

                <div className="search-field">
                    <label className="search-label">Description</label>
                    <div>
                        <Field name="description" component="input" type="text" placeholder="Description"/>
                    </div>
                </div>

            </div>

            <div className="search-row">

                <div className="search-field">
                    <label className="search-label">Measurement unit</label>
                    <div>
                        <Field name="measurement_unit" component="input" type="text" placeholder="Unit"/>
                    </div>
                </div>

                <div className="search-field">
                    <label className="search-label">Organization</label>
                    <div>
                        <Field name="organization" component="input" type="text" placeholder="Organization"/>
                    </div>
                </div>

            </div>

            <div className="search-row">

                <div className="search-field">
                    <label className="search-label">Related themes</label>
                    <div>
                        <Field name="related_themes" component="select">
                            <option value={""}>---</option>
                            {
                                THEMES.map( (item,index) => {
                                    return <option key={index} value={item}>{item}</option>
                                })
                            }
                        </Field>
                    </div>
                </div>

                <div className="search-field">
                    <label className="search-label">Related concepts</label>
                    <div>
                        <Field name="related_concepts" component="select">
                          <option value={""}>---</option>
                          {
                              CONCEPTS.map( (item,index) => {
                                  return <option key={index} value={item}>{item}</option>
                              })
                          }
                        </Field>

                    </div>
                </div>

            </div>

            <div className="separator">Location</div>

            <div className="search-row">

                <div className="search-field">
                    <label className="search-label">Region</label>
                    <div>
                        <Field name="region" component="select">
                            <option value={""}>---</option>
                            {
                                REGIONS.map( (item,index) => {
                                    return <option key={index} value={item.code}>{item.region}</option>
                                })
                            }
                        </Field>
                    </div>
                </div>

                <div className="search-field">
                    <label className="search-label">Country</label>
                    <div>
                        <Field name="country" component="select">
                          <option value={""}>---</option>
                          {
                              COUNTRIES.map( (item,index) => {
                                  return <option key={index} value={item.code}>{item.country}</option>
                              })
                          }
                        </Field>

                    </div>
                </div>

            </div>

            <div className="search-row">

                <div className="search-field-spatial">
                    <label className="search-label">Min Latitude</label>
                    <div>
                        <Field name="minlat" component="input" type="number"/>
                    </div>
                </div>

                <div className="search-field-spatial">
                    <label className="search-label">Min Longitude</label>
                    <div>
                        <Field name="minlon" component="input" type="number"/>
                    </div>
                </div>

                <div className="search-field-spatial">
                    <label className="search-label">Max Latitude</label>
                    <div>
                        <Field name="maxlat" component="input" type="number"/>
                    </div>
                </div>

                <div className="search-field-spatial">
                    <label className="search-label">Max Longitude</label>
                    <div>
                        <Field name="maxlon" component="input" type="number"/>
                    </div>
                </div>

                <div className="grab-extent" onClick={() => setBoundingBox(getExtent, change)}>Current map extent</div>

            </div>

            <div className="search-buttons">
                <div className="search-buttons-form">
                    <button id="submit" type="submit">
                        Search
                    </button>
                    <span id="reset" disabled={pristine || submitting} onClick={reset}>
                        Reset
                    </span>

                    <span style={{marginLeft: '5px', cursor: 'pointer'}} onClick={onClose}>
                         Close
                    </span>
                </div>
            </div>

        </form>
    )
}



export default reduxForm({
    form: 'search-form', // a unique identifier for this form
    initialValues
})(SearchForm)
