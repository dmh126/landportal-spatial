import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {MdHome} from 'react-icons/md';

import './style.scss';

class NavBar extends Component {

  render() {
    return (
      <header>
        <nav>
          <ul>
            <li>
              <Link className="blue" to="/">
                <MdHome />
              </Link>
            </li>
            <li>
              <Link className="orange" to="/catalog">
                Catalog
              </Link>
            </li>
            <li>
              <Link className="red" to="/how-to">
                How To
              </Link>
            </li>
            <li>
              <a className="teal" href="https://landportal.org" target="_blank">
                Land Portal Home
              </a>
            </li>
            <li>
            </li>
          </ul>
        </nav>
      </header>
    )
  }
}

function mapStateToProps(state) {
    return {
        testing: state.testing,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        testAction: null,
    }, dispatch)
}

export default NavBar;
