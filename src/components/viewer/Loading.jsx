import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Spinner from '../loaders/Spinner';

const style = {
    position: "absolute",
    zIndex: "9999",
    width: "100%",
    height: "100%",
    textAlign: "center",
    backgroundColor: "rgba(0,0,0,0.2)",
    color: "#fff",
    paddingTop: '10%',
    lineHeight: '50px',
}


class Loading extends Component {



    render() {

        if (this.props.show) {

            return (
                <div style={style}>
                    <Spinner />
                    <div>Loading map... Please wait... </div>
                </div>
            );

        } else {

            return null;

        }

    }
}

function mapStateToProps(state) {

    return {show: state.compositions.loading}
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(Loading);
