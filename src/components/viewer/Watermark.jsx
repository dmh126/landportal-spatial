import React, {Component} from 'react';
import Logo from '../../assets/logo-dark.png';

const styleDiv = {
    position: "absolute",
    zIndex: 10,
    bottom: 5,
    left: 10,
    cursor: "pointer"
}

const styleImg = {
    height: 50
}

export default class Watermark extends Component {

    goHome() {
        window.open('http://landportal.org');
    }

    render() {
        return (
            <div className="watermark noselect" style={styleDiv} onClick={this.goHome}>
                <img src={Logo} style={styleImg}/>
            </div>
        )
    }
}
