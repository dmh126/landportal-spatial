import React, {Component} from 'react';
import {isMobile} from 'react-device-detect';


export default class Intro extends Component {

    constructor(){
        super();

        this.state = {
            show: false,
            notShowAgain: false,
        }
    }

    componentDidMount() {
        const skipped = localStorage.getItem('skip_tour');
        if (!skipped) {
            this.setState({show: true});
        }
    }

    closeWarning = () => {
        if (this.state.notShowAgain) {
            localStorage.setItem('skip_tour', true);
        }
        this.setState({
            show: false
        });
    }

    onStart = () => {
        if (this.state.notShowAgain) {
            localStorage.setItem('skip_tour', true);
        }
        this.setState({show: false});
        this.props.startTour();
    }

    handleCheckbox = (e) => {
        this.setState({notShowAgain: e.target.checked})
    }

    render() {
        if (!isMobile && this.state.show) {
            return (
                <div className="intro-backdrop" >
                    <div className="intro-message">
                        <div>Welcome to the Land Portal’s Geoportal! If you’re new to the platform, we can take you through a short tour and get you up to speed.  </div>

                        <a onClick={this.closeWarning} className="introjs-button introjs-skipbutton">Exit</a>
                        <a onClick={this.onStart} className="introjs-button introjs-skipbutton">Start</a>

                        <div className="not-show">
                            <input onChange={this.handleCheckbox} type="checkbox" /> Don’t show again
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return null;
        }
    }
}
