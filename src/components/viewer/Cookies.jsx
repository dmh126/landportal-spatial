import React, {Component} from 'react';
import Cookies from 'js-cookie'

export default class Warning extends Component {

    constructor(){
        super();

        this.state = {
            show: false,
        }
    }

    componentDidMount(){
        const consent = Cookies.get('has_consent');
        if (!consent) {
            this.setState({show: true});
        }
    }

    closeCookies = () => {
        this.setState({show: false});
        Cookies.set('has_consent', 1, {expires: 365});
    }

    goTo = (url) => {
        window.open(url);
    }

    render() {
        if (this.state.show) {
            return (
                <div className="cookies-info" >
                    <h4>Geoportal uses cookies to enhance your experience</h4>
                    <p>
                        By clicking any link on this page you are giving your consent for us to set cookies.
                        We do not store, sell or share any of your personal information with third parties,
                        unless you have explicitly agreed to or we are required to by law.
                        We want to be completely transparency on what personal data we store and how that is used.
                        Check our <span onClick={() => this.goTo('https://landportal.org/about/privacy-policy')}>Privacy Policy</span> for more information.
                    </p>
                    <div onClick={this.closeCookies}>Accept</div>
                </div>
            )
        }
        else {
            return null;
        }
    }
}
