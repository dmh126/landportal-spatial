import React, {Component} from 'react';
import {isMobile} from 'react-device-detect';

const styleBackdrop = {
    position: "absolute",
    zIndex: 40,
    width: '100vw',
    height: '100vh',
    background: 'rgba(0,0,0,0.7)'
}

const styleMessage = {
    position: "absolute",
    margin: "auto",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: "#ebebe5",
    width: '200px',
    height: '150px',
    textAlign: 'center',
    padding: '10px'
}

const styleButton = {
    color: '#0066CC',
    cursor: 'pointer',
    lineHeight: '35px'
}

export default class Warning extends Component {

    constructor(){
        super();

        this.state = {
            show: true,
        }
    }

    closeWarning = () => {
        this.setState({
            show: false
        })
    }

    render() {
        if (this.state.show && isMobile) {
            return (
                <div style={styleBackdrop} >
                    <div style={styleMessage}>
                        <div>Geoportal is not optimized for mobile devices. Please use desktop view. </div>
                        <div style={styleButton} onClick={this.closeWarning}>OK</div>
                    </div>
                </div>
            )
        }
        else {
            return null;
        }
    }
}
