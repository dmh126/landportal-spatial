import React, {Component} from 'react';

import Viewer from './Viewer';
import Panel from '../map-widgets/panel/Panel';
//import SwitchKD from '../map-widgets/controls/SwitchKD';
//import CaptureImage from '../map-widgets/controls/CaptureImage';
//import OpenCatalog from '../map-widgets/controls/OpenCatalog';
import Bar from '../map-widgets/controls/Bar';

export default class ViewerContainer extends Component {

  constructor(props){

    super();

    const search = props.location.search;

    this.params = new URLSearchParams(search);

  }


  render() {
    return (
      <div>
          <Panel />
          <Bar />
          <Viewer search={this.params} />
      </div>
    )
  }
}
