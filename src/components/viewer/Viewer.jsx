import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import ReactGA from "react-ga";

import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';

import {
    viewerCreated,
    addDisplayedLayer,
    selectFeature,
    showModalCatalog,
    addLayer,
    getComposition,
    getThemes,
    getConcepts,
    compositionIdReceived,
    showHelp,
    showLayerSettings,
    selectLayer,
    getExampleDataset
} from '../../actions/index';
import DatasetsService from '../../services/datasetsService';

import Watermark from './Watermark';
import Loading from './Loading';

import LayerInfo from '../map-widgets/panel/LayerInfo/LayerInfo';
import AttributesTable from '../map-widgets/panel/AttributesTable/AttributesTable';
import FeatureInfo from '../map-widgets/feature-info/FeatureInfo';
import LayerSettings from '../map-widgets/panel/LayerSettings/LayerSettings';
import ModalCatalog from '../modal-catalog/NewModalCatalog';
import BaseMapPicker from '../map-widgets/base-map-picker/BaseMapPicker';
import ShareWindow from '../map-widgets/share-window/ShareWindow';
import MapLegend from '../map-widgets/map-legend/MapLegend';
import Warning from './Warning';
import ExploreContainer from '../explore/ExploreContainer';
import Cookies from './Cookies';
import HowToPage from '../how-to/HowToPage';
import Intro from './Intro';

// Openlayers
import Map from 'ol/Map';
import View from 'ol/View';
import {defaults as defaultControls, ScaleLine} from 'ol/control.js';
import {ZoomSlider} from 'ol/control.js';
import {get} from 'ol/proj';

import {createBaseMap, createWmsLayer, createVectorLayer, createSparqlLayer} from './helpers';

import {introSteps, introOpts} from '../../assets/steps';

//import 'ol/ol.css';

// Openlayers-Cesium
import OLCesium from 'olcs/OLCesium.js';

// style
import './style.scss';

class Viewer extends Component {

    constructor() {
        super();

        this.state = {
            notFound: false,
            tour: false,
        };

    }

    componentDidMount() {

        ReactGA.initialize('UA-153086335-1');
        ReactGA.pageview(window.location.pathname + window.location.search);

        const {search} = this.props;

        const props = this.props;

        const baseMap = createBaseMap();

        const scaleLineControl = new ScaleLine();

        const map2d = new Map({
            target: 'map',
            layers: [baseMap],
            controls: defaultControls().extend([scaleLineControl]),
            view: new View({
                center: [
                    1330615.788388, 2328577.629680
                ],
                zoom: 2.8,
                minZoom: 2.8,
                maxZoom: 15,
            })
        });
        const olc = new OLWrapper(map2d);
        this.map3d = olc;

        const zoomslider = new ZoomSlider();
        map2d.addControl(zoomslider);

        map2d.on('click', this.handleMapClick);
        window.onkeydown = this.handleEscKey;

        //this.map3d.setEnabled(true);

        this.props.viewerCreated(this.map3d);
        this.props.getThemes();

        const composition = search.get('map');
        if (composition) {
            this.props.getComposition(composition, this.map3d);
        } else {
            this.addLayer(search)
        }

    }

    handleMapClick = (e) => {
        const props = this.props;
        this.collapseDropdowns(props);
        const map2d = this.map3d.getOlMap();
        let counter = 0;
        map2d.forEachFeatureAtPixel(e.pixel, function(feature, layer) {
            if (counter)
                return true;
            feature.position = {left: e.pointerEvent.clientX, top: e.pointerEvent.clientY};
            props.selectFeature(feature)
            counter++
        })
        if (!counter) {
            props.selectFeature(null)
        }
    }

    handleEscKey = (e) => {
        if (e.keyCode == 27) {
            this.collapseDropdowns(this.props)
        };
    }

    collapseDropdowns = (props) => {
        if (props.collapsibles) {
            props.showModalCatalog(false);
            props.compositionIdReceived(null);
            props.showHelp(false);
        };
    }

    addLayer = (search) => {

        const type = search.get('type');
        const id = search.get('id');

        const zIndex = this.props.displayed_layers.length + 1;
        if (id) {
            this.props.addLayer(this.map3d, type, id, zIndex);
        } else {
            this.props.addLayer(this.map3d, 'Vector', null, zIndex);
        }


    }

    renderNotFound() {
        return (<ExploreContainer/>)
    }

    selectObject() {}

    onBeforeChange = nextStepIndex => {

            if ([4,5,6].includes(nextStepIndex)) {
                this.props.showModalCatalog(true)
                if (!(this.props.datasets.length && this.props.indicators.length)) {
                    this.props.getExampleDataset();
                }
            } else {
                this.props.showModalCatalog(false)
            }
            if (nextStepIndex === 8) {
                this.props.selectLayer(this.props.displayed_layers[0]);
                this.props.showLayerSettings(1);
                this.steps.updateStepElement(nextStepIndex);
            } else {
                this.props.selectLayer(null);
                this.props.showLayerSettings(false);
            }

            if (nextStepIndex > 0) {
                this.steps.updateStepElement(nextStepIndex);
            }
    }

    startTour = () => {
        this.setState({tour: true});
    }

    exitTour = () => {
        this.props.showModalCatalog(false);
        this.props.selectLayer(null);
        this.props.showLayerSettings(false);
    }

    render() {

        return (
            <div id='map' ref={(div) => this.mapContainer = div}>
                <Steps
                  enabled={this.state.tour}
                  steps={introSteps}
                  initialStep={0}
                  onExit={this.exitTour}
                  ref={s => (this.steps = s)}
                  onBeforeChange={this.onBeforeChange}
                  options={introOpts}
                />
                <ExploreContainer />
                <ShareWindow />
                <FeatureInfo />
                <HowToPage />
                <Loading />
                <MapLegend />
                <Warning />
                <Cookies />
                <Intro startTour={this.startTour}/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        viewer: state.viewer.viewer,
        displayed_layers: state.datasets.displayed_layers,
        globe: state.viewer.globe,
        themes: state.viewer.themes,
        collapsibles: state.viewer.modal_catalog || state.compositions.composition || state.viewer.help,
        datasets: state.datasets.datasets,
        indicators: state.datasets.indicators,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        viewerCreated,
        addDisplayedLayer,
        selectFeature,
        showModalCatalog,
        addLayer,
        getComposition,
        getThemes,
        getConcepts,
        compositionIdReceived,
        showHelp,
        showLayerSettings,
        selectLayer,
        getExampleDataset
    }, dispatch)
}

class OLWrapper {

    constructor(map2d){
        this.map2d = map2d;
    }

    getOlMap() {
        return this.map2d;
    }

}

export default connect(mapStateToProps, matchDispatchToProps)(Viewer);
