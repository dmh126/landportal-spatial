import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS.js';
import XYZ from 'ol/source/XYZ';
import OSM from 'ol/source/OSM';
import WMTS from 'ol/source/WMTS';
import KML from 'ol/format/KML.js';
//import GML from 'ol/format/GML.js';
import WFS from 'ol/format/WFS.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import VectorSource from 'ol/source/Vector.js';
import VectorLayer from 'ol/layer/Vector';
import HeatmapLayer from 'ol/layer/Heatmap';
// ol styles
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Text from 'ol/style/Text';
import Circle from 'ol/style/Circle';
import RegularShape from 'ol/style/RegularShape';
import Point from 'ol/geom/Point';

import {WORLD_MAP, BASEMAPS} from '../../constants/spatial';

import {SPARQL} from '../../constants/response'; // example sparql response
import DatasetsService from '../../services/datasetsService';
import {styleParameters} from '../../helpers/index';

import Geostats from 'geostats';
import Chroma from 'chroma-js';
import axios from 'axios';


/**
*
* Creates a WMS layer from geoserver url.
*
*/
export const createWmsLayer = (params, style) => {

    const wmsLayer = new TileLayer({
        source: new TileWMS({
            url: params.wms.source_url,
            params: {
                'TILED': true
            },
            serverType: 'geoserver',
            crossOrigin: 'Anonymous'
        })
    });

    if (style && style.categorical) {
        let jenks = [];
        let ramp = [];
        Object.entries(style.categorical).sort().forEach( e => {
            jenks.push(e[0]);
            ramp.push(e[1]);
        });
        wmsLayer.set('legend', {jenks, ramp});
    }

    wmsLayer.set('layer_name', params.name, true)
    wmsLayer.set('layer_id', params._id, true)

    return wmsLayer;

}

/**
*
* Creates a vector layer from geoserver url.
*
*/
export const createVectorLayer = (params, style, proportional) => {
    let parameters = styleParameters(style);
    let categorical = style ? style.categorical : null;
    const format_info = params.vector.format;
    let format;

    switch (format_info) {

        case 'KML':
            format = new KML();
            break;

        case 'GML':
            format = new WFS(); // GML() doesn't work, must be WFS
            break;

        case 'GeoJSON':
            format = new GeoJSON();
            break;

        default:
            break;

    }

    if (format) {

        const vector = new VectorLayer({
            source: new VectorSource({url: params.vector.source_url, format: format, crossOrigin: 'Anonymous'}),
        });

        vector.set('layer_name', params.name, true)
        vector.set('layer_id', params._id, true)

        // styling
        let color = parameters.color;
        let shape = parameters.shape;
        let noind;
        vector.getSource().once('change', e => {

            const source = e.target;
            const features = source.getFeatures();

            let values = []

            let times = features[0].getKeys()
            .filter(k => k.includes('indicator'))
            .map(k => k.split('_')[1]).filter(k => k).sort();
            const time = times[times.length -1];

            features.forEach(feature => {
                let ind;
                if (time) {
                    ind = feature.get('indicator_' + time);
                } else {
                    ind = feature.get('indicator');
                }

                values.push(ind);

            });
            let isString;
            if (categorical || typeof values[0] === 'string' ) {
                isString = true;
            } else {
                isString = false;
            };
            // calculate breaks and color ramp
            let jenks = [];
            let ramp = [];
            if (isString) {
                if (categorical) {
                    Object.entries(categorical).sort().forEach( e => {
                        jenks.push(e[0]);
                        ramp.push(e[1]);
                    });
                } else {
                    jenks = Array.from(new Set(values)).sort();
                    ramp = Chroma.scale(parameters.palette).colors(jenks.length);
                }

            } else {
                values = values.filter(v => (typeof v === 'number'));
                let vl = values.length;
                if (vl === 0) {
                    jenks = [];
                    ramp = [Chroma.random()];
                    noind = true;
                } else if (vl < 6) {
                    values = values.sort((a,b)=>a-b);
                    jenks = [values[0], values[Math.floor(vl/2)], values[vl -1]];
                    ramp = Chroma.scale(parameters.palette).colors(2);
                } else {
                    const geostats = new Geostats(values);
                    jenks = geostats.getClassJenks(5);
                    ramp = Chroma.scale(parameters.palette).colors(5);
                }

            }

            vector.set('legend', {jenks, ramp, color, shape});

            vector.set('time', time);

            features.forEach(feature => {
                if (proportional) {
                    let point = getPoint(feature);

                    feature.geometry = point;
                    feature.set("geometry", point);
                }
                feature.setStyle(styleGraduatedColors(feature, jenks, {ramp, color, shape, noind}, time));
            })


        });

        vector.setOpacity(parameters.opacity/100)

        return vector;

    } else {

        return undefined;

    }

}

/*
*
* Creates a raster layer. Obsolete.
*
*/
export const createRasterLayer = (params) => {
    // TODO
    const format_info = params.raster.format;
    let format;

    switch (format_info) {

        case 'KML':
            format = new KML();
            break;

        default:
            break;

    }

    if (format) {

        const raster = new TileLayer({
            source: new WMTS({url: params.raster.source_url, crossOrigin: 'Anonymous'})
        });

        raster.set('layer_name', params.name, true)
        raster.set('layer_id', params._id, true)

        return raster;

    } else {

        return undefined;

    }

}

/**
*
* Creates a basemap depending on the type. Types are predefined.
*
*/
export const createBaseMap = (type) => {

    const base = BASEMAPS[type] || BASEMAPS['ESRI_LIGHT'];
    const url = base.url;

    let tiles;
    if (type === 'OSM') {

        tiles = new OSM();

    } else {

        tiles = new XYZ({url: url, crossOrigin: 'Anonymous'});

    }

    const baseMap = new TileLayer({source: tiles});

    baseMap.set('isBasemap', true, true);

    return baseMap;

}

/**
*
* Creates a vector layer by joining the world map and bindings from a sparql query.
*
*/
export const createSparqlLayer = (params, bindings, style, proportional, zIndex) => {

    const format = new GeoJSON();
    const randpalettes = ['OrRd', 'Blues', 'Greens'];
    const randcolors = ['#6CAF00', '#EA670D', '#1084C6'];
    const randshapes = ['circle', 'triangle', 'square'];
    // styling
    let palette, color, shape;
    if (style) {
        palette = style.properties.palette;
        color = style.properties.color;
        shape = style.properties.shape;
    } else {
        palette = zIndex ? randpalettes[zIndex - 1] : 'OrRd';
        color = zIndex ? randcolors[zIndex - 1] : '#6CAF00';
        shape = zIndex ? randshapes[zIndex - 1] : 'circle';
    }

    const opacity = style ? style.properties.opacity : 100 ;
    const categorical = style ? style.categorical : null ;

    // sparql response

    let to_merge = bindings.map(item => {

        let value = parseFloat(item.value.value);

        if (isNaN(value)) {
            value = item.value.value || 0;
        }

        return {
            country: item.country.value,
            value: value,
            time: item.time.value
        }

    });

    let isString;
    if (typeof to_merge[0].value === 'string' ) {
        isString = true;
    } else {
        isString = false;
    };
    const times = Array.from(new Set(to_merge.map(f => f.time)));
    const display_time = times[times.length - 1]
    let values = [];
    to_merge.forEach(f => {
        if (f.time === display_time) {
            values.push(f.value);
        }
    })
    /*
    let times = [];
    let values = [];
    to_merge.forEach( i => {
        values.push(i.value);
        if (!times.includes(i.time)) {
                times.push(i.time)
        }
    })*/
    let t1 = Array.from(new Set(to_merge.map(t => t.time))).sort();
    let t2 = t1[t1.length - 1];

    // calculate breaks and color ramp
    let jenks = [];
    let ramp = [];
    if (isString) {

        if (categorical) {
            Object.entries(categorical).sort().forEach( e => {
                jenks.push(e[0]);
                ramp.push(e[1]);
            });
        } else {
            //values = values.filter(v => (typeof v === 'number'))
            jenks = Array.from(new Set(values)).sort();
            ramp = Chroma.scale(palette).colors(jenks.length);
        }

    } else {
        values = values.filter(v => (typeof v === 'number'))
        let vl = values.length;
        if (vl < 3) {
            jenks = [values[0]];
            ramp = Chroma.scale(palette).colors(1);
        } else if (vl < 6) {
            values = values.sort((a,b)=>a-b);
            jenks = [values[0], values[Math.floor(vl/2)], values[vl -1]];
            ramp = Chroma.scale(palette).colors(2);
        } else {
            const geostats = new Geostats(values);
            jenks = geostats.getClassJenks(5)
            ramp = Chroma.scale(palette).colors(5);
        }
        //let n_breaks = (values.length < 6) ? values.length -1  : 5;
        //const geostats = new Geostats(values);
        //jenks = geostats.getClassJenks(n_breaks || 1)
        //ramp = Chroma.scale(palette).colors(n_breaks || 1);

    }

    const vector = new VectorLayer({
        source: new VectorSource({url: WORLD_MAP, format: format, crossOrigin: 'Anonymous'}),
        style: (feature, resolution) => {
            return styleGraduatedColors(feature, jenks, {ramp, color, shape}, display_time);
        },
        minResolution: 2000,
    });

    vector.set('time', t2);

    let featuresToAdd = [];
    vector.getSource().once('change', e => {

        const source = e.target;
        const features = source.getFeatures();

        features.forEach(feature => {
            let country = feature.get('iso_a3');
            /*let _obj = to_merge.find(element => {
                return element.country === country
            });*/
            // new
            let _objs = to_merge.filter(element => {
                return element.country === country
            });

            let _ol = _objs.length;

            if (_ol === 1) {
                feature.set('indicator_' + _objs[0].time.toString(), _objs[0].value, true);
                feature.set('times', _objs[0].time, true);
                feature.set('indicator', _objs[0].value, true);
                feature.set('time', _objs[0].time, true);
            } else if (_ol > 1) {
                let counter = 0;
                //let indicators = '';
                let times = '';
                while (counter < _ol) {
                    //indicators += ';' + _objs[counter].value;
                    times += ';' + _objs[counter].time;
                    feature.set('indicator_' + _objs[counter].time.toString(), _objs[counter].value, true);
                    counter++;
                }
                let ind_last = _objs.find(o => o.time === display_time);
                if (ind_last) feature.set('indicator', ind_last.value, true); // todo
                //feature.set('time', _objs[0].time, true);
                //feature.set('indicators', indicators, true);
                feature.set('times', times, true);

            }

            if (proportional) {
                let geom = getPoint(feature);
                feature.geometry = geom;
                feature.set("geometry", geom);
            }

            // end

        })

    })

    vector.setOpacity(opacity/100);

    vector.set('layer_name', params.name, true);
    vector.set('layer_id', params._id, true);
    vector.set('indicator_type', (isString) ? 'string' : 'number');

    vector.set('legend', {jenks, ramp, color, shape});

    return vector;

}

/**
*
* Rebuilds an existing Sparql layer. To use after turning a normal Sparql layer into a heatmap.
* It brings back the default geometry.
*
*/
export const rebuildSparqlLayer = (layer, style, proportional, zIndex) => {
    const colors = style.properties.palette;
    const features = layer.getSource().getFeatures();
    const values = features.map(f => f.get('indicator') || 0);
    let n_breaks = (values.length < 6) ? values.length -1 : 5;
    const geostats = new Geostats(values);
    const jenks = geostats.getClassJenks(n_breaks || 1)
    const ramp = Chroma.scale(colors).colors(n_breaks || 1)
    const time = style.time;

    axios.get(WORLD_MAP)
    .then( res => {
        const data = res.data.features;
        //console.log(data)
        features.forEach( feature => {
            let geom;
            if (proportional) {
                geom = getPoint(feature);

            } else {
                const id = feature.getId();
                const new_geom = data.find( f => f.id === id);
                const format = new GeoJSON().readFeature(new_geom, {featureProjection: 'EPSG:3857'});
                geom = format.getGeometry();
            }

            feature.geometry = geom;
            feature.set("geometry", geom);

            feature.setStyle( f => {
                return styleGraduatedColors(f, jenks, {ramp}, time);
            })

        })

    })
    let color = '#6CAF00';
    let shape = 'circle';

    const sparql = new VectorLayer({ source: layer.getSource() });
    sparql.set("layer_id", layer.get("layer_id"));
    sparql.set("layer_name", layer.get("layer_name"));
    sparql.set("time", time);
    sparql.set('legend', {jenks, ramp, color, shape});
    sparql.setZIndex(zIndex);

    return sparql;

}

/**
*
* Style function. Create a graduated colors style depending on the indicator.
*
*/
export const styleGraduatedColors = (feature, jenks, style, time, label) => {

    let ramp = style.ramp;
    let pcolor = style.color;
    let shape = style.shape;
    let noind = style.noind;
    let indicator;

    if (time) {
        indicator = feature.get('indicator_' + time);
    } else {
        indicator = feature.get('indicator');
    }

    let text;
    if (indicator) {
        let val = feature.get("name") || feature.get("name_2") || feature.get("name_1") || "";
        val = val.toLocaleString();
        text = new Text({
            text: val,
            font: 'bold 10px sans-serif',
            stroke: new Stroke({
                color: '#ebebe5',
                width: 3
            })
        });

    }

    const geom = feature.getGeometry().getType();
    const isString = typeof jenks[0] === 'string';

    if (indicator) {
        let gcolor;
        let proportional;
        if (isString) {
            gcolor = ramp[jenks.indexOf(indicator)];
        } else {
            const ind = ramp.length - jenks.filter(areBigger(indicator)).length;
            gcolor = ramp[ind] || ramp[ind + 1]; // ramp[ind +1] || ramp[ind];
            proportional = ind + 3;
        };
        return createStyle(geom, gcolor, text, proportional, pcolor, shape)

    } else {

        if (noind) {
            return createStyle(geom, ramp[0], null, 5, ramp[0], "circle")
        } else {
            return createStyle(geom, null);
        }

    }
}

/**
*
* Turns an existing vector layer into a heatmap.
*
*/
export const createHeatmapLayer = (layer, time, color) => {

    const source = layer.getSource();
    const features = source.getFeatures();
    let attribute = time ? "indicator_" + time : "indicator";
    let values = [];

    features.forEach(feature => {

        let point = getPoint(feature);

        feature.geometry = point;
        feature.set("geometry", point);
        feature.setStyle(null);

        const indicator = feature.get(attribute);
        values.push(indicator);

    })
    values = values.filter( v => v);
    const min = Math.min(...values);
    const max = Math.max(...values);

    const heatmap = new HeatmapLayer({
        source: source,
        weight: (feature) => {
            const weight = feature.get(attribute);
            return normalize(weight, max, min) || 0;

        },
        //gradient: color,
        renderMode: 'image'
    });

    heatmap.set("layer_id", layer.get("layer_id"));
    heatmap.set("layer_name", layer.get("layer_name"));
    heatmap.set("legend", {min, max})
    heatmap.set("time", time);

    return heatmap;

}

/**
*
* Creates a heatmap from vector source (url).
*
*/
export const buildHeatmapFromVector = (params, style) => {

    const parameters = styleParameters(style);
    const format_info = params.vector.format;
    let format;

    switch (format_info) {

        case 'KML':
            format = new KML();
            break;

        case 'GML':
            format = new WFS(); // GML() doesn't work, must be WFS
            break;

        case 'GeoJSON':
            format = new GeoJSON();
            break;

        default:
            break;

    }

    const source = new VectorSource({url: params.vector.source_url, format: format, crossOrigin: 'Anonymous'});

    const heatmap = new HeatmapLayer({
        source: source,
        //gradient: color,
        renderMode: 'image'
    });

    source.once('change', e => {
        let values = [];
        const features = e.target.getFeatures();
        features.forEach( feature => {

            let point = getPoint(feature);

            feature.geometry = point;
            feature.set("geometry", point);
            feature.setStyle(null);

            const indicator = feature.get("indicator");
            values.push(indicator);

        });

        values = values.filter( v => v);
        const min = Math.min(...values);
        const max = Math.max(...values);

        features.forEach( feature => {
            const weight = feature.get('indicator');
            feature.set('weight', normalize(weight, max, min) || 0)
        })
    })

    heatmap.set('layer_name', params.name, true)
    heatmap.set('layer_id', params._id, true)

    heatmap.setBlur(parameters.blur);
    heatmap.setRadius(parameters.radius)
    heatmap.setOpacity(parameters.opacity/100)

    return heatmap
}

/**
*
* Creates a heatmap from sparql response.
*
*/
export const buildHeatmapFromSparql = (params, bindings, style) => {

    const parameters = styleParameters(style);

    const format = new GeoJSON();

    // styling
    const radius = style ? style.properties.radius : 8;
    const blur = style ? style.properties.blur : 15 ;

    // sparql response

    let to_merge = bindings.map(item => {
        return {
            country: item.country.value,
            value: parseFloat(item.value.value) || item.value.value || 0,
            time: item.time.value
        }
    });
    let isString;
    if (typeof to_merge[0].value === 'string' ) {
        isString = true;
    } else {
        isString = false;
    };

    if (isString) {
        return null;
    }
    const values = to_merge.map(f => f.value);
    const min = Math.min(...values);
    const max = Math.max(...values);

    const source = new VectorSource({url: WORLD_MAP, format: format, crossOrigin: 'Anonymous'});

    const heatmap = new HeatmapLayer({
        source: source,
    });

    source.once("change", e => {

        const features = source.getFeatures();

        features.forEach( feature => {

            // geometry reduction
            let point = getPoint(feature)

            feature.geometry = point;
            feature.set("geometry", point);
            feature.setStyle(null);

            // sparql merging
            let country = feature.get('iso_a3');
            let _obj = to_merge.find(element => {
                return element.country === country
            });
            // new
            let _objs = to_merge.filter(element => {
                return element.country === country
            });

            let _ol = _objs.length;

            if (_ol === 1) {
                feature.set('indicator_' + _objs[0].time.toString(), _objs[0].value, true);
                feature.set('times', _objs[0].time, true);
                feature.set('indicator', _objs[0].value, true);
                feature.set('time', _objs[0].time, true);
            } else if (_ol > 1) {

                let counter = 0;
                let indicators = '';
                let times = '';
                while (counter < _ol) {
                    indicators += ';' + _objs[counter].value;
                    times += ';' + _objs[counter].time;
                    feature.set('indicator_' + _objs[counter].time.toString(), _objs[counter].value, true);
                    counter++;
                }
                feature.set('indicator', _objs[0].value, true);
                feature.set('time', _objs[0].time, true);
                feature.set('indicators', indicators, true);
                feature.set('times', times, true);

            }
            const weight = feature.get('indicator');
            feature.set('weight', normalize(weight, max, min) || 0)

        })
    })

    heatmap.set('layer_name', params.name, true);
    heatmap.set('layer_id', params._id, true);
    heatmap.set('indicator_type', (isString) ? 'string' : 'number');

    heatmap.setBlur(parameters.blur);
    heatmap.setRadius(parameters.radius)
    heatmap.setOpacity(parameters.opacity/100)

    return heatmap

}

/* Helper */
export const areBigger = (value) => {
    return (element, index, array) => (element + 0.01) >= value;
}

/*
    normalization (0,1)
*/
export const normalize = (val, max, min) => {
    return (val - min) / (max - min);
}

/* Geometry styling */

export const createStyle = (geom, gcolor, text, proportional, pcolor, shape) => {

    if (gcolor) {

        const isLine = ['LineString', 'MultiLineString'].includes(geom);

        let fill = new Fill({
            color: gcolor
        });

        let stroke = new Stroke({
            color: isLine ? gcolor : [53, 53, 53, 1],
            width: isLine ? 5 : 1,
        });

        let propFill = new Fill({
            color: pcolor || '#6CAF00'
        });

        let image;

        switch(shape) {

            case "triangle":
                image = new RegularShape({
                    radius: 5*proportional,
                    fill: propFill,
                    stroke: stroke,
                    points: 3,
                });
                break;

            case "square":
                image = new RegularShape({
                    radius: 5*proportional,
                    fill: propFill,
                    stroke: stroke,
                    points: 4,
                });
                break;

            case "circle":
            default:
                image = new Circle({
                    radius: 5*proportional,
                    fill: propFill,
                    stroke: stroke,
                });

        };


        return new Style({

            fill: fill,

            stroke: stroke,

            text: text,

            image: image

        });

    } else {

        return new Style({ fill: null });
    }

}

/**
*
* Get point from non-point geometry.
*
*/
export const getPoint = (feature) => {

    const geom = feature.getGeometry();

    let point;

    switch (geom.getType()) {

        case "MultiPolygon":
            //let points = geom.getInteriorPoints().getPoints();
            //point = points[Math.floor(points.length/2)];
            let polygon = geom.getPolygons().reduce((max, next) => max.getArea() > next.getArea() ? max : next);
            point = polygon.getInteriorPoint();
            break;

        case "Polygon":
            point = geom.getInteriorPoint();
            break;

        case "MultiLineString":
        case "LineString":
            let extent = geom.getExtent();
            point = new Point(geom.getClosestPoint([extent[2] - extent[0], extent[3] - extent[1]]));
            break;

        default:
            point = geom;
            break;

    }

    return point;
}
