import React, {Component} from 'react';
import './style.scss';

class Resizer extends Component {

    initResize = (e) => {

        window.addEventListener('mousemove', this.Resize, false);
        window.addEventListener('mouseup', this.stopResize, false);

    }

    Resize = (e) => {

        const element = document.getElementById(this.props.container);
        const values = element.style.transform.match(/translate\((.*)px, (.*)px\)/);
        const dx = parseInt(values[1]), dy = parseInt(values[2]);
        const x = element.offsetLeft + dx;
        const y = element.offsetTop + dy;
        element.style.width = (e.clientX - x + 10) + 'px';
        element.style.height = (e.clientY -y + 10) + 'px';

    }

    stopResize = (e) => {

        window.removeEventListener('mousemove', this.Resize, false);
        window.removeEventListener('mouseup', this.stopResize, false);

    }

    render() {
        return (

            <div className="resizer" onMouseDown={(e) => this.initResize(e)}></div>

        )
    }
}

export default Resizer;
