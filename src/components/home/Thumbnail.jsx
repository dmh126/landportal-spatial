import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import ExampleThumbnail from '../../assets/thumbnail.png';

class Thumbnail extends Component {

    render() {

        const indicator = this.props.indicator;
        const url = `/viewer?type=${indicator.type}&id=${indicator._id}`;

        return (
            <div className="home-thumbnail" title={indicator.name}>
                <Link to={url} target="_blank">
                    <img src={ExampleThumbnail}/>
                    <div className="home-thumbnail-name">{indicator.name}</div>
                </Link>
            </div>
        )
    }
}

export default Thumbnail;
