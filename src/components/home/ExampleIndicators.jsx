import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import Thumbnail from './Thumbnail';

import {getExampleIndicators} from '../../actions/index';

import './style.scss';

class ExampleIndicators extends Component {

    componentDidMount() {

        this.props.getExampleIndicators();

    }

    render() {
        return (
            <div className="home-examples">
                {
                    this.props.example_indicators.map((item, index) => {
                        return <Thumbnail key={index} indicator={item} />
                    })
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        example_indicators: state.datasets.example_indicators,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getExampleIndicators: getExampleIndicators
    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(ExampleIndicators);
