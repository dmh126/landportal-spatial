export const createLink = (type, id) => {

    return `/viewer?type=${type}&id=${id}`

}


export const sortStringsFirst = (a, b) => {

  if (typeof a[1] === 'string' ) {
    return -1;
  } else if (typeof b[1] === 'string') {
    return 1;
  } else {
    return 0;
  }

}


export const formatDate = (d) => {

    return new Date(d).toUTCString()

}

export const styleParameters = (style) => {

    const legend = (style && style.legend) ? style.legend : {};

    return {
        visualization: style ? style.type : 'graduated',
        palette: style ? style.properties.palette : 'OrRd',
        color: style ? style.properties.color : '#6CAF00',
        shape: style ? style.properties.shape : 'circle',
        opacity: style ? style.properties.opacity : 100,
        radius: style ? style.properties.radius : 8,
        blur: style ? style.properties.blur : 15,
        ramp: legend.ramp || [],
        jenks: legend.jenks || [],
        allowed: style ? style.allowed : ['Graduated Colors', 'Heatmap', 'Proportional'],
    }
}
