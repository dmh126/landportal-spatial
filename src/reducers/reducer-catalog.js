import * as actionTypes from '../constants/actions';

const initial = {
    active_tab: null,
    selected_item: null,
    indicator_info: null,
}

export default function(state = initial, action) {

    switch (action.type) {

        case actionTypes.BROWSE_BY_MAIN:
            return {
                active_tab: null,

            }

        case actionTypes.BROWSE_BY_DATASETS:

            return {
                active_tab: 'datasets',
                selected_item: action.payload,
            };

        case actionTypes.BROWSE_BY_THEMES:

            return {
                active_tab: 'themes',
                selected_item: action.payload,
            };

        case actionTypes.BROWSE_BY_COUNTRIES:

            return {
                active_tab: 'countries',
                selected_item: action.payload,
            };

        case actionTypes.BROWSE_ADVANCED:

            return {
                active_tab: 'advanced',
                selected_item: null,
            };

        case actionTypes.SHOW_INDICATOR_INFO:

            return {
                ...state,
                indicator_info: action.payload,
            };

        default:

            return state;
    }

}
