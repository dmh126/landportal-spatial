import {combineReducers} from 'redux';
import { reducer as formReducer } from 'redux-form';

import ReducerTesting from './reducer-testing';
import ReducerDatasets from './reducer-datasets';
import ReducerViewer from './reducer-viewer';
import ReducerFeatures from './reducer-features';
import ReducerCompositions from './reducer-compositions';
import ReducerCatalog from './reducer-catalog';

export const allReducers = combineReducers({
  form: formReducer,
  testing: ReducerTesting,
  datasets: ReducerDatasets,
  viewer: ReducerViewer,
  features: ReducerFeatures,
  compositions: ReducerCompositions,
  catalog: ReducerCatalog,

});

export default allReducers;
