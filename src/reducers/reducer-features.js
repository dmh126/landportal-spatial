import * as actionTypes from '../constants/actions';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
import Circle from 'ol/style/Circle';

const initial = {
    selected_feature: null,
    style: null
}

export default function(state = initial, action) {

    switch (action.type) {

        case actionTypes.FEATURE_SELECTED:
            const feature = action.payload;
            let style;
            if (feature) {

                if (state.selected_feature) {
                    state.selected_feature.setStyle(state.style);
                }

                style = feature.getStyle();

                let stroke = new Stroke({color: 'rgb(0, 255, 255)', width: 1});
                let fill = new Fill({color: 'rgba(0, 255, 255, 0.7)'});

                action.payload.setStyle(
                    new Style({
                        stroke: stroke,
                        fill: fill,
                        image: new Circle({
                            stroke: stroke,
                            fill: fill,
                            radius: 10,
                        })
                    })
                )

            } else {

                if (state.style) {
                    state.selected_feature.setStyle(state.style);
                } else {
                    if (state.selected_feature) {
                        state.selected_feature.setStyle(null);
                    }
                }

            }

            return {
                selected_feature: action.payload,
                style: style,
            };

        default:
            return state;
    }
}
