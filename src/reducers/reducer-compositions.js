import * as actionTypes from '../constants/actions';

const initial = {
    composition: null,
    loading: false,
}

export default function(state = initial, action) {

    switch (action.type) {

        case actionTypes.RECEIVED_CREATE_COMPOSITION:

            return {
                composition: action.payload,
                loading: false,
            };

        case actionTypes.REQUESTED_COMPOSITION:

            return {
                loading: true,
                composition: null,
            };

        case actionTypes.LOADED_COMPOSITION:

            return {
                loading: false,
                composition: null,
            };

        default:

            return state;
    }

}
