import * as actionTypes from '../constants/actions';
import {styleParameters} from '../helpers/index';
import _ from 'lodash';

const initial = {
    datasets: [],
    indicators: [],
    dataset_info: null,
    displayed_layers: [],
    selected_layer: null,
    example_indicators: [],
    advanced: null,
    loading: false,
}

export default function(state = initial, action) {

    switch (action.type) {

        case actionTypes.REQUEST_DATASETS:

            return {
                ...state,
                loading: true,
            };

        case actionTypes.RECEIVED_DATASETS:

            return {
                ...state,
                datasets: action.payload,
                indicators: [],
                loading: false,
            };

        case actionTypes.RECEIVED_DATASET_INFO:

            return {
                ...state,
                dataset_info: action.payload
            };

        case actionTypes.REQUEST_INDICATOR:

            return {
                ...state,
                loading: true,
            }

        case actionTypes.ADDED_LAYER:
            const new_layer = action.payload.data;
            const style = action.payload.style;
            let displayed_layers = _.cloneDeep(state.displayed_layers);
            // styling
            let parameters = styleParameters(style);
            if (["WMS", "Raster"].includes(new_layer.type)) {
                parameters.palette = null;
            }

            new_layer.style = {
                visualization: parameters.visualization,
                palette: parameters.palette,
                color: parameters.color,
                shape: parameters.shape,
                opacity: parameters.opacity,
                radius: parameters.radius,
                blur: parameters.blur,
                ramp: parameters.ramp,
                jenks: parameters.jenks,
                zIndex: displayed_layers.length + 1,
                allowed: parameters.allowed,
            };
            displayed_layers.unshift(new_layer);

            return {
                ...state,
                displayed_layers: displayed_layers,
                loading: false,
            }

        case actionTypes.REMOVED_LAYER:

            let remaining_layers = state.displayed_layers;
            remaining_layers = remaining_layers.filter( layer => layer._id != action.payload);

            return {
                ...state,
                displayed_layers: _.cloneDeep(remaining_layers),
            }

        case actionTypes.UPDATED_LAYER:
            let updated_layers = state.displayed_layers;
            const _obj = action.payload;
            updated_layers = updated_layers.map( layer => {

                if (layer._id === _obj._id) {

                    layer.style = Object.assign(layer.style, _obj.style);

                    return layer;

                } else {

                    return layer;

                }

            });
            return {
                ...state,
                displayed_layers: updated_layers,
            }

        case actionTypes.MOVED_LAYER:
            let moved_layers = _.cloneDeep(state.displayed_layers);
            const index =  moved_layers.length - action.payload.index;
            const newIndex = moved_layers.length - action.payload.index - action.payload.dir;
            [moved_layers[index], moved_layers[newIndex]] = [moved_layers[newIndex], moved_layers[index]];
            moved_layers[index].style.zIndex = index;
            moved_layers[newIndex].style.zIndex = newIndex;

            return {
                ...state,
                displayed_layers: moved_layers,
            }


        case actionTypes.SELECTED_LAYER:

            return {
                ...state,
                selected_layer: action.payload
            }

        case actionTypes.REQUEST_INDICATORS:

            return {
                ...state,
                loading: true,
            }

        case actionTypes.RECEIVED_INDICATORS:

            return {
                ...state,
                indicators: action.payload,
                loading: false,
            }

        case actionTypes.MODAL_CATALOG_DISPLAY:
            // on display turn off the loader because adding layer changing this action when not found
            return {
                ...state,
                loading: false,
            }

        case actionTypes.RECEIVED_EXAMPLE_INDICATORS:

            return {
                ...state,
                example_indicators: action.payload
            }

        case actionTypes.REQUEST_ADVANCED:

            return {
                ...state,
                loading: true,
                advanced: true,
            }

        case actionTypes.RECEIVED_ADVANCED:

            return {
                ...state,
                datasets: action.payload.datasets,
                indicators: action.payload.indicators,
                advanced: false,
                loading: false,

            }

        default:

            return state;
    }

}
