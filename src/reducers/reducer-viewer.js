import * as actionTypes from '../constants/actions';

const initial = {
    viewer: null,
    layer_info: false,
    layer_settings: false,
    modal_catalog: false,
    basemap_picker: false,
    attributes_table: false,
    map_legend: false,
    panel: true,
    globe: false,
    basemap: 'ESRI_LIGHT',
    themes: null,
    concepts: null,
    help: false,
    ga: null,
}

export default function(state = initial, action) {

    switch (action.type) {

        case actionTypes.VIEWER_CREATED:
            return {
                ...state,
                viewer: action.payload
            };

        case actionTypes.LAYER_INFO_DISPLAY:
            return {
                ...state,
                layer_info: action.payload
            };

        case actionTypes.LAYER_SETTINGS_DISPLAY:
            return {
                ...state,
                layer_settings: action.payload
            };

        case actionTypes.HELP_DISPLAY:
            return {
                ...state,
                help: action.payload
            };

        case actionTypes.ATTRIBUTES_TABLE_DISPLAY:
            return {
                ...state,
                attributes_table: action.payload
            };

        case actionTypes.MODAL_CATALOG_DISPLAY:
            return {
                ...state,
                modal_catalog: action.payload
            };

        case actionTypes.BASEMAP_PICKER_DISPLAY:
            return {
                ...state,
                basemap_picker: action.payload
            };

        case actionTypes.MAP_LEGEND_DISPLAY:
            return {
                ...state,
                map_legend: action.payload
            };

        case actionTypes.PANEL_DISPLAY:
            return {
                ...state,
                panel: action.payload
            };

        case actionTypes.SET_BASEMAP:
            return {
                ...state,
                basemap: action.payload
            };

        case actionTypes.SHOW_GLOBE:
            return {
                ...state,
                globe: action.payload
            }

        case actionTypes.RECEIVED_THEMES:
            return {
                ...state,
                themes: action.payload
            }

        case actionTypes.RECEIVED_CONCEPTS:
            return {
                ...state,
                concepts: action.payload
            }

        case actionTypes.GA_INITIALIZED:
            return {
                ...state,
                ga: action.payload
            }

        default:
            return state;
    }
}
