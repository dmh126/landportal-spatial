export const COLORS = [
    {name: 'Green', value: '#6CAF00'},
    {name: 'Red', value: '#EA670D'},
    {name: 'Blue', value: '#1084C6'},
    {name: 'Cyan', value: '#9264CC'},
    {name: 'Magenta', value: '#8BC7EA'},
    {name: 'Yellow', value: '#ffff99'}
];
