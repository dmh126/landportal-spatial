export const RAMPS = [
    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAATCAIAAAD9HmPeAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAvElEQVRoge2RMW4DMRADmfz/o/lAmiOXKXQ6RzD8AAKcwhis6Obmy78/sDACAAueJbYAYAQ/T1vmlCH22Ob9r+E55h4PZj7I54EEABTEWygAEL2E3Bs+T0ssWwPAtGUA1ryEPja6N0Ot37m2kEt8X/i2uS/i6BGtp3+X8+l1mfXtQODawjcBcJ33b5RwmjCeJoynCeNpwniaMJ4mjKcJ42nCeJowniaMpwnjacJ4mjCeJoynCeNpwniaMJ4/0ufjpZHaqQsAAAAASUVORK5CYII=')",
        value: "OrRd",
        palette: ['#fff7ec', '#fee8c8', '#fdd49e', '#fdbb84', '#fc8d59', '#ef6548', '#d7301f', '#b30000', '#7f0000'],
    },
    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAVCAIAAAArR4DDAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAzklEQVRoge2RS27DMBBDmdz/jL2Ih2QX0kT5dNdFMADfwiAoGjL8bvKPXQAMAh26MQoAQJ+GAGjJBiCLVofdyHresJvS3pT2WyV30L5mB5XcYQ8uGUCdxlcfVR9146e3VuDFx1gALrGDir1Zt3M/RQEQxRO4guplc4JMGYC4w6MhrY9mBcug158+QW+N/uzvCMOJwvFE4XiicDxROJ4oHE8UjicKxxOF44nC8UTheKJwPFE4nigcTxSOJwrHE4XjicLxROF4bra//Q3hX/wCWZ0So8mEEBAAAAAASUVORK5CYII=')",
        value: "Blues",
        palette: ["#f7fbff", "#deebf7", "#c6dbef", "#9ecae1", "#6baed6", "#4292c6", "#2171b5", "#08519c", "#08306b"],
    },
    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAUCAIAAADgG1NmAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA4UlEQVRoge2RwY3EIAwAhzg4nVzx1+EBNvsg7O6lA0ue12CQjDTlt/4AtVJPbqllC0A9t1REBRAV0eOWS4BDZV8d7zdFBSiXlEuAoregz0lR+Sz72l2+dz9+IxU5t1SgnM8Jci6xqTYrYK42FRjvyVRzBWzWddVtNvclQHdvS8y739K29OFA6/aU4b3bU5oBo9uS3m00B3qz9aY3G1u8GeB/Wz6T8f9oB0lwMmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4MmF4XlKrii+Kh2MnAAAAAElFTkSuQmCC')",
        value: "Greens",
        palette: ['#f7fcf5', '#e5f5e0', '#c7e9c0', '#a1d99b', '#74c476', '#41ab5d', '#238b45', '#006d2c', '#00441b'],
    },
    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAUCAIAAADgG1NmAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4wMNDx0JZCLyBAAAAMpJREFUaN7tlsuNwzAMREfpaFtPH9tHOtiLOW8Ppiz5nEMQYB4Mg+JPNgYUNOApLCzXet8MC4tDvuw9oaaztoRa5VcHlzCUTD9IRsylJZC9Qiuqm43vJXtodpglTJtCFva2PD3IqCR3cufUVmtUqDrUy6t2eTj37/9GeMBln183bLVzGnBzmrF1EHTh7NBbHP77ef0+FL4ZIBJGwhAJQySMhCEShkgY3iISZgrDR7EdCTOFIVMYcp3JQRoiYfgUIxJmCkMkDG8fpcA/ggJYvhdUTpQAAAAASUVORK5CYII=')",
        value: "RdYlGn",
        palette: ['#d7191c', '#fdae61', '#ffffbf', '#a6d96a', '#1a9641'],
    },
    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAVCAIAAAArR4DDAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAA4ElEQVRoge2RsW0EMQwER4b7b+PbcQ1u4oGjdulAuscfLnTyBDjRgKQCYUZm0lTme/oHMMrUVeaSTJktSgNKlOMUAPk2SeTL5PA4DBDa8j4BDnN4yQjtg1gHHuEBRN4m5m11mUhIeQqAlUvuK52rVAJjmmmA6fGSyCVrNW6CzExgyPvb02jd5BKU+9V5k5IdgBzOAOyQ55LXCnCGPfelA3j+Pr7+07/5BDpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTpheTphef4AI6vgKaPETuUAAAAASUVORK5CYII=')",
        value: "Spectral",
        palette: ["#9e0142", "#d53e4f", "#f46d43", "#fdae61", "#fee08b", "#ffffbf", "#e6f598", "#abdda4", "#66c2a5", "#3288bd", "#5e4fa2"],
    },
    /*{
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAUCAIAAADgG1NmAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4wMNDxoQTwjMAwAAA5JJREFUaN7tmkuu7MgNRM8J6tlr8k68Bm/YCzDgVVxleJCp97Pd6HeBBnogogChSmImyUPmoEL+/W//iAkDQnBAkyQVErQTk6oJijrRoA2qsWZFo7IkSrICoWL2kymoZO8WUrUpQCAF1qgiZmEJ6LKqAXEswA4BrEoklCagZq9gWRpcqrOwSGTvGwM15IR2JwIJgKm5tZMi0kSyRgRD5s5q0sxHILmHXR5wxfUXV3TKWOe+uCPRyUf0C9UOhHXNumBYE9IOXkE6mqzBLzA4YcgY8zE4ev3rn/+WmAlB6siYK2ElJGSI8Vox11RRZgjElUSYIa7Y2eUhmRV2vVAuSzO5QyUDu+jDEtNi4oqcnmBZE9WhwS6EwchYUEgTm1WP12ZP6lA3zo38jubajjUSMqsswZgUydzJhnrvRDNoZyqNd6YRU71HMx9hjSRKJx/jSvxi9b7s5CNBeoXrXldW7OVK7gu+2HFddLwDf4VxTXu1X+BjcdnKdRd3vuSmaCaLwSu5Mnt2IqNiIE7URDJmKs4EMimQ9IrQmJjo7Bmlu8c8SzYHoWGpcbRRi+yZdg8i7DvNJleVcTd+Q4kucgIse/JI6DPWSYCDXJuV4B4fM6jUfZrY1HMgkJisJyGhOlpDfJoWzPW0WbHBMLKjbY47EaJUqxMaGPYx4bDiSWI3mBT2MVgQ9sQvBKm04Lm1Ye6jCdtzXvzprD9dP+f9hwXWT7h9Kih/z0N/ToSv/YK9CF+Er70IX3sRvghfexG+9iJ87UX4InztRfjai/C1F+GL8Jv1h8tv2K/9+d5PbeL/XKP/7SpA/fbzJ8UKf3c6/aW8/Vax9ofS9Tcr+ZMw0v+L8FHEPHuZs29z1Ku6ZfLub93qO0fc+oFooW13Rd0/22J/CM2jhEG2i90CPKUP4lIPG59v9vifzRbF49xTrvPZwhqusij3d/W7bZ/g9x594ukpuTvmnQm1pF20i53RAfmo2t8F/a0efUTADe8svs5O3XlzCuXP4/EV+hPIybA9RQPgwlXcYihuqAsGijdbyk/qOuJp9gOBavpwf1LOd013WtAi7VYot556emHh0IWRrwTaXYZv0Bd1p2GfDuvX0VuuHjmVUsWyTjnV1p1anzApi8S9iEWbtV+8qIv9toc0ra2rnfPCBm3W1o4fMRZYnBHYLxNsXZY8EUrX1oRp060GCz5j7wnru6Ht05PdTb5jP8BdDwb0PxbN4LehFUGPAAAAAElFTkSuQmCC')",
        value: "Viridis",
        palette: ["#440154", "#482777", "#3f4a8a", "#31678e", "#26838f", "#1f9d8a", "#6cce5a", "#b6de2b", "#fee825"],
    },*/

    {
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAATCAIAAAD9HmPeAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAvElEQVRoge2RMW4DMRADmfz/o/lAmiOXKXQ6RzD8AAKcwhis6Obmy78/sDACAAueJbYAYAQ/T1vmlCH22Ob9r+E55h4PZj7I54EEABTEWygAEL2E3Bs+T0ssWwPAtGUA1ryEPja6N0Ot37m2kEt8X/i2uS/i6BGtp3+X8+l1mfXtQODawjcBcJ33b5RwmjCeJoynCeNpwniaMJ4mjKcJ42nCeJowniaMpwnjacJ4mjCeJoynCeNpwniaMJ4/0ufjpZHaqQsAAAAASUVORK5CYII=')",
        value: "Set3",
        palette: ["#8dd3c7", "#ffffb3", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#b3de69", "#fccde5", "#d9d9d9", "#bc80bd", "#ccebc5", "#ffed6f"],
    },
    /*{
        img: "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAVCAIAAAArR4DDAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAzklEQVRoge2RS27DMBBDmdz/jL2Ih2QX0kT5dNdFMADfwiAoGjL8bvKPXQAMAh26MQoAQJ+GAGjJBiCLVofdyHresJvS3pT2WyV30L5mB5XcYQ8uGUCdxlcfVR9146e3VuDFx1gALrGDir1Zt3M/RQEQxRO4guplc4JMGYC4w6MhrY9mBcug158+QW+N/uzvCMOJwvFE4XiicDxROJ4oHE8UjicKxxOF44nC8UTheKJwPFE4nigcTxSOJwrHE4XjicLxROF4bra//Q3hX/wCWZ0So8mEEBAAAAAASUVORK5CYII=')",
        value: "Paired",
        palette: ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a", "#ffff99", "#b15928"],
    },*/
]
