export const TYPES = [
  "Vector",
  "Raster",
  "WMS"
]

export const INITIAL_VALUES = {
  'name': '',
  'region': '',
  'country': '',
  'label': '',
  'description': '',
  'organization': '',
  'measurement_unit': '',
  'high_low': '',
  'type-Vector': true,
  'type-Raster': true,
  'type-WMS': true,
  'type-SPARQL': true,
  'related_overarching_categories': '',
  'related_themes': '',
  'related_concepts': '',
  'created_at_from': '',
  'created_at_to': ''
}
