# Landportal Front-End app

Requirements:
```
React JS app, after build can be hosted as a static website.
```

Development how To:

```
npm install

npm start
```

Build:
```
npm run build-dev
```

Deployment how to:
```
docker build --tag=<app_name> .
```

Contact:
```
harasymczuk@contecht.eu
```
